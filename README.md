# Cubbis #

You can check the website here [cubbis.ro](https://cubbis.ro/)

## Tech stack
Concrete5 CMS, PHP, mySQL, jQuery, AJAX. Bootstrap, SCSS

## Installation

1. Download Concrete5 v8+ and follow the install steps from [here](https://documentation.concretecms.org/developers/introduction/installing-concrete-cms)
2. Check the system requirements from [here](https://documentation.concretecms.org/developers/introduction/system-requirements)
3. Copy and replace all files and folders from this repo to your Concrete5 setup in the appropriate locations 
4. Login as an administrator
5. Find the 'Extend Concrete5' page in your dashboard
6. Find 'Cubbis Theme' package in the list of packages awaiting installation and install it

![alt text](cubbis_preview.jpg)