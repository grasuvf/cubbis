<?php defined('C5_EXECUTE') or die('Access denied.');
	$form = Core::make('helper/form');
	$dh = Core::make('helper/date');
?>

<form method="post" action="<?= URL::to('/login', 'authenticate', $this->getAuthenticationTypeHandle()) ?>">
	<div class="form-group">
		<label class="control-label" for="uName"><?= Config::get('concrete.user.registration.email_registration') ? t('Email') : t('Nume utilizator') ?></label>
		<input name="uName" id="uName" class="form-control" autofocus="autofocus" />
	</div>

	<div class="form-group">
		<label class="control-label" for="uPassword"><?=t('Parola')?></label>
		<input name="uPassword" id="uPassword" class="form-control" type="password" />
	</div>

	<?php if (Config::get('concrete.session.remember_me.lifetime') > 86400) { ?>
		<div class="checkbox">
			<label>
				<input type="checkbox" name="uMaintainLogin" value="1">
				<?php 
					$days = ceil((int)Config::get('concrete.session.remember_me.lifetime') / 86400);
					echo t('Ramai logat pentru %s zile', $days); 
				?>
			</label>
		</div>
	<?php } ?>

	<?php if (isset($locales) && is_array($locales) && count($locales) > 0) { ?>
		<div class="form-group">
			<label for="USER_LOCALE" class="control-label"><?= t('Limba') ?></label>
			<?= $form->select('USER_LOCALE', $locales) ?>
		</div>
	<?php } ?>

	<div class="form-group">
		<button class="btn btn-primary"><?= t('Continua') ?></button>
		<a href="<?= URL::to('/login', 'concrete', 'forgot_password')?>" class="btn pull-right"><?= t('Am uitat parola') ?></a>
	</div>

	<?php Core::make('helper/validation/token')->output('login_' . $this->getAuthenticationTypeHandle()); ?>

	<?php if (Config::get('concrete.user.registration.enabled')) { ?>
		<br/><hr/>
		<a href="<?=URL::to('/register')?>" class="btn btn-block btn-success"><?=t('Nu esti membru? Inregistreaza-te')?></a>
	<?php } ?>
</form>
