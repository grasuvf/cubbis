<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<?php if (count($userOrders) > 0) { ?>
  <table class="table table-hover">
    <thead>
      <tr>
        <th>Username</th>
        <th>Email</th>
        <th>Last Seen Online</th>
        <th>Total Orders</th>
        <th></th>
      </tr>
    </thead>
    <tbody>
      <?php foreach($userOrders as $user) { ?>
      <tr>
        <td><?= $user['userName'] ?></td>
        <td><?= $user['userEmail'] ?></td>
        <td><?= date("jS M Y", $user['userLastOnline']) ?></td>
        <td><?= $user['userOrders'] ?></td>
        <td class="text-right">
          <a href="#" class="btn btn-xs btn-danger" data-delete="<?= $view->action('delete', $user['userID']) ?>">Delete</a>
        </td>
      </tr>
      <?php } ?>
    </tbody>
  </table>

  <div style="display: none">
    <div class="ccm-ui data-delete-dialog">
      <form id="formDelete" action="" method="post">
        <span>Delete all this user orders? This cannot be undone.</span>
        <?= Loader::helper('validation/token')->output('delete_user_orders') ?>
      </form>
      <div class="dialog-buttons">
        <button onclick="jQuery.fn.dialog.closeTop()" class="btn btn-default pull-left">Cancel</button>
        <button onclick="$('form#formDelete').submit()" type="submit" class="btn btn-danger pull-right">Delete</button>
      </div>
    </div>
  </div>

<?php } else { ?>
  <div class="alert alert-info">There is no user with saved orders.</div>
<?php } ?>

<style type="text/css">
  .ccm-ui .table>tbody>tr>td {
    vertical-align: middle;
  }
</style>

<script>
$(function() {
  $('a[data-delete]').on('click', function() {
    var actionUrl = $(this).attr('data-delete');
    jQuery('.data-delete-dialog form').attr('action', actionUrl);
    jQuery.fn.dialog.open({
      element: '.data-delete-dialog',
      title: 'Delete User Orders',
      modal: true,
      width: 400,
      height: 150
    });
  });
});
</script>
