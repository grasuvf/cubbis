<?php defined('C5_EXECUTE') or die('Access denied.');
use Concrete\Core\Attribute\Key\Key;
use Concrete\Core\Http\ResponseAssetGroup;

$c = Page::getCurrentPage();
$p = new Permissions($c);

$form = Loader::helper('form');
$auth = AuthenticationType::getByHandle('concrete');
?>

<div class="row">
	<div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
		<div class="login-logo">
			<?php
			$a = new Area('Login Logo');
			$a->display($c);
			?>
		</div>
		<div class="login-box">
			<h1><?= t('Autentificare.') ?></h1>
			<?php $auth->renderForm('form', array()) ?>
		</div>
	</div>
</div>

<style type="text/css">
	<?php if (!$p->canViewToolbar()) { ?>
		body > div:not(.ccm-ui) {
			display: none!important;
		}
		body #ccm-toolbar,
		body #ccm-page-controls-wrapper {
			display: none!important;
		}
	<?php } ?>

	body .backstretch,
	body > div:first-of-type img,
	body .ccm-page-background-credit {
		display: none!important;
	}

	body {
		font-family: Helvetica;
		background: #f7f7f7;
	}
	body .container {
		padding-top: 0;
	}
	.ccm-ui .alert{
		margin: 15px 0;
	}
	.login-logo {
		text-align: center;
		min-height: 15px;
	}
	.login-logo img {
		width: auto;
		margin: 20px auto;
		max-width: 100px!important;
		max-height: 100px!important;
	}
	.login-box {
		padding: 40px 30px;
		border: 1px solid #ebebeb;
		color: #636363;
		background: #fafafa;
		-webkit-box-shadow: 0 0 4px rgba(0,0,0,.15);
		box-shadow:         0 0 4px rgba(0,0,0,.15);
		-webkit-transition: box-shadow .3s ease;
		transition:         box-shadow .3s ease;
	}
	.login-box:hover {
		-webkit-box-shadow: 0 0 9px rgba(0,0,0,.2);
		box-shadow:         0 0 9px rgba(0,0,0,.2);
	}
	.login-box h1 {
		font-size: 32px;
		font-weight: 300;
		margin: 0 0 15px;
	}
	.login-box .form-group:last-of-type {
		margin-bottom: 0;
	}
	.login-box .form-group input {
		color: #636363;
		border-radius: 3px;
		border: 2px solid rgba(51, 51, 51, 0.3);
		-webkit-transition: border-color 0.4s ease;
    transition: border-color 0.4s ease;
	}
	.login-box .form-group input:focus {
		border: 2px solid rgba(51, 51, 51, 0.5);
	}
	.login-box .form-group button.btn {
		background: #79b38a;
		border-color: #79b38a;
		border-radius: 3px;
	}
	.login-box .form-group button.btn:hover {
		border-color: #79b38a;
		-webkit-box-shadow: inset 0 0 100px rgba(0, 0, 0, 0.1);
		box-shadow:         inset 0 0 100px rgba(0, 0, 0, 0.1);
	}
	@media (min-width: 768px) {
		.login-logo {
			margin-top: 100px;
		}
		.login-logo img {
			max-width: 150px!important;
			max-height: 150px!important;
		}
	}
	@media (max-width: 360px) {
		.login-logo img {
			max-width: 70px!important;
			max-height: 70px!important;
		}
		.login-box {
			padding: 20px;
		}
		.login-box h1 {
			font-size: 28px;
		}
		.login-box a.btn {
			padding: 8px 10px;
		}
	}
</style>
