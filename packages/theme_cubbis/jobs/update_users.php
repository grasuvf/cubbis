<?php
namespace Concrete\Package\ThemeCubbis\Job;
use Concrete\Core\Job\Job as AbstractJob;
use Concrete\Core\User\UserInfoRepository;
use Concrete\Core\User\RegistrationService;
use Concrete\Core\Support\Facade\Application;
use Concrete\Core\Database\Connection\Connection;
use RuntimeException;
use Core;

class UpdateUsers extends AbstractJob
{
  protected $app;
  protected $userInfoRepository;
  protected $userRegistrationService;

  public function __construct()
  {
    $this->app = Application::getFacadeApplication();
    $this->userInfoRepository = $this->app->make(UserInfoRepository::class);
    $this->userRegistrationService = $this->app->make(RegistrationService::class);
  }

  public function getJobName()
  {
    return t("Cubbis Update Users");
  }

  public function getJobDescription()
  {
    return t("Cubbis Update Users job from app external");
  }

  public function run()
  {
    $usersUpdated = array();

    $db = $this->app->make('database')->connection('cubbis_module');
    $queryUsers = $db->fetchAll(
<<<'EOT'
SELECT
  u.id,u.first_name,u.last_name,u.email,u.phone,u.password,u.usertype_id,u.provider_id,IFNULL(pv.price,0) AS uprice
FROM
  users u
  JOIN providers pv
    ON u.provider_id=pv.id
WHERE
  u.usertype_id=2 OR u.usertype_id=3
EOT
    );
    
    /* 
    imported users are of 2 types: workers and partners
    the difference between them is that workers will see the default products prices ('price')
    the partners are of 5 levels: from 'bronze' to 'premium' and they can see products prices with discount (from 'price1' to 'price5')
    */
    foreach ($queryUsers as $user) {
      $existingUser = $this->userInfoRepository->getByEmail($user['email']);
      $userType = $user['usertype_id'] == 3 ? 'partner' : 'worker';
      $userPrice = 'price';
      if ($user['uprice'] > 0) {
        $userPrice = 'price'.$user['uprice'];
      }
      // if existing user 'price' or 'type' field were modified, update
      if (!empty($existingUser)) {
        if ($existingUser->getAttribute('user_type') != $userType || $existingUser->getAttribute('user_price') != $userPrice) {
          $existingUser->setAttribute('user_price', $userPrice);
          $existingUser->setAttribute('user_type', $userType);

          $existingUserObj = $existingUser->getUserObject();

          array_push($usersUpdated, $user['email']);
        }
      } else {
        // add new user if doesn't exist
        $newUser = $this->userRegistrationService->create([
          'uName' => $this->getNewUsernameFromUserDetails($user['email'], $user['first_name'], $user['last_name']), 
          'uEmail' => $user['email'], 
          'uPassword' => strtolower($user['last_name']).'123',
          'uIsValidated' => 1
        ]);
        $newUser->setAttribute('user_first_name', $user['first_name']);
        $newUser->setAttribute('user_last_name', $user['last_name']);
        $newUser->setAttribute('user_phone', $user['phone']);
        $newUser->setAttribute('user_token_id', $user['id']);
        $newUser->setAttribute('user_token', $user['password']);
        $newUser->setAttribute('user_price', $userPrice);
        $newUser->setAttribute('user_type', $userType);

        if ($userType == 'partner') {
          $newUserObj = $newUser->getUserObject();
        }

        array_push($usersUpdated, $user['email']);
      }
    }

    return t('%1$d users added or updated.', count($usersUpdated));
  }

  public function getNewUsernameFromUserDetails($email, $firstName = '', $lastName = '')
  {
    $firstName = $this->stringToUsernameChunk($firstName);
    $lastName = $this->stringToUsernameChunk($lastName);
    if ($firstName !== '' || $lastName !== '') {
      $baseUsername = trim($firstName . '_' . $lastName, '_');
    } else {
      $mailbox = strstr((string) $email, '@', true);
      $baseUsername = $this->stringToUsernameChunk($mailbox);
    }
    if ($baseUsername === '') {
      $baseUsername = 'user';
    }
    $username = $baseUsername;
    $suffix = 1;
    while ($this->userInfoRepository->getByName($username) !== null) {
      $username = $baseUsername . '_' . $suffix;
      ++$suffix;
    }
    return $username;
  }

  public function stringToUsernameChunk($string)
  {
    $string = trim((string) $string);
    $string = preg_replace('/[^a-z0-9]+/', '_', strtolower($string));
    $string = preg_replace('/__+/', '_', $string);
    $string = trim($string, '_');
    return $string;
  }
}
