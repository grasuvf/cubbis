<?php

return [
	'default-connection' => 'concrete',
	'connections' => [
		'concrete' => [
			'driver' => 'c5_pdo_mysql',
			'server' => 'localhost',
			'database' => 'concrete5_data',
			'username' => 'concrete5user',
			'password' => 'H1TTheConcrete@1',
			'character_set' => 'utf8mb4',
			'collation' => 'utf8mb4_unicode_ci',
		],
		'cubbis_module' => [
			'driver' => 'c5_pdo_mysql',
			'server' => 'localhost',
			'database' => 'cubbis',
			'username' => 'hnet',
			'password' => 'McqLBSUbH8fdAGvf',
			'character_set' => 'utf8mb4',
			'collation' => 'utf8mb4_unicode_ci',
		],
	],
];
