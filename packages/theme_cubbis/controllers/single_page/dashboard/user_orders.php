<?php
namespace Concrete\Package\ThemeCubbis\Controller\SinglePage\Dashboard;
use Concrete\Core\Database\Connection\Connection;

class UserOrders extends \Concrete\Core\Page\Controller\DashboardPageController
{
  public function view()
  {
    $db = $this->app->make('database')->connection();
    $userOrders = $db->fetchAll(
<<<'EOT'
SELECT
  o.uID as userID,COUNT(o.uID) as userOrders,u.uName as userName,u.uEmail as userEmail,u.uLastOnline as userLastOnline
FROM
  btCubbisOrderList o
  JOIN Users u
    ON o.uID=u.uID
GROUP BY
  o.uID
EOT
    );
    $this->set('userOrders', $userOrders);
  }

  public function user_orders_deleted()
  {
    $this->set('success', 'All orders for this user have been deleted.');
    $this->view();
  }

  public function delete($uID = false)
  {
    if (!$this->token->validate('delete_user_orders')) {
      $this->error->add(t($this->token->getErrorMessage()));
    }

    $db = $this->app->make('database')->connection();
    $userOrders = $db->fetchAll('SELECT orderID FROM btCubbisOrderList WHERE uID = ?', [$uID]);
    
    if (count($userOrders) == 0) {
      $this->error->add(t('There is no saved order found for this user'));
    }

    if (!$this->error->has()) {
      $userOrderList = array_column($userOrders, 'orderID');
      $db->executeQuery('DELETE FROM btCubbisOrderList WHERE uID = ?', [$uID]);
      $db->executeQuery('DELETE FROM btCubbisOrderListEntries WHERE orderID IN (?)', [$userOrderList], [Connection::PARAM_STR_ARRAY]);

      $this->redirect('/dashboard/user_orders', 'user_orders_deleted');
    }

    $this->view();
  }
}
?>
