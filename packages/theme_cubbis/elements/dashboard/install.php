<?php defined('C5_EXECUTE') or die(_("Access Denied.")); ?>

<h3><?= t('Cubbis Theme Package'); ?></h3>
<?php if (version_compare(APP_VERSION, '8.3.2', '>=')) { ?>
	<p><?= t('The current version of concrete5 is <b>%s</b>, greater than the required version. <b>You are good to go!</b>', APP_VERSION); ?></p>
	<p><?= t('After package install, the following items should be added to the dashboard:'); ?></p>
	<table>
		<tr>
			<th>Type</th>
			<th>Items</th>
			<th>Location</th>
		</tr>
		<tr>
			<td>Theme</td>
			<td>Cubbis</td>
			<td><i>/dashboard/pages/themes</i></td>
		</tr>
		<tr>
			<td>Page template</td>
			<td>Full</td>
			<td><i>/dashboard/pages/templates</i></td>
		</tr>
		<tr>
			<td>Jobs</td>
			<td>Cubbis Update Users</td>
			<td><i>/dashboard/system/optimization/jobs</i></td>
		</tr>
		<tr>
			<td>Blocks</td>
			<td>
				Spacer<br/>
				Carousel<br/>
				Image Gallery<br/>
				Link List<br/>
				Map Locations<br/>
				Teaser Banner<br/>
				Teaser Info<br/>
				Video Background<br/>
				Product List<br/>
				Order Cart<br/>
				Order Logs
			</td>
			<td><i>/dashboard/blocks/types</i></td>
		</tr>
		<tr>
			<td rowspan="3">Attributes</td>
			<td>
				Thumbnail<br/>
				Exclude Subpages from Nav
			</td>
			<td><i>/dashboard/pages/attributes</i></td>
		</tr>
		<tr>
			<td>
				User First Name<br/>
				User Last Name<br/>
				User Phone<br/>
				User Token ID<br/>
				User Token<br/>
				User Price<br/>
				User Type
			</td>
			<td><i>/dashboard/users/attributes</i></td>
		</tr>
		<tr>
			<td>
				Header Logo<br/>
				Header Search Page<br/>
				Header Phone<br/>
				Header Email<br/>
				Shopping Cart Page<br/>
				Shopping List Page<br/>
				Product List Page<br/>
				App External Page<br/>
				App External File Path
			</td>
			<td><i>/dashboard/system/basics/name</i></td>
		</tr>
	</table>
<?php } else { ?>
	<p class="warning"><?= t('The current version of concrete5 is <b>%s</b>, lower than the required version. Some things need to be done manually!', APP_VERSION); ?></p>
	<p><?= t('Go to <i>/dashboard/system/basics/attributes</i> and add the following attributes:'); ?></p>
	<table>
		<tr>
			<th>Attribute type</th>
			<th>Attribute handle</th>
			<th>Attribute name</th>
		</tr>
		<tr>
			<td>Image/File</td>
			<td>header_logo</td>
			<td>Header Logo</td>
		</tr>
		<tr>
			<td>Phone Number</td>
			<td>header_phone</td>
			<td>Header Phone</td>
		</tr>
		<tr>
			<td>Email</td>
			<td>header_email</td>
			<td>Header Email</td>
		</tr>
		<tr>
			<td>Text</td>
			<td>order_cart_page</td>
			<td>Shopping Cart Page</td>
		</tr>
		<tr>
			<td>Text</td>
			<td>order_list_page</td>
			<td>Shopping List Page</td>
		</tr>
		<tr>
			<td>Text</td>
			<td>product_list_page</td>
			<td>Product List Page</td>
		</tr>
		<tr>
			<td>Text</td>
			<td>app_external_page</td>
			<td>App External Page</td>
		</tr>
		<tr>
			<td>Text</td>
			<td>app_external_file_path</td>
			<td>App External File Path</td>
		</tr>
	</table>
	<br/>
	<p><?= t('After package install, the following items should be added to the dashboard:'); ?></p>
	<table>
		<tr>
			<th>Type</th>
			<th>Items</th>
			<th>Location</th>
		</tr>
		<tr>
			<td>Theme</td>
			<td>Cubbis</td>
			<td><i>/dashboard/pages/themes</i></td>
		</tr>
		<tr>
			<td>Page template</td>
			<td>Full</td>
			<td><i>/dashboard/pages/templates</i></td>
		</tr>
		<tr>
			<td>Jobs</td>
			<td>Cubbis Update Users</td>
			<td><i>/dashboard/system/optimization/jobs</i></td>
		</tr>
		<tr>
			<td>Blocks</td>
			<td>
				Spacer<br/>
				Carousel<br/>
				Image Gallery<br/>
				Link List<br/>
				Map Locations<br/>
				Teaser Banner<br/>
				Teaser Info<br/>
				Video Background<br/>
				Product List<br/>
				Order Cart<br/>
				Order Logs
			</td>
			<td><i>/dashboard/blocks/types</i></td>
		</tr>
		<tr>
			<td rowspan="2">Attributes</td>
			<td>
				Thumbnail<br/>
				Exclude Subpages from Nav
			</td>
			<td><i>/dashboard/pages/attributes</i></td>
		</tr>
		<tr>
			<td>
				User First Name<br/>
				User Last Name<br/>
				User Phone<br/>
				User Token ID<br/>
				User Token<br/>
				User Price<br/>
				User Type
			</td>
			<td><i>/dashboard/users/attributes</i></td>
		</tr>
	</table>
<?php } ?>

<style>
	.warning {
		color: red;
		font-weight: 600;
	}
	.ccm-ui table {
		border-collapse: collapse;
		width: 100%;
	}
	.ccm-ui th {
		background-color: #dddddd;
	}
	.ccm-ui th, 
	.ccm-ui td {
		border: 1px solid #c6cbcf;
		text-align: left;
		padding: 4px 8px;
	}
	.ccm-ui tr:nth-child(odd) {
		background-color: #f3f3f3;
	}
</style>