<?php defined('C5_EXECUTE') or die("Access Denied.");
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$c = Page::getCurrentPage();

if ($c->isEditMode()) {
	if (count($rows) > 0) {
		?>
		<div class="ccm-edit-mode-disabled-item" style="width: 100%; height: 100px; margin-bottom: 30px;">
			<div style="padding: 30px 0px 0px 0px"><?= t('Carousel disabled in edit mode.'); ?></div>
		</div>
		<?php
	} else {
		?>
		<div class="ccm-edit-mode-disabled-item"><?= t('Empty Carousel Block.'); ?></div>
		<?php
	}
} elseif (count($rows) > 0) {
	?>
	<div class="block-carousel_text carousel-slick">
		<?php if ($heading) { echo '<h3>'. $heading .'</h3>'; } ?>

		<div class="carousel-container carousel-container-<?= $bID ?>">
			<?php foreach ($rows as $row) {
				$f = File::getByID($row['fID']);
				if ($row['text']) {
					?>
					<div class="carousel-item">
						<?php if (is_object($f) && $f->getFileID()) {
							$img = $app->make('helper/image');
							$thumb = $img->getThumbnail($f, 100, 100, true);
							$imgTag = new \HtmlObject\Image();
							$imgTag->src($thumb->src);
							$imgTag->class('carousel-image');
							$imgTag->alt($row['title'] ? $row['title'] : '');
							echo $imgTag;
						}
						?>
						<?php echo $row['text']; ?>
					</div>
				<?php
				}
			} ?>
		</div>
		<div class="carousel-container-loading">
			<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
			<span class="sr-only">Loading...</span>
		</div>
	</div>

	<script>
	$(document).ready(function(){
		$('.carousel-container-<?= $bID; ?>').slick({
			dots: true,
			infinite: true,
			speed: 300,
			slidesToShow: 1,
			swipeToSlide: true,
			responsive: [
				{
					breakpoint: 768,
					settings: {adaptiveHeight: true}
				}
			]
		});
	});
	</script>
	<?php
} ?>
