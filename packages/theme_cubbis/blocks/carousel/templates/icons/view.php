<?php defined('C5_EXECUTE') or die("Access Denied.");
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$c = Page::getCurrentPage();

if ($c->isEditMode()) {
	if (count($rows) > 0) {
		?>
		<div class="ccm-edit-mode-disabled-item" style="width: 100%; height: 100px; margin-bottom: 30px;">
			<div style="padding: 30px 0px 0px 0px"><?= t('Carousel disabled in edit mode.'); ?></div>
		</div>
		<?php
	} else {
		?>
		<div class="ccm-edit-mode-disabled-item"><?= t('Empty Carousel Block.'); ?></div>
		<?php
	}
} elseif (count($rows) > 0) {
	?>
	<div class="block-carousel_icons carousel-slick">
		<div class="carousel-container carousel-container-<?= $bID ?>">
			<?php foreach ($rows as $row) {
				$f = File::getByID($row['fID']);
				if (is_object($f) && $f->getFileID()) {
					$imgWidth = $f->getAttribute('width');
					$imgHeight = $f->getAttribute('height');
					$cropWidth = 150;
					$cropHeight = (150 * $imgHeight) / $imgWidth;
					$img = $app->make('helper/image');
					$thumb = $img->getThumbnail($f, $cropWidth, (int) $cropHeight, true);
					$imgTag = new \HtmlObject\Image();
					$imgTag->src($thumb->src);
					$imgTag->class('img-responsive');
					$imgTag->alt($row['title'] ? $row['title'] : '');
					?>
					<div class="carousel-item">
						<?php
						if ($row['linkURL']) {
							echo '<a href="'. $row['linkURL'] .'" target="'. $row['linkTarget'] .'">'. $imgTag .'</a>';
						} else {
							echo $imgTag;
						}
						?>
					</div>
				<?php
				}
			} ?>
		</div>
		<div class="carousel-container-loading">
			<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
			<span class="sr-only">Loading...</span>
		</div>
	</div>

	<script>
	$(document).ready(function(){
		$('.carousel-container-<?= $bID; ?>').slick({
				dots: false,
				infinite: true,
				speed: 300,
				slidesToShow: 7,
				swipeToSlide: true,
				responsive: [
					{
						breakpoint: 1200,
						settings: {slidesToShow: 6}
					},
					{
						breakpoint: 992,
						settings: {slidesToShow: 5}
					},
					{
						breakpoint: 768,
						settings: {slidesToShow: 3}
					},
					{
						breakpoint: 480,
						settings: {slidesToShow: 2}
					}
				]
		});
	});
	</script>
	<?php
} ?>
