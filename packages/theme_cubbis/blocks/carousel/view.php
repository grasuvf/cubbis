<?php defined('C5_EXECUTE') or die("Access Denied.");
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$c = Page::getCurrentPage();

if ($c->isEditMode()) {
	if (count($rows) > 0) {
		?>
		<div class="ccm-edit-mode-disabled-item" style="width: 100%; height: 100px; margin-bottom: 30px;">
			<div style="padding: 30px 0px 0px 0px"><?= t('Carousel disabled in edit mode.'); ?></div>
		</div>
		<?php
	} else {
		?>
		<div class="ccm-edit-mode-disabled-item"><?= t('Empty Carousel Block.'); ?></div>
		<?php
	}
} elseif (count($rows) > 0) {
	?>
	<div class="block-carousel carousel-slick">
		<?php 
			if ($heading) { echo '<h3>'. $heading .'</h3>'; }
			if ($description) { echo '<p>'. $description .'</p>'; }
		?>

		<div class="carousel-container carousel-container-<?= $bID ?>">
			<?php foreach ($rows as $row) {
				$f = File::getByID($row['fID']);
				if (is_object($f) && $f->getFileID()) {
					$img = $app->make('helper/image');
					$thumb = $img->getThumbnail($f, 400, 200, true);
					$imgTag = new \HtmlObject\Image();
					$imgTag->src($thumb->src);
					$imgTag->class('img-responsive');
					$imgTag->alt($row['title'] ? $row['title'] : '');
					?>
					<div class="carousel-item">
						<div class="carousel-item-inner">
							<div class="image-style-zoom">
								<div class="img-zoom">
									<?php echo $imgTag; ?>
								</div>
							</div>
							<div class="carousel-item-info">
								<?php 
									if ($row['title']) { echo '<span class="btn-style">'. $row['title'] .'</span>'; }
									if ($row['text']) { echo $row['text']; }
								?>
							</div>
						</div>
					</div>
				<?php
				}
			} ?>
		</div>
		<div class="carousel-container-loading">
			<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i>
			<span class="sr-only">Loading...</span>
		</div>
	</div>

	<script>
	$(document).ready(function(){
		$('.carousel-container-<?= $bID; ?>').slick({
			dots: true,
			infinite: true,
			speed: 300,
			slidesToShow: 3,
			swipeToSlide: true,
			responsive: [
				{
					breakpoint: 768,
					settings: {slidesToShow: 2}
				},
				{
					breakpoint: 480,
					settings: {slidesToShow: 1}
				}
			]
		});
	});
	</script>
	<?php
} ?>
