<?php
namespace Concrete\Package\ThemeCubbis\Block\Carousel;

use Concrete\Core\Block\BlockController;
use Concrete\Core\Editor\LinkAbstractor;
use Core;
use Page;

class Controller extends BlockController
{
	protected $btTable = 'btCubbisCarousel';
	protected $btInterfaceWidth = 500;
	protected $btInterfaceHeight = 600;
	protected $btWrapperClass = 'ccm-ui';
	protected $btCacheBlockRecord = true;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btExportTables = ['btCubbisCarousel', 'btCubbisCarouselEntries'];
	protected $btDefaultSet = 'blocksetcubbis';

	public function getBlockTypeName()
	{
		return t('Carousel');
	}

	public function getBlockTypeDescription()
	{
		return t('Carousel widget with multiple templates');
	}

	public function registerViewAssets($outputContent = '')
	{
		$this->requireAsset('javascript', 'jquery');
	}

	public function add()
	{
		$this->requireAsset('core/file-manager');
		$this->requireAsset('core/sitemap');
	}

	public function edit()
	{
		$this->requireAsset('core/file-manager');
		$this->requireAsset('core/sitemap');
		$db = $this->app->make('database')->connection();
		$query = $db->fetchAll('SELECT * from btCubbisCarouselEntries WHERE bID = ? ORDER BY sortOrder', [$this->bID]);
		$this->set('rows', $query);
	}

	public function view()
	{
		$db = $this->app->make('database')->connection();
		$query = $db->fetchAll('SELECT * from btCubbisCarouselEntries WHERE bID = ? ORDER BY sortOrder', [$this->bID]);
		
		$rows = [];
		foreach ($query as $row) {
			$row['linkTarget'] = '_self';
			if ($row['linkURL'] && !$row['internalLinkCID']) {
				$row['linkTarget'] = '_blank';
			}
			if (!$row['linkURL'] && $row['internalLinkCID']) {
				$c = Page::getByID($row['internalLinkCID'], 'ACTIVE');
				$row['linkURL'] = $c->getCollectionLink();
				$row['linkPage'] = $c;
				
			}
			$row['text'] = LinkAbstractor::translateFrom($row['text']);
			$rows[] = $row;
		}

		$this->set('rows', $rows);
	}

	public function duplicate($newBID)
	{
		parent::duplicate($newBID);
		$db = $this->app->make('database')->connection();
		$v = [$this->bID];
		$q = 'SELECT * FROM btCubbisCarouselEntries WHERE bID = ?';
		$r = $db->executeQuery($q, $v);
		foreach ($r as $row) {
			$db->executeQuery('INSERT INTO btCubbisCarouselEntries (bID, fID, linkURL, title, text, sortOrder, internalLinkCID) values(?,?,?,?,?,?,?)',
				[
					$newBID,
					$row['fID'],
					$row['linkURL'],
					$row['title'],
					$row['text'],
					$row['sortOrder'],
					$row['internalLinkCID'],
				]
			);
		}
	}

	public function delete()
	{
		$db = $this->app->make('database')->connection();
		$db->executeQuery('DELETE FROM btCubbisCarouselEntries WHERE bID = ?', [$this->bID]);
		parent::delete();
	}

	public function save($args)
	{
		$db = $this->app->make('database')->connection();
		$db->executeQuery('DELETE FROM btCubbisCarouselEntries WHERE bID = ?', [$this->bID]);
		parent::save($args);

		$count = isset($args['sortOrder']) ? count($args['sortOrder']) : 0;
		$i = 0;
		while ($i < $count) {
			$linkURL = $args['linkURL'][$i];
			$internalLinkCID = $args['internalLinkCID'][$i];
			switch ((int) $args['linkType'][$i]) {
				case 1:
					$linkURL = '';
					break;
				case 2:
					$internalLinkCID = 0;
					break;
				default:
					$linkURL = '';
					$internalLinkCID = 0;
					break;
			}

			if (isset($args['text'][$i])) {
				$args['text'][$i] = LinkAbstractor::translateTo($args['text'][$i]);
			}

			$db->executeQuery('INSERT INTO btCubbisCarouselEntries (bID, fID, title, text, sortOrder, linkURL, internalLinkCID) values(?,?,?,?,?,?,?)',
				[
					$this->bID,
					(int) $args['fID'][$i],
					$args['title'][$i],
					$args['text'][$i],
					$args['sortOrder'][$i],
					$linkURL,
					$internalLinkCID,
				]
			);
			++$i;
		}
	}
}
