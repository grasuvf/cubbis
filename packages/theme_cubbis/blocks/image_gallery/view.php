<?php defined('C5_EXECUTE') or die("Access Denied.");
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$c = Page::getCurrentPage();

if (count($rows) > 0) {
	?>
	<div class="row">
		<?php 
		foreach ($rows as $row) {
			$f = File::getByID($row['fID']);
			if (is_object($f) && $f->getFileID()) {
				$imWidth = $f->getAttribute('width');
				$imHeight = $f->getAttribute('height');
				$cropSize = (int)(1500 / $cols);
				$cropWidth = $cropSize;
				$cropHeight = ($cropSize * $imHeight) / $imWidth;

				$img = $app->make('helper/image');
				$thumb = $img->getThumbnail($f, $cropWidth, (int)$cropHeight, true);

				$imgTag = new \HtmlObject\Image();
				$imgTag->src($thumb->src);
				$imgTag->class('img-responsive');
				$imgTag->alt($row['title'] ? $row['title'] : '');
				?>
				<div class="col-sm-<?= (12/$cols) ?>">
					<div class="block-image-gallery <?= $zoomEffect == 1 ? 'image-style-zoom' : '' ?>">
						<div class="img-zoom <?= $row['title'] ? 'text-zoom' : '' ?>">
							<?php echo $imgTag; ?>

							<?php if ($row['linkURL']) {
								?>
								<a href="<?= $row['linkURL']; ?>"></a>
								<?php 
							} ?>

							<?php if ($row['title']) {
								?>
								<span><?= $row['title']; ?></span>
								<?php 
							} ?>
						</div>
					</div>
				</div>
			<?php
			}
		} ?>
	</div>
	<?php 
} elseif ($c->isEditMode()) { 
	?>
	<div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Image Gallery Block'); ?></div>
	<?php
}
