<?php
namespace Concrete\Package\ThemeCubbis\Block\ImageGallery;

use Concrete\Core\Block\BlockController;
use Core;
use Page;

class Controller extends BlockController
{
    protected $btTable = 'btCubbisImageGallery';
    protected $btInterfaceWidth = 500;
    protected $btInterfaceHeight = 600;
    protected $btWrapperClass = 'ccm-ui';
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btExportTables = ['btCubbisImageGallery', 'btCubbisImageGalleryEntries'];
    protected $btDefaultSet = 'blocksetcubbis';

    public function getBlockTypeName()
    {
        return t('Image Gallery');
    }

    public function getBlockTypeDescription()
    {
        return t('Image Gallery widget with custom display settings');
    }

    public function add()
    {
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
    }

    public function edit()
    {
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $db = $this->app->make('database')->connection();
        $query = $db->fetchAll('SELECT * FROM btCubbisImageGalleryEntries WHERE bID = ? ORDER BY sortOrder', [$this->bID]);
        $this->set('rows', $query);
    }

    public function view()
    {
        $db = $this->app->make('database')->connection();
        $query = $db->fetchAll('SELECT * FROM btCubbisImageGalleryEntries WHERE bID = ? ORDER BY sortOrder', [$this->bID]);

        $rows = [];
        foreach ($query as $row) {
            if (!$row['linkURL'] && $row['internalLinkCID']) {
                $c = Page::getByID($row['internalLinkCID'], 'ACTIVE');
                $row['linkURL'] = $c->getCollectionLink();
                $row['linkPage'] = $c;
            }
            $rows[] = $row;
        }

        $this->set('rows', $rows);
    }

    public function duplicate($newBID)
    {
        parent::duplicate($newBID);
        $db = $this->app->make('database')->connection();
        $v = [$this->bID];
        $q = 'SELECT * FROM btCubbisImageGalleryEntries WHERE bID = ?';
        $r = $db->executeQuery($q, $v);
        foreach ($r as $row) {
            $db->executeQuery('INSERT INTO btCubbisImageGalleryEntries (bID, fID, linkURL, title, sortOrder, internalLinkCID) values(?,?,?,?,?,?)',
                [
                    $newBID,
                    $row['fID'],
                    $row['linkURL'],
                    $row['title'],
                    $row['sortOrder'],
                    $row['internalLinkCID'],
                ]
            );
        }
    }

    public function delete()
    {
        $db = $this->app->make('database')->connection();
        $db->executeQuery('DELETE FROM btCubbisImageGalleryEntries WHERE bID = ?', [$this->bID]);
        parent::delete();
    }

    public function save($args)
    {
        $args += [
            'cols' => 1,
            'zoomEffect' => 0,
        ];
        $args['cols'] = isset($args['cols']) ? (int) $args['cols'] : 1;
        $args['zoomEffect'] = isset($args['zoomEffect']) ? 1 : 0;
        
        $db = $this->app->make('database')->connection();
        $db->executeQuery('DELETE FROM btCubbisImageGalleryEntries WHERE bID = ?', [$this->bID]);
        parent::save($args);

        $count = isset($args['sortOrder']) ? count($args['sortOrder']) : 0;
        $i = 0;
        while ($i < $count) {
            $linkURL = $args['linkURL'][$i];
            $internalLinkCID = $args['internalLinkCID'][$i];
            switch ((int) $args['linkType'][$i]) {
                case 1:
                    $linkURL = '';
                    break;
                case 2:
                    $internalLinkCID = 0;
                    break;
                default:
                    $linkURL = '';
                    $internalLinkCID = 0;
                    break;
            }

            $db->executeQuery('INSERT INTO btCubbisImageGalleryEntries (bID, fID, title, sortOrder, linkURL, internalLinkCID) values(?,?,?,?,?,?)',
                [
                    $this->bID,
                    (int) $args['fID'][$i],
                    $args['title'][$i],
                    $args['sortOrder'][$i],
                    $linkURL,
                    $internalLinkCID,
                ]
            );
            ++$i;
        }
    }
}
