<?php defined('C5_EXECUTE') or die("Access Denied.");

$fp = FilePermissions::getGlobal();
$tp = new TaskPermission();

?>
<script>
    $(document).ready(function() {
        var ccmReceivingEntry = '';
        var galleryEntriesContainer = $('.ccm-image-gallery-entries-<?php echo $bID; ?>');
        var _templateImageGallery = _.template($('#imageTemplate-<?php echo $bID; ?>').html());

        var attachDelete = function($obj) {
            $obj.click(function() {
                var deleteIt = confirm('<?php echo t('Are you sure?'); ?>');
                if (deleteIt === true) {
                    $(this).closest('.ccm-image-gallery-entry-<?php echo $bID; ?>').remove();
                    doSortCount();
                }
            });
        };

        var attachFileManagerLaunch = function($obj) {
            $obj.click(function() {
                var oldLauncher = $(this);
                ConcreteFileManager.launchDialog(function(data) {
                    ConcreteFileManager.getFileDetails(data.fID, function(r) {
                        jQuery.fn.dialog.hideLoader();
                        var file = r.files[0];
                        oldLauncher.html(file.resultsThumbnailImg);
                        oldLauncher.next('.image-fID').val(file.fID);
                    });
                });
            });
        };

        var doSortCount = function() {
            $('.ccm-image-gallery-entry-<?php echo $bID; ?>').each(function(index) {
                $(this).find('.ccm-image-gallery-entry-sort').val(index);
            });
        };

        galleryEntriesContainer.on('change', 'select[data-field=entry-link-select]', function() {
            var container = $(this).closest('.ccm-image-gallery-entry-<?php echo $bID; ?>');
            switch (parseInt($(this).val())) {
                case 2:
                    container.find('div[data-field=entry-link-page-selector]').addClass('hide-image-link').removeClass('show-image-link');
                    container.find('div[data-field=entry-link-url]').addClass('show-image-link').removeClass('hide-image-link');
                    break;
                case 1:
                    container.find('div[data-field=entry-link-url]').addClass('hide-image-link').removeClass('show-image-link');
                    container.find('div[data-field=entry-link-page-selector]').addClass('show-image-link').removeClass('hide-image-link');
                    break;
                default:
                    container.find('div[data-field=entry-link-page-selector]').addClass('hide-image-link').removeClass('show-image-link');
                    container.find('div[data-field=entry-link-url]').addClass('hide-image-link').removeClass('show-image-link');
                    break;
            }
        });

        <?php if ($rows) {
        foreach ($rows as $row) {
            $linkType = 0;
            if ($row['linkURL']) {
                $linkType = 2;
            } elseif ($row['internalLinkCID']) {
                $linkType = 1;
            } ?>
            galleryEntriesContainer.append(_templateImageGallery({
                fID: '<?php echo $row['fID']; ?>',
                <?php if (File::getByID($row['fID'])) { ?>
                image_url: '<?php echo File::getByID($row['fID'])->getThumbnailURL('file_manager_listing'); ?>',
                <?php } else { ?>
                image_url: '',
                <?php } ?>
                link_url: '<?php echo $row['linkURL']; ?>',
                link_type: '<?php echo $linkType; ?>',
                title: '<?php echo addslashes(h($row['title'])); ?>',
                sort_order: '<?php echo $row['sortOrder']; ?>'
            }));
            galleryEntriesContainer.find('.ccm-image-gallery-entry-<?php echo $bID; ?>:last-child div[data-field=entry-link-page-selector]').concretePageSelector({
                'inputName': '<?php echo $view->field('internalLinkCID'); ?>[]', 
                'cID': '<?php echo (1 == $linkType) ? intval($row['internalLinkCID']) : false ?>'
            });
            <?php
            }
        } ?>

        doSortCount();
        galleryEntriesContainer.find('select[data-field=entry-link-select]').trigger('change');

        $('.ccm-add-image-gallery-entry-<?php echo $bID; ?>').click(function() {
            var thisModal = $(this).closest('.ui-dialog-content');
            galleryEntriesContainer.append(_templateImageGallery({
                fID: '',
                title: '',
                link_url: '',
                cID: '',
                link_type: 0,
                sort_order: '',
                image_url: ''
            }));

            $('.ccm-image-gallery-entry-<?php echo $bID; ?>').not('.image-closed').each(function() {
                $(this).addClass('image-closed');
                var thisEditButton = $(this).closest('.ccm-image-gallery-entry-<?php echo $bID; ?>').find('.btn.ccm-edit-image');
                thisEditButton.text(thisEditButton.data('imageEditText'));
            });
            var newImage = $('.ccm-image-gallery-entry-<?php echo $bID; ?>').last();
            var closeText = newImage.find('.btn.ccm-edit-image').data('imageCloseText');
            newImage.removeClass('image-closed').find('.btn.ccm-edit-image').text(closeText);

            thisModal.scrollTop(newImage.offset().top);
            attachDelete(newImage.find('.ccm-delete-image-gallery-entry-<?php echo $bID; ?>'));
            attachFileManagerLaunch(newImage.find('.ccm-pick-image-image'));
            newImage.find('div[data-field=entry-link-page-selector-select]').concretePageSelector({
                'inputName': '<?php echo $view->field('internalLinkCID'); ?>[]'
            });
            doSortCount();
        });

        $('.ccm-image-gallery-entries-<?php echo $bID; ?>').on('click','.ccm-edit-image', function() {
            $(this).closest('.ccm-image-gallery-entry-<?php echo $bID; ?>').toggleClass('image-closed');
            var thisEditButton = $(this);
            if (thisEditButton.data('imageEditText') === thisEditButton.text()) {
                thisEditButton.text(thisEditButton.data('imageCloseText'));
            } else if (thisEditButton.data('imageCloseText') === thisEditButton.text()) {
                thisEditButton.text(thisEditButton.data('imageEditText'));
            }
        });

        $('.ccm-image-gallery-entries-<?php echo $bID; ?>').sortable({
            placeholder: "ui-state-highlight",
            axis: "y",
            handle: "i.fa-arrows",
            cursor: "move",
            update: function() {
                doSortCount();
            }
        });

        attachDelete($('.ccm-delete-image-gallery-entry-<?php echo $bID; ?>'));
        attachFileManagerLaunch($('.ccm-pick-image-image-<?php echo $bID; ?>'));
    });
</script>
<style>
    .ccm-image-gallery-block-container .form-group {
        margin: 0px 0px 10px !important;
        padding: 0px!important;
        margin-right: 0px !important;
        border-bottom: none !important;
    }
    .ccm-image-gallery-block-container input[type="text"] {
        display: block;
        width: 100%;
        height: 35px;
    }
    .ccm-image-gallery-block-container .checkbox {
        margin-top: 20px;
        margin-bottom: 0;
    }
    .ccm-image-gallery-block-container .btn-success {
        margin-bottom: 20px;
    }
    .ccm-image-gallery-entries {
        padding-bottom: 30px;
        position: relative;
    }
    .ccm-pick-image-image {
        padding: 5px;
        cursor: pointer;
        background: #dedede;
        border: 1px solid #cdcdcd;
        text-align: center;
        vertical-align: middle;
        width: 72px;
        height: 72px;
        display: table-cell;
    }
    .ccm-pick-image-image img {
        max-width: 100%;
    }
    .ccm-image-gallery-entry {
        position: relative;
    }
    .ccm-image-gallery-block-container .image-well {
        min-height: 20px;
        padding: 30px 10px 10px;
        margin-bottom: 10px;
        background-color: #f5f5f5;
        border: 1px solid #e3e3e3;
        border-radius: 4px;
        -moz-box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
        box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
    }
    .ccm-image-gallery-block-container .image-well.image-closed {
        padding: 20px 10px;
    }
    .ccm-image-gallery-entry.image-closed .form-group {
        display: none;
    }
    .ccm-image-gallery-entry.image-closed .form-group:first-of-type {
        display: block!important;
        margin-bottom: 0px!important;
    }
    .ccm-image-gallery-entry.image-closed .form-group:first-of-type label {
        display: none;
    }
    .btn.ccm-edit-image {
        position: absolute;
        top: 30px;
        right: 127px;
    }
    .btn.ccm-delete-image-gallery-entry {
        position: absolute;
        top: 30px;
        right: 41px;
    }
    .ccm-image-gallery-block-container i:hover {
        color: #428bca;
    }
    .ccm-image-gallery-block-container i.fa-arrows {
        position: absolute;
        top: 6px;
        right: 5px;
        cursor: move;
        font-size: 20px;
        padding: 5px;
    }
    .ccm-image-gallery-block-container .ui-state-highlight {
        height: 94px;
        margin-bottom: 15px;
    }
    .ccm-image-gallery-entries .ui-sortable-helper {
        -webkit-box-shadow: 0px 10px 18px 2px rgba(54,55,66,0.27);
        -moz-box-shadow: 0px 10px 18px 2px rgba(54,55,66,0.27);
        box-shadow: 0px 10px 18px 2px rgba(54,55,66,0.27);
    }
    .ccm-image-gallery-block-container .show-image-link {
        display: block;
    }
    .ccm-image-gallery-block-container .hide-image-link {
        display: none;
    }
</style>

<div class="ccm-image-gallery-block-container">
    <div class="form-group">
        <div class="checkbox">
            <label>
            <?php
            echo $form->checkbox($view->field('zoomEffect'), 1, $zoomEffect);
            echo t('Display image zoom effect on mouse hover');
            ?>
            </label>
        </div>
    </div>
    <div class="form-group" >
        <label class="control-label"><?php echo t('Display images per row'); ?></label>
        <select name="<?= $view->field('cols'); ?>" class="form-control">
            <option value="1" <?php if ($cols == 1) { ?>selected<?php } ?>><?php echo t('1 image per row'); ?></option>
            <option value="2" <?php if ($cols == 2) { ?>selected<?php } ?>><?php echo t('2 images per row'); ?></option>
            <option value="3" <?php if ($cols == 3) { ?>selected<?php } ?>><?php echo t('3 images per row'); ?></option>
            <option value="4" <?php if ($cols == 4) { ?>selected<?php } ?>><?php echo t('4 images per row'); ?></option>
            <option value="6" <?php if ($cols == 6) { ?>selected<?php } ?>><?php echo t('6 images per row'); ?></option>
            <option value="12" <?php if ($cols == 12) { ?>selected<?php } ?>><?php echo t('12 images per row'); ?></option>
        </select>
    </div>
    <div class="ccm-image-gallery-entries ccm-image-gallery-entries-<?php echo $bID; ?>"></div>
    <div>
        <button type="button" class="btn btn-success ccm-add-image-gallery-entry ccm-add-image-gallery-entry-<?php echo $bID; ?>"><?php echo t('Add Image'); ?></button>
    </div>
</div>

<script type="text/template" id="imageTemplate-<?php echo $bID; ?>">
    <div class="ccm-image-gallery-entry ccm-image-gallery-entry-<?php echo $bID; ?> image-well image-closed">
        <div class="form-group">
            <label class="control-label"><?php echo t('Image'); ?></label>
            <div class="ccm-pick-image-image ccm-pick-image-image-<?php echo $bID; ?>">
                <% if (image_url.length > 0) { %>
                    <img src="<%= image_url %>" />
                <% } else { %>
                    <i class="fa fa-picture-o"></i>
                <% } %>
            </div>
            <input type="hidden" name="<?php echo $view->field('fID'); ?>[]" class="image-fID" value="<%=fID%>" />
        </div>
        <div class="form-group" >
            <label class="control-label"><?php echo t('Title'); ?></label>
            <input class="form-control ccm-input-text" type="text" name="<?php echo $view->field('title'); ?>[]" value="<%=title%>" />
        </div>
        <div class="form-group" >
            <label class="control-label"><?php echo t('Link'); ?></label>
            <select data-field="entry-link-select" name="<?php echo $view->field('linkType'); ?>[]" class="form-control">
                <option value="0" <% if (!link_type) { %>selected<% } %>><?php echo t('None'); ?></option>
                <option value="1" <% if (link_type == 1) { %>selected<% } %>><?php echo t('Another Page'); ?></option>
                <option value="2" <% if (link_type == 2) { %>selected<% } %>><?php echo t('External URL'); ?></option>
            </select>
        </div>
        <div data-field="entry-link-url" class="form-group hide-image-link">
            <label class="control-label"><?php echo t('URL:'); ?></label>
            <textarea class="form-control" name="<?php echo $view->field('linkURL'); ?>[]"><%=link_url%></textarea>
        </div>
        <div data-field="entry-link-page-selector" class="form-group hide-image-link">
            <label class="control-label"><?php echo t('Choose Page:'); ?></label>
            <div data-field="entry-link-page-selector-select"></div>
        </div>
        <button type="button" class="btn btn-sm btn-default ccm-edit-image ccm-edit-image-<?php echo $bID; ?>" data-image-close-text="<?php echo t('Collapse Image'); ?>" data-image-edit-text="<?php echo t('Edit Image'); ?>"><?php echo t('Edit Image'); ?></button>
        <button type="button" class="btn btn-sm btn-danger ccm-delete-image-gallery-entry ccm-delete-image-gallery-entry-<?php echo $bID; ?>"><?php echo t('Remove'); ?></button>
        <i class="fa fa-arrows"></i>

        <input class="ccm-image-gallery-entry-sort" type="hidden" name="<?php echo $view->field('sortOrder'); ?>[]" value="<%=sort_order%>"/>
    </div>
</script>
