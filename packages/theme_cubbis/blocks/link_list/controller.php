<?php
namespace Concrete\Package\ThemeCubbis\Block\LinkList;

use Concrete\Core\Block\BlockController;
use Core;
use Page;

class Controller extends BlockController
{
    protected $btTable = 'btCubbisLinkList';
    protected $btInterfaceWidth = 500;
    protected $btInterfaceHeight = 500;
    protected $btWrapperClass = 'ccm-ui';
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btExportTables = ['btCubbisLinkList', 'btCubbisLinkListEntries'];
    protected $btDefaultSet = 'blocksetcubbis';

    public function getBlockTypeName()
    {
        return t('Link List');
    }

    public function getBlockTypeDescription()
    {
        return t('List of links widget');
    }

    public function add()
    {
        $this->requireAsset('core/sitemap');
    }

    public function edit()
    {
        $this->requireAsset('core/sitemap');
        $db = $this->app->make('database')->connection();
        $query = $db->fetchAll('SELECT * FROM btCubbisLinkListEntries WHERE bID = ? ORDER BY sortOrder', [$this->bID]);
        $this->set('rows', $query);
    }

    public function view()
    {
        $db = $this->app->make('database')->connection();
        $query = $db->fetchAll('SELECT * FROM btCubbisLinkListEntries WHERE bID = ? ORDER BY sortOrder', [$this->bID]);

        $rows = [];
        foreach ($query as $row) {
            if (!$row['linkURL'] && $row['internalLinkCID']) {
                $c = Page::getByID($row['internalLinkCID'], 'ACTIVE');
                $row['linkURL'] = $c->getCollectionLink();
                $row['linkPage'] = $c;
            }
            $rows[] = $row;
        }

        $this->set('rows', $rows);
    }

    public function duplicate($newBID)
    {
        parent::duplicate($newBID);
        $db = $this->app->make('database')->connection();
        $v = [$this->bID];
        $q = 'SELECT * FROM btCubbisLinkListEntries WHERE bID = ?';
        $r = $db->executeQuery($q, $v);
        foreach ($r as $row) {
            $db->executeQuery('INSERT INTO btCubbisLinkListEntries (bID, linkText, linkURL, internalLinkCID, sortOrder) values(?,?,?,?,?)',
                [
                    $newBID,
                    $row['linkText'],
                    $row['linkURL'],
                    $row['internalLinkCID'],
                    $row['sortOrder'],
                ]
            );
        }
    }

    public function delete()
    {
        $db = $this->app->make('database')->connection();
        $db->executeQuery('DELETE FROM btCubbisLinkListEntries WHERE bID = ?', [$this->bID]);
        parent::delete();
    }

    public function save($args)
    {
        $db = $this->app->make('database')->connection();
        $db->executeQuery('DELETE FROM btCubbisLinkListEntries WHERE bID = ?', [$this->bID]);
        parent::save($args);

        $count = isset($args['sortOrder']) ? count($args['sortOrder']) : 0;
        $i = 0;
        while ($i < $count) {
            $linkText = $args['linkText'][$i];
            $linkURL = $args['linkURL'][$i];
            $internalLinkCID = $args['internalLinkCID'][$i];
            switch ((int) $args['linkType'][$i]) {
                case 1:
                    $linkText = Page::getByID($args['internalLinkCID'][$i])->getCollectionName();
                    $linkURL = '';
                    break;
                case 2:
                    $internalLinkCID = 0;
                    break;
                default:
                    $linkURL = '';
                    $internalLinkCID = 0;
                    break;
            }

            $db->executeQuery('INSERT INTO btCubbisLinkListEntries (bID, linkText, linkURL, internalLinkCID, sortOrder) values(?,?,?,?,?)',
                [
                    $this->bID,
                    $linkText,
                    $linkURL,
                    $internalLinkCID,
                    $args['sortOrder'][$i],
                ]
            );
            ++$i;
        }
    }
}
