<?php defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();

if (count($rows) > 0) {
	?>
	<div class="block-link-list">
		<ul>
		<?php 
		foreach ($rows as $row) {
			if ($row['linkURL'] && $row['linkText']) {
				?>
				<li>
					<a href="<?= $row['linkURL'] ?>"><?= $row['linkText'] ?></a>
				</li>
				<?php 
			}
		} ?>
		</ul>
	</div>
	<?php 
} elseif ($c->isEditMode()) { 
	?>
	<div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Link List Block'); ?></div>
	<?php
}
?>
