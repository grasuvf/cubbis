<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<script>
    $(document).ready(function() {
        var ccmReceivingEntry = '';
        var linkListEntriesContainer = $('.ccm-link-list-entries-<?php echo $bID; ?>');
        var _templateLinkItem = _.template($('#linkListTemplate-<?php echo $bID; ?>').html());

        var attachDelete = function($obj) {
            $obj.click(function() {
                var deleteIt = confirm('<?php echo t('Are you sure?'); ?>');
                if (deleteIt === true) {
                    $(this).closest('.ccm-link-list-entry-<?php echo $bID; ?>').remove();
                    doSortCount();
                }
            });
        };

        var doSortCount = function() {
            $('.ccm-link-list-entry-<?php echo $bID; ?>').each(function(index) {
                $(this).find('.ccm-link-list-entry-sort').val(index);
            });
        };

        linkListEntriesContainer.on('change', 'select[data-field=entry-link-select]', function() {
            var container = $(this).closest('.ccm-link-list-entry-<?php echo $bID; ?>');
            switch (parseInt($(this).val())) {
                case 2:
                    container.find('div[data-field=entry-link-text]').addClass('show-item-link').removeClass('hide-item-link');
                    container.find('div[data-field=entry-link-page-selector]').addClass('hide-item-link').removeClass('show-item-link');
                    container.find('div[data-field=entry-link-url]').addClass('show-item-link').removeClass('hide-item-link');
                    break;
                case 1:
                    container.find('div[data-field=entry-link-text]').addClass('hide-item-link').removeClass('show-item-link');
                    container.find('div[data-field=entry-link-url]').addClass('hide-item-link').removeClass('show-item-link');
                    container.find('div[data-field=entry-link-page-selector]').addClass('show-item-link').removeClass('hide-item-link');
                    break;
                default:
                    container.find('div[data-field=entry-link-text]').addClass('hide-item-link').removeClass('show-item-link');
                    container.find('div[data-field=entry-link-page-selector]').addClass('hide-item-link').removeClass('show-item-link');
                    container.find('div[data-field=entry-link-url]').addClass('hide-item-link').removeClass('show-item-link');
                    break;
            }
        });

        <?php if ($rows) {
        foreach ($rows as $row) {
            $linkType = 0;
            if ($row['linkURL']) {
                $linkType = 2;
            } elseif ($row['internalLinkCID']) {
                $linkType = 1;
            } ?>
               linkListEntriesContainer.append(_templateLinkItem({
                    link_text: '<?php echo $row['linkText']; ?>',
                    link_url: '<?php echo $row['linkURL']; ?>',
                    link_type: '<?php echo $linkType; ?>',
                    sort_order: '<?php echo $row['sortOrder']; ?>'
                }));
                linkListEntriesContainer.find('.ccm-link-list-entry-<?php echo $bID; ?>:last-child div[data-field=entry-link-page-selector]').concretePageSelector({
                    'inputName': '<?php echo $view->field('internalLinkCID'); ?>[]', 
                    'cID': '<?php echo (1 == $linkType) ? intval($row['internalLinkCID']) : false ?>'
                });
                <?php
            }
        } ?>

        doSortCount();
        linkListEntriesContainer.find('select[data-field=entry-link-select]').trigger('change');

        $('.ccm-add-link-list-entry-<?php echo $bID; ?>').click(function() {
            var thisModal = $(this).closest('.ui-dialog-content');
            linkListEntriesContainer.append(_templateLinkItem({
                link_text: '',
                link_url: '',
                cID: '',
                link_type: 0,
                sort_order: ''
            }));

            var newLinkItem = $('.ccm-link-list-entry-<?php echo $bID; ?>').last();
            thisModal.scrollTop(newLinkItem.offset().top);
            attachDelete(newLinkItem.find('.ccm-delete-link-list-entry-<?php echo $bID; ?>'));
            newLinkItem.find('div[data-field=entry-link-page-selector-select]').concretePageSelector({
                'inputName': '<?php echo $view->field('internalLinkCID'); ?>[]'
            });
            doSortCount();
        });

        $('.ccm-link-list-entries-<?php echo $bID; ?>').sortable({
            placeholder: "ui-state-highlight",
            axis: "y",
            handle: "i.fa-arrows",
            cursor: "move",
            update: function() {
                doSortCount();
            }
        });

        attachDelete($('.ccm-delete-link-list-entry-<?php echo $bID; ?>'));
    });
</script>
<style>
    .ccm-link-list-block-container input[type="text"],
    .ccm-link-list-block-container textarea {
        display: block;
        width: 100%;
    }
    .ccm-link-list-block-container .btn-success {
        margin-bottom: 20px;
    }
    .ccm-link-list-entries {
        padding-bottom: 30px;
        position: relative;
    }
    .ccm-link-list-entry {
        position: relative;
        min-height: 50px;
        padding: 10px;
        margin-bottom: 10px;
        background-color: #f5f5f5;
        border: 1px solid #e3e3e3;
        border-radius: 4px;
        -moz-box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
        box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
    }
    .ccm-link-list-entry .form-group {
        margin: 0px 0px 10px !important;
        padding: 0px!important;
        margin-right: 0px !important;
        border-bottom: none !important;
    }
    .btn.ccm-delete-link-list-entry {
        position: absolute;
        top: 10px;
        right: 41px;
    }
    .ccm-link-list-block-container i:hover {
        color: #428bca;
    }
    .ccm-link-list-block-container i.fa-arrows {
        position: absolute;
        top: 6px;
        right: 5px;
        cursor: move;
        font-size: 20px;
        padding: 5px;
    }
    .ccm-link-list-block-container .ui-state-highlight {
        height: 94px;
        margin-bottom: 15px;
    }
    .ccm-link-list-entries .ui-sortable-helper {
        -webkit-box-shadow: 0px 10px 18px 2px rgba(54,55,66,0.27);
        -moz-box-shadow: 0px 10px 18px 2px rgba(54,55,66,0.27);
        box-shadow: 0px 10px 18px 2px rgba(54,55,66,0.27);
    }
    .ccm-link-list-block-container .show-item-link {
        display: block;
    }
    .ccm-link-list-block-container .hide-item-link {
        display: none;
    }
</style>

<div class="ccm-link-list-block-container">
    <div class="ccm-link-list-entries ccm-link-list-entries-<?php echo $bID; ?>"></div>
    <div>
        <button type="button" class="btn btn-success ccm-add-link-list-entry ccm-add-link-list-entry-<?php echo $bID; ?>"><?php echo t('Add Link'); ?></button>
    </div>
</div>

<script type="text/template" id="linkListTemplate-<?php echo $bID; ?>">
    <div class="ccm-link-list-entry ccm-link-list-entry-<?php echo $bID; ?>">
        <div class="form-group" >
            <label class="control-label"><?php echo t('Link Type'); ?></label>
            <select data-field="entry-link-select" name="<?php echo $view->field('linkType'); ?>[]" class="form-control" style="width: 50%;">
                <option value="0" <% if (!link_type) { %>selected<% } %>><?php echo t('None'); ?></option>
                <option value="1" <% if (link_type == 1) { %>selected<% } %>><?php echo t('Another Page'); ?></option>
                <option value="2" <% if (link_type == 2) { %>selected<% } %>><?php echo t('External URL'); ?></option>
            </select>
        </div>
        <div data-field="entry-link-text" class="form-group hide-item-link">
            <label class="control-label"><?php echo t('Link Text:'); ?></label>
            <input class="form-control ccm-input-text" type="text" name="<?php echo $view->field('linkText'); ?>[]" value="<%= link_text %>" />
        </div>
        <div data-field="entry-link-url" class="form-group hide-item-link">
            <label class="control-label"><?php echo t('Link URL:'); ?></label>
            <textarea class="form-control" name="<?php echo $view->field('linkURL'); ?>[]"><%=link_url%></textarea>
        </div>
        <div data-field="entry-link-page-selector" class="form-group hide-item-link">
            <label class="control-label"><?php echo t('Choose Page:'); ?></label>
            <div data-field="entry-link-page-selector-select"></div>
        </div>
        <button type="button" class="btn btn-sm btn-danger ccm-delete-link-list-entry ccm-delete-link-list-entry-<?php echo $bID; ?>"><?php echo t('Remove'); ?></button>
        <i class="fa fa-arrows"></i>

        <input class="ccm-link-list-entry-sort" type="hidden" name="<?php echo $view->field('sortOrder'); ?>[]" value="<%=sort_order%>"/>
    </div>
</script>
