<?php defined('C5_EXECUTE') or die('Access Denied.');
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$c = Page::getCurrentPage();
$f = File::getByID($fID);

if (is_object($f) && $f->getFileID()) {
    ?>
    <div class="block-teaser-info_box">
        <div class="teaser-image">
            <?php
            $imWidth = $f->getAttribute('width');
            $imHeight = $f->getAttribute('height');
            $cropWidth = 100;
            $cropHeight = (100 * $imHeight) / $imWidth;

            $img = $app->make('helper/image');
            $thumb = $img->getThumbnail($f, $cropWidth, (int)$cropHeight, true);

            $imgTag = new \HtmlObject\Image();
            $imgTag->src($thumb->src);
            $imgTag->class('img-responsive');
            $imgTag->alt($title ? $title : '');
            echo $imgTag;
            ?>
        </div>

        <div class="teaser-info">
            <?php if ($title) { ?>
                <h4><?= $title ?></h4>
            <?php } ?>

            <?php if ($paragraph) { echo $paragraph; } ?>

            <?php if ($linkURL && $linkText) { ?>
                <a href="<?= $linkURL ?>" class="btn-box btn-style"><?= $linkText ?></a>
            <?php } ?>
        </div>
    </div>
    <?php
} elseif ($c->isEditMode()) {
    ?>
    <div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Teaser Info Block'); ?></div>
    <?php
}
