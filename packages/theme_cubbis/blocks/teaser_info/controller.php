<?php
namespace Concrete\Package\ThemeCubbis\Block\TeaserInfo;

use Concrete\Core\Block\BlockController;
use Concrete\Core\Editor\LinkAbstractor;
use Core;
use Error;
use File;
use Page;

class Controller extends BlockController
{
    protected $btInterfaceWidth = 600;
    protected $btInterfaceHeight = 600;
    protected $btTable = 'btCubbisTeaserInfo';
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btExportFileColumns = ['fID'];
    protected $btExportPageColumns = ['internalLinkCID'];
    protected $btDefaultSet = 'blocksetcubbis';

    public function getBlockTypeName()
    {
        return t('Teaser Info');
    }

    public function getBlockTypeDescription()
    {
        return t('Teaser widget with image, text and link');
    }

    public function add()
    {
        $this->edit();
    }

    public function edit()
    {
        $bf = null;
        if ($this->getFileID() > 0) {
            $bf = $this->getFileObject();
        }
        $this->set('bf', $bf);
        $this->set('paragraph', LinkAbstractor::translateFrom($this->paragraph));
    }

    public function view()
    {
        $f = $this->getFileObject();
        $this->set('f', $f);
        $this->set('linkURL', $this->getLinkURL());
        $this->set('paragraph', LinkAbstractor::translateFrom($this->paragraph));
    }

    public function delete()
    {
        parent::delete();
    }

    public function save($args)
    {
        $args = $args + [
            'fID' => 0,
        ];
        $args['fID'] = $args['fID'] != '' ? $args['fID'] : 0;
        $args['paragraph'] = LinkAbstractor::translateTo($args['paragraph']);

        switch (isset($args['linkType']) ? intval($args['linkType']) : 0) {
            case 1:
                $args['externalLink'] = '';
                break;
            case 2:
                $args['internalLinkCID'] = 0;
                break;
            default:
                $args['externalLink'] = '';
                $args['internalLinkCID'] = 0;
                break;
        }
        unset($args['linkType']);
        parent::save($args);
    }

    public function validate($args)
    {
        $e = $this->app->make('helper/validation/error');

        if (!$args['fID']) {
            $e->add(t('Please select an image.'));
        }

        return $e;
    }

    public function getImageFeatureDetailFileObject()
    {
        $db = $this->app->make('database')->connection();

        $file = null;
        $fID = $db->fetchColumn('SELECT fID FROM btCubbisTeaserInfo WHERE bID = ?', [$this->bID], 0);
        if ($fID) {
            $f = File::getByID($fID);
            if (is_object($f) && $f->getFileID()) {
                $file = $f;
            }
        }

        return $file;
    }

    public function getFileID()
    {
        return isset($this->fID) ? $this->fID : null;
    }

    public function getFileObject()
    {
        return File::getByID($this->getFileID());
    }

    public function getLinkURL()
    {
        $linkUrl = '';
        if (!empty($this->externalLink)) {
            $sec = $this->app->make('helper/security');
            $linkUrl = $sec->sanitizeURL($this->externalLink);
        } elseif (!empty($this->internalLinkCID)) {
            $linkToC = Page::getByID($this->internalLinkCID);
            if (is_object($linkToC) && !$linkToC->isError()) {
                $linkUrl = $linkToC->getCollectionLink();
            }
        }

        return $linkUrl;
    }
}
