<?php defined('C5_EXECUTE') or die('Access Denied.');
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$al = $app->make('helper/concrete/asset_library');
?>

<div class="form-group">
    <?php
    echo $form->label('ccm-b-image', t('Image'));
    echo $al->image('ccm-b-image', 'fID', t('Choose Image'), $bf);
    ?>
</div>

<div class="form-group">
    <?php
    echo $form->label('title', t('Title'));
    echo $form->text('title', isset($title) ? $title : '', ['maxlength' => 255]);
    ?>
</div>

<div class="form-group">
    <?= $form->label('paragraph', t('Paragraph')); ?>
    <?php
        $editor = Core::make('editor');
        echo $editor->outputBlockEditModeEditor('paragraph', $paragraph);
    ?>
</div>

<div class="form-group">
    <select name="linkType" data-select="feature-link-type" class="form-control">
        <option value="0" <?=(empty($externalLink) && empty($internalLinkCID) ? 'selected="selected"' : '')?>><?=t('None')?></option>
        <option value="1" <?=(empty($externalLink) && !empty($internalLinkCID) ? 'selected="selected"' : '')?>><?=t('Another Page')?></option>
        <option value="2" <?=(!empty($externalLink) ? 'selected="selected"' : '')?>><?=t('External URL')?></option>
    </select>
</div>

<div data-select-contents="feature-link-text" style="display: none;" class="form-group">
    <?= $form->label('linkText', t('Link Text')) ?>
    <?= $form->text('linkText', $linkText); ?>
</div>

<div data-select-contents="feature-link-type-internal" style="display: none;" class="form-group">
    <?= $form->label('internalLinkCID', t('Choose Page:')) ?>
    <?= Loader::helper('form/page_selector')->selectPage('internalLinkCID', $internalLinkCID); ?>
</div>

<div data-select-contents="feature-link-type-external" style="display: none;" class="form-group">
    <?= $form->label('externalLink', t('URL')) ?>
    <?= $form->text('externalLink', $externalLink); ?>
</div>

<script type="text/javascript">
$(function() {
    $('select[data-select=feature-link-type]').on('change', function() {
       if ($(this).val() == '0') {
           $('div[data-select-contents=feature-link-text]').hide();
           $('div[data-select-contents=feature-link-type-internal]').hide();
           $('div[data-select-contents=feature-link-type-external]').hide();
       }
       if ($(this).val() == '1') {
           $('div[data-select-contents=feature-link-text]').show();
           $('div[data-select-contents=feature-link-type-internal]').show();
           $('div[data-select-contents=feature-link-type-external]').hide();
       }
       if ($(this).val() == '2') {
           $('div[data-select-contents=feature-link-text]').show();
           $('div[data-select-contents=feature-link-type-internal]').hide();
           $('div[data-select-contents=feature-link-type-external]').show();
       }
    }).trigger('change');
});
</script>
