<?php
namespace Concrete\Package\ThemeCubbis\Block\VideoBackground;

use Concrete\Core\Block\BlockController;
use Concrete\Core\Editor\LinkAbstractor;
use Core;
use File;

class Controller extends BlockController
{
	protected $btTable = 'btCubbisVideoBackground';
	protected $btInterfaceWidth = 600;
	protected $btInterfaceHeight = 550;
	protected $btCacheBlockOutput = true;
	protected $btCacheBlockOutputOnPost = true;
	protected $btWrapperClass = 'ccm-ui';
	protected $btExportFileColumns = ['fID'];
	protected $btIgnorePageThemeGridFrameworkContainer = true;
	protected $btDefaultSet = 'blocksetcubbis';

	public function getBlockTypeName()
	{
		return t('Video Background');
	}

	public function getBlockTypeDescription()
	{
		return t('Video widget with autoplay in background and text in the center');
	}

	public function add()
	{
		$this->edit();
	}

	public function edit()
	{
		$this->set('paragraph', LinkAbstractor::translateFrom($this->paragraph));
	}

	public function view()
	{
		$file = $this->getFileObject();
		$this->set('fileURL', $file === null ? '' : $file->getURL());
		$this->set('paragraph', LinkAbstractor::translateFrom($this->paragraph));
	}

	public function delete()
	{
		parent::delete();
	}

	public function save($args)
	{
		$args['fID'] = $args['fID'] != '' ? $args['fID'] : 0;
		$args['color'] = $args['color'] != '' ? $args['color'] : '#ffffff';
		$args['paragraph'] = LinkAbstractor::translateTo($args['paragraph']);
		parent::save($args);
	}

	public function validate($args)
	{
		$error = Core::make('helper/validation/error');

		if (!$args['fID']) {
			$error->add(t('Please add a video file'));
		}

		return $error;
	}

	public function getFileObject()
	{
		return ($id = $this->getFileID()) ? File::getByID($id) : null;
	}

	public function getFileID()
	{
		return isset($this->fID) ? (int) $this->fID : 0;
	}

}
