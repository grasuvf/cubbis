<?php defined('C5_EXECUTE') or die('Access Denied.');
$c = Page::getCurrentPage();

if ($c->isEditMode()) {
	?>
	<div class="ccm-edit-mode-disabled-item" style="width: 100%; height: 100px; margin-bottom: 30px;">
		<div style="padding: 30px 0px 0px 0px"><?= t('Video background disabled in edit mode.') ?></div>
	</div>
	<?php
} else { 
	?>
	<div class="block-video-background">
		<video autoplay="true" 
					 loop="true" 
					 muted="true" 
					 allowfullscreen="true" 
					 playsinline="true"><source src="<?= $fileURL; ?>" 
					 														type="video/mp4"><?= t("Your browser doesn't support the HTML5 video tag."); ?></video>
		<?php if ($paragraph) { ?>
			<div class="video-text" style="color: <?= $color ?>; border: 5px solid <?= $color ?>50"><?= $paragraph ?></div>
		<?php } ?>
	</div>
	<?php
} ?>
