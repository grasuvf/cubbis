<?php defined('C5_EXECUTE') or die('Access Denied.');
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$al = $app->make('helper/concrete/asset_library');

$file = $controller->getFileID() > 0 ? $controller->getFileObject() : null;
?>

<div class="form-group">
    <label class="control-label"><?= t('Video File'); ?></label>
    <?= $al->video('ccm-video-file', 'fID', t('Choose Video File'), $file); ?>
</div>

<div class="form-group">
    <label class="control-label"><?= t('Text color'); ?></label>
    <?php
        $colorPicker = Core::make('helper/form/color');
        $colorPicker->output('color', $color ? $color : '#ffffff', array('preferredFormat' => 'hex'));
    ?>
</div>

<div class="form-group">
    <label class="control-label">Text</label>
    <?php
        $editor = Core::make('editor');
        echo $editor->outputBlockEditModeEditor('paragraph', $paragraph);
    ?>
</div>
