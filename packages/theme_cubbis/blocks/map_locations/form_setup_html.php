<?php defined('C5_EXECUTE') or die("Access Denied.");

$fp = FilePermissions::getGlobal();
$tp = new TaskPermission();

$getString = Core::make('helper/validation/identifier')->getString(18);
$tabs = [
    ['settings-' . $getString, t('Settings'), true],
    ['locations-' . $getString, t('Locations')],
];
echo Core::make('helper/concrete/ui')->tabs($tabs);
?>
<script>
    var CCM_EDITOR_SECURITY_TOKEN = "<?php echo Core::make('helper/validation/token')->generate('editor'); ?>";
    <?php
    $editorJavascript = Core::make('editor')->outputStandardEditorInitJSFunction();
    ?>
    var launchEditor = <?= $editorJavascript; ?>;
    $(document).ready(function() {
        var ccmReceivingEntry = '';
        var locationEntriesContainer = $('.ccm-map-locations-entries-<?php echo $bID; ?>');
        var _templateLocations = _.template($('#locationTemplate-<?php echo $bID; ?>').html());

        var attachDelete = function($obj) {
            $obj.click(function() {
                var deleteIt = confirm('<?php echo t('Are you sure?'); ?>');
                if (deleteIt === true) {
                    var locationID = $(this).closest('.ccm-map-locations-entry').find('.editor-content').attr('id');
                    if (typeof CKEDITOR === 'object') {
                        CKEDITOR.instances[locationID].destroy();
                    }

                    $(this).closest('.ccm-map-locations-entry-<?php echo $bID; ?>').remove();
                    doSortCount();
                }
            });
        };

        var attachFileManagerLaunch = function($obj) {
            $obj.click(function() {
                var oldLauncher = $(this);
                ConcreteFileManager.launchDialog(function(data) {
                    ConcreteFileManager.getFileDetails(data.fID, function(r) {
                        jQuery.fn.dialog.hideLoader();
                        var file = r.files[0];
                        oldLauncher.html(file.resultsThumbnailImg);
                        oldLauncher.next('.image-fID').val(file.fID);
                    });
                });
            });
        };

        var doSortCount = function() {
            $('.ccm-map-locations-entry-<?php echo $bID; ?>').each(function(index) {
                $(this).find('.ccm-map-locations-entry-sort').val(index);
            });
        };

        <?php if ($rows) {
          foreach ($rows as $row) {
            ?>
              locationEntriesContainer.append(_templateLocations({
                  fID: '<?php echo $row['fID']; ?>',
                  <?php if (File::getByID($row['fID'])) {
                    ?>
                    image_url: '<?php echo File::getByID($row['fID'])->getThumbnailURL('file_manager_listing'); ?>',
                    <?php
                  } else {
                    ?>
                    image_url: '',
                    <?php
                  } ?>
                  coordinates: '<?php echo addslashes(h($row['coordinates'])); ?>',
                  title: '<?php echo addslashes(h($row['title'])); ?>',
                  address: '<?php echo addslashes(h($row['address'])); ?>',
                  telephone: '<?php echo addslashes(h($row['telephone'])); ?>',
                  email: '<?php echo addslashes(h($row['email'])); ?>',
                  description: '<?php echo str_replace(["\t", "\r", "\n"], "", addslashes(h($row['description']))); ?>',
                  sort_order: '<?php echo $row['sortOrder']; ?>'
              }));
            <?php
            }
        } ?>

        doSortCount();
        $('.ccm-add-map-locations-entry-<?php echo $bID; ?>').click(function() {
            var thisModal = $(this).closest('.ui-dialog-content');
            locationEntriesContainer.append(_templateLocations({
                fID: '',
                coordinates: '',
                title: '',
                address: '',
                telephone: '',
                email: '',
                description: '',
                image_url: '',
                sort_order: '',
            }));

            $('.ccm-map-locations-entry-<?php echo $bID; ?>').not('.location-closed').each(function() {
                $(this).addClass('location-closed');
                var thisEditButton = $(this).closest('.ccm-map-locations-entry-<?php echo $bID; ?>').find('.btn.ccm-edit-location');
                thisEditButton.text(thisEditButton.data('locationEditText'));
            });
            var newLocation = $('.ccm-map-locations-entry-<?php echo $bID; ?>').last();
            var closeText = newLocation.find('.btn.ccm-edit-location').data('locationCloseText');
            newLocation.removeClass('location-closed').find('.btn.ccm-edit-location').text(closeText);

            thisModal.scrollTop(newLocation.offset().top);
            launchEditor(newLocation.find('.editor-content'));
            attachDelete(newLocation.find('.ccm-delete-map-locations-entry-<?php echo $bID; ?>'));
            attachFileManagerLaunch(newLocation.find('.ccm-pick-location-image'));
            doSortCount();
        });

        $('.ccm-map-locations-entries-<?php echo $bID; ?>').on('click','.ccm-edit-location', function() {
            $(this).closest('.ccm-map-locations-entry-<?php echo $bID; ?>').toggleClass('location-closed');
            var thisEditButton = $(this);
            if (thisEditButton.data('locationEditText') === thisEditButton.text()) {
                thisEditButton.text(thisEditButton.data('locationCloseText'));
            } else if (thisEditButton.data('locationCloseText') === thisEditButton.text()) {
                thisEditButton.text(thisEditButton.data('locationEditText'));
            }
        });

        attachDelete($('.ccm-delete-map-locations-entry-<?php echo $bID; ?>'));
        attachFileManagerLaunch($('.ccm-pick-location-image-<?php echo $bID; ?>'));
        $(function() {  // activate editors
            if ($('.editor-content-<?php echo $bID; ?>').length) {
                launchEditor($('.editor-content-<?php echo $bID; ?>'));
            }
        });
    });
</script>
<style>
    .ccm-map-locations-block-container input[type="text"] {
        height: 35px;
    }
    .ccm-map-locations-block-container input[type="text"],
    .ccm-map-locations-block-container textarea {
        display: block;
        width: 100%;
    }
    .ccm-map-locations-block-container .btn-success {
        margin-bottom: 20px;
    }
    .ccm-map-locations-entries {
        padding-bottom: 30px;
        position: relative;
    }
    .ccm-pick-location-image {
        padding: 5px;
        cursor: pointer;
        background: #dedede;
        border: 1px solid #cdcdcd;
        text-align: center;
        vertical-align: middle;
        width: 72px;
        height: 72px;
        display: table-cell;
    }
    .ccm-pick-location-image img {
        max-width: 100%;
    }
    .ccm-map-locations-entry {
        position: relative;
    }
    .ccm-map-locations-entry .form-group {
        margin: 0px 0px 20px !important;
        margin-right: 0px !important;
        border-bottom: none !important;
        padding: 0px!important;
    }
    .ccm-map-locations-entry .cke_contents {
        height: 150px !important;
    }
    .ccm-map-locations-block-container .location-well {
        min-height: 20px;
        padding: 40px 10px 10px;
        margin-bottom: 10px;
        background-color: #f5f5f5;
        border: 1px solid #e3e3e3;
        border-radius: 4px;
        -moz-box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
        -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
        box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
    }
    .ccm-map-locations-block-container .location-well.location-closed {
        padding: 25px 10px;
    }
    .ccm-map-locations-entry.location-closed .form-group {
        display: none;
    }
    .ccm-map-locations-entry.location-closed .form-group:first-of-type {
        display: block!important;
        margin-bottom: 0px!important;
    }
    .ccm-map-locations-entry.location-closed .form-group:first-of-type input {
        width: 60px;
        width: -webkit-calc(100% - 205px);
        width:   expression(100% - 205px);
        width:    -moz-calc(100% - 205px);
        width:      -o-calc(100% - 205px);
        width:         calc(100% - 205px);
    }
    .ccm-map-locations-entry.location-closed .form-group:first-of-type label {
        display: none;
    }
    .btn.ccm-edit-location {
        position: absolute;
        top: 25px;
        right: 98px;
    }
    .btn.ccm-delete-map-locations-entry {
        position: absolute;
        top: 25px;
        right: 12px;
    }
    .ccm-map-locations-block-container .ui-state-highlight {
        height: 94px;
        margin-bottom: 15px;
    }
</style>

<div id="ccm-tab-content-settings-<?php echo $getString; ?>" class="ccm-tab-content">
    <p style="color: #daa520">
    This widget is using <a href="https://leafletjs.com/index.html">Leaflet</a>, an open-source JavaScript library for interactive maps.
    If you don't set an API access token from a tileprovider like <a href="https://account.mapbox.com/">Mapbox</a>, 
    it will use a free tileprovider from <a href="https://www.openstreetmap.org/">OpenStreetMap</a>. 
    </p>
    <div class="form-group">
        <?php
        echo $form->label('mapApiKey', t('Map Api Key'));
        echo $form->text('mapApiKey', isset($mapApiKey) ? $mapApiKey : '');
        ?>
    </div>
    <div class="form-group">
        <?php
        echo $form->label('mapCenterCoordinates', t('Map Center Coordinates: latitude,longitude'));
        echo $form->text('mapCenterCoordinates', isset($mapCenterCoordinates) ? $mapCenterCoordinates : '45.8767667,23.9205025');
        ?>
    </div>
    <div class="form-group">
        <?php
        echo $form->label('mapZoom', t('Map Zoom'));
        echo $form->text('mapZoom', isset($mapZoom) ? $mapZoom : 7.5);
        ?>
    </div>
    <div class="form-group">
        <?php
        echo $form->label('mapBoxHeight', t('Map Box Height (px)'));
        echo $form->number('mapBoxHeight', isset($mapBoxHeight) ? $mapBoxHeight : 500, ['min' => '100']);
        ?>
    </div>
</div>

<div id="ccm-tab-content-locations-<?php echo $getString; ?>" class="ccm-tab-content">
    <div class="ccm-map-locations-block-container">
        <div class="ccm-map-locations-entries ccm-map-locations-entries-<?php echo $bID; ?>"></div>
        <div>
            <button type="button" class="btn btn-success ccm-add-map-locations-entry ccm-add-map-locations-entry-<?php echo $bID; ?>"><?php echo t('Add Location'); ?></button>
        </div>
    </div>
</div>

<script type="text/template" id="locationTemplate-<?php echo $bID; ?>">
    <div class="ccm-map-locations-entry ccm-map-locations-entry-<?php echo $bID; ?> location-well location-closed">
        <div class="form-group">
            <label class="control-label"><?php echo t('Title'); ?></label>
            <input class="form-control ccm-input-text" type="text" name="<?php echo $view->field('title'); ?>[]" value="<%=title%>" />
        </div>
        <div class="form-group">
            <label class="control-label"><?php echo t('Coordinates: latitude,longitude'); ?></label>
            <input class="form-control ccm-input-text" type="text" name="<?php echo $view->field('coordinates'); ?>[]" value="<%=coordinates%>" />
        </div>
        <div class="form-group">
            <label class="control-label"><?php echo t('Address'); ?></label>
            <input class="form-control ccm-input-text" type="text" name="<?php echo $view->field('address'); ?>[]" value="<%=address%>" />
        </div>
        <div class="form-group">
            <label class="control-label"><?php echo t('Telephone'); ?></label>
            <input class="form-control ccm-input-text" type="text" name="<?php echo $view->field('telephone'); ?>[]" value="<%=telephone%>" />
        </div>
        <div class="form-group">
            <label class="control-label"><?php echo t('Email'); ?></label>
            <input class="form-control ccm-input-text" type="email" name="<?php echo $view->field('email'); ?>[]" value="<%=email%>" />
        </div>
        <div class="form-group">
            <label class="control-label"><?php echo t('Description'); ?></label>
            <div class="editor-edit-content"></div>
            <textarea id="ccm-location-editor-<%= _.uniqueId() %>" style="display: none" class="editor-content editor-content-<?php echo $bID; ?>" name="<?php echo $view->field('description'); ?>[]"><%=description%></textarea>
        </div>
        <div class="form-group">
            <label class="control-label"><?php echo t('Image'); ?></label>
            <div class="ccm-pick-location-image ccm-pick-location-image-<?php echo $bID; ?>">
                <% if (image_url.length > 0) { %>
                    <img src="<%= image_url %>" />
                <% } else { %>
                    <i class="fa fa-picture-o"></i>
                <% } %>
            </div>
            <input type="hidden" name="<?php echo $view->field('fID'); ?>[]" class="image-fID" value="<%=fID%>" />
        </div>
        <button type="button" class="btn btn-sm btn-default ccm-edit-location ccm-edit-location-<?php echo $bID; ?>" data-location-close-text="<?php echo t('Collapse Location'); ?>" data-location-edit-text="<?php echo t('Edit Location'); ?>"><?php echo t('Edit Location'); ?></button>
        <button type="button" class="btn btn-sm btn-danger ccm-delete-map-locations-entry ccm-delete-map-locations-entry-<?php echo $bID; ?>"><?php echo t('Remove'); ?></button>
        
        <input class="ccm-map-locations-entry-sort" type="hidden" name="<?php echo $view->field('sortOrder'); ?>[]" value="<%=sort_order%>"/>
    </div>
</script>
