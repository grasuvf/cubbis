<?php defined('C5_EXECUTE') or die('Access Denied.');
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$c = Page::getCurrentPage();

if ($c->isEditMode()) {
    ?>
	<div class="ccm-edit-mode-disabled-item" style="width: 100%; height: 100px; margin-bottom: 30px;">
		<div style="padding: 30px 0px 0px 0px"><?=t('Map Locations disabled in edit mode.')?></div>
	</div>
    <?php 
} else {
    ?>
    <div class="block-map-locations">
        <div id="mapID" style="height: <?= $mapBoxHeight ?>px"></div>

        <script>
        $(document).ready(function() {
            var map = new L.Map('mapID', {
                center: [<?= $mapCenterCoordinates ?>],
                zoom: <?= $mapZoom ?>,
                layers: [
                <?php if (!empty($mapApiKey)) { ?>
                    new L.TileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token=<?= $mapApiKey ?>', {
                        id: 'mapbox/streets-v11',
                        attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>'
                        })
                <?php } else { ?>
                    new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                        'attribution': 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors'
                        })
                <?php } ?>
                ]
            });

            <?php if (count($rows) > 0) {
                foreach ($rows as $k => $row) { ?>
                    var marker<?= $k ?> = L.marker([<?= $row['coordinates'] ?>]).addTo(map);

                    <?php
                    $f = File::getByID($row['fID']);
                    $hasImage = is_object($f) && $f->getFileID();
                    if ($hasImage) {
                        $im = $app->make('helper/image');
                        $thumb = $im->getThumbnail($f, 150, 150, true);
                        $imgTag = new \HtmlObject\Image();
                        $imgTag->src($thumb->src);
                        $imgTag->alt($row['title'] ? $row['title'] : '');
                    } ?>
                    
                    marker<?= $k ?>.bindPopup(`
                    <div class="location-popup clearfix">
                        <?php if ($hasImage) { ?>
                            <div class="location-image">
                                <?= $imgTag ?>
                            </div>
                        <?php } ?>
                        <div class="location-info <?= !$hasImage ? 'fullwidth' : '' ?>">
                            <h3><?= $row['title'] ?></h3>
                            <?php if (!empty($row['address'])) { ?>
                                <span class="address"><?= $row['address'] ?></span>
                            <?php } ?>
                            <?php if (!empty($row['telephone'])) { ?>
                                <a href="tel:<?= $row['telephone'] ?>" class="telephone"><?= $row['telephone'] ?></a>
                            <?php } ?>
                            <?php if (!empty($row['email'])) { ?>
                                <a href="mailto:<?= $row['email'] ?>" class="email"><?= $row['email'] ?></a>
                            <?php } ?>
                            <?php if (!empty($row['description'])) { ?>
                                <div class="details"><?= $row['description'] ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    `);
                <?php 
                }
            } ?>
        });
        </script>
    </div>
    <?php  
}
?>
