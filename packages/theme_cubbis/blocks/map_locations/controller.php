<?php
namespace Concrete\Package\ThemeCubbis\Block\MapLocations;

use Concrete\Core\Block\BlockController;
use Concrete\Core\Editor\LinkAbstractor;
use Core;
use Page;

class Controller extends BlockController
{
    protected $btTable = 'btCubbisMapLocations';
    protected $btInterfaceWidth = 550;
    protected $btInterfaceHeight = 600;
    protected $btWrapperClass = 'ccm-ui';
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btIgnorePageThemeGridFrameworkContainer = true;
    protected $btExportTables = ['btCubbisMapLocations', 'btCubbisMapLocationsEntries'];
    protected $btDefaultSet = 'blocksetcubbis';

    public function getBlockTypeName()
    {
        return t('Map Locations');
    }

    public function getBlockTypeDescription()
    {
        return t('Interactive Map widget with custom marker locations');
    }

    public function registerViewAssets($outputContent = '')
    {
        $this->requireAsset('javascript', 'jquery');

        $c = Page::getCurrentPage();
        if (!$c->isEditMode()) {
            $this->addHeaderItem(
                '<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css" integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ==" crossorigin=""/>'
            );
            $this->addFooterItem(
                '<script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js" integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew==" crossorigin=""></script>'
            );
        }
    }

    public function add()
    {
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
    }

    public function edit()
    {
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $db = $this->app->make('database')->connection();
        $query = $db->fetchAll('SELECT * FROM btCubbisMapLocationsEntries WHERE bID = ? ORDER BY sortOrder', [$this->bID]);
        $this->set('rows', $query);
    }

    public function view()
    {
        $db = $this->app->make('database')->connection();
        $query = $db->fetchAll('SELECT * FROM btCubbisMapLocationsEntries WHERE bID = ? ORDER BY sortOrder', [$this->bID]);

        $rows = [];
        foreach ($query as $row) {
            $row['description'] = LinkAbstractor::translateFrom($row['description']);
            $rows[] = $row;
        }

        $this->set('rows', $rows);
    }

    public function duplicate($newBID)
    {
        parent::duplicate($newBID);
        $db = $this->app->make('database')->connection();
        $v = [$this->bID];
        $q = 'SELECT * FROM btCubbisMapLocationsEntries WHERE bID = ?';
        $r = $db->executeQuery($q, $v);
        foreach ($r as $row) {
            $db->executeQuery(
                'INSERT INTO btCubbisMapLocationsEntries (bID, coordinates, title, address, telephone, email, description, fID, sortOrder) VALUES(?,?,?,?,?,?,?,?,?)',
                [
                    $newBID,
                    $row['coordinates'],
                    $row['title'],
                    $row['address'],
                    $row['telephone'],
                    $row['email'],
                    $row['description'],
                    $row['fID'],
                    $row['sortOrder'],
                ]
            );
        }
    }

    public function delete()
    {
        $db = $this->app->make('database')->connection();
        $db->executeQuery('DELETE FROM btCubbisMapLocationsEntries WHERE bID = ?', [$this->bID]);
        parent::delete();
    }

    public function save($args)
    {
        $args += [
            'mapZoom' => 7.5,
            'mapBoxHeight' => 500,
            'mapCenterCoordinates' => '45.8767667,23.9205025',
        ];
        $args['mapZoom'] = (int) $args['mapZoom'];
        $args['mapBoxHeight'] = (int) $args['mapBoxHeight'];
        $args['mapCenterCoordinates'] = $args['mapCenterCoordinates'];

        $db = $this->app->make('database')->connection();
        $db->executeQuery('DELETE FROM btCubbisMapLocationsEntries WHERE bID = ?', [$this->bID]);
        parent::save($args);

        $count = isset($args['sortOrder']) ? count($args['sortOrder']) : 0;
        $i = 0;
        while ($i < $count) {
            if (isset($args['description'][$i])) {
                $args['description'][$i] = LinkAbstractor::translateTo($args['description'][$i]);
            }

            $db->executeQuery(
                'INSERT INTO btCubbisMapLocationsEntries (bID, coordinates, title, address, telephone, email, description, fID, sortOrder) VALUES(?,?,?,?,?,?,?,?,?)',
                [
                    $this->bID,
                    $args['coordinates'][$i],
                    $args['title'][$i],
                    $args['address'][$i],
                    $args['telephone'][$i],
                    $args['email'][$i],
                    $args['description'][$i],
                    (int) $args['fID'][$i],
                    $args['sortOrder'][$i],
                ]
            );
            ++$i;
        }
    }

    public function validate($args)
    {
        $error = Core::make('helper/validation/error');
        $mcc = explode(",", $args['mapCenterCoordinates']);

        if (!is_numeric((float)$mcc[0]) && !is_numeric((float)$mcc[1]) && !(float)$mcc[0] > 0 && !(float)$mcc[1] > 0) {
            $error->add(t('Missing Map Coordinates: latitude,longitude'));
        }

        if (!is_numeric($args['mapZoom'])) {
            $error->add(t('Map Zoom should be a positive number'));
        }

        return $error;
    }
}
