<?php defined('C5_EXECUTE') or die('Access Denied.');
$c = Page::getCurrentPage();

if ($c->isEditMode()) {
  echo '<div class="ccm-edit-mode-disabled-item" style="width:100%;height:35px;padding:5px 10px">'. t('Spacer block') .'</div>';
} else {
  if ($spacerDesktop > 0 && $spacerDesktop == $spacerMobile) {
    echo '<div style="height:'.$spacerDesktop.'px"></div>';
  } else {
    if ($spacerDesktop > 0) {
      echo '<div style="height:'.$spacerDesktop.'px" class="visible-md visible-lg"></div>';
    }
    if ($spacerMobile > 0) {
      echo '<div style="height:'.$spacerMobile.'px" class="visible-xs visible-sm"></div>';
    }
  }
} 
?>
