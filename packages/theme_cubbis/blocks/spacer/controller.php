<?php
namespace Concrete\Package\ThemeCubbis\Block\Spacer;

use Concrete\Core\Block\BlockController;
use Error;

class Controller extends BlockController
{
    protected $btInterfaceWidth = 350;
    protected $btInterfaceHeight = 350;
    protected $btTable = 'btCubbisSpacer';
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btDefaultSet = 'blocksetcubbis';

    public function getBlockTypeName()
    {
        return t('Spacer');
    }

    public function getBlockTypeDescription()
    {
        return t('Spacer widget with margin size in pixels for desktop and mobile');
    }

    public function delete()
    {
        parent::delete();
    }

    public function save($args)
    {
        $args['spacerDesktop'] = isset($args['spacerDesktop']) ? $args['spacerDesktop'] : 0;
        $args['spacerMobile'] = isset($args['spacerMobile']) ? $args['spacerMobile'] : 0;

        parent::save($args);
    }

    public function validate($args)
    {
        $e = $this->app->make('helper/validation/error');

        if (!is_numeric($args['spacerDesktop']) || !is_numeric($args['spacerMobile'])) {
            $e->add(t('Please insert a numeric value'));
        }

        return $e;
    }
}
