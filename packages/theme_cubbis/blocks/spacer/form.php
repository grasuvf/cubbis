<?php defined('C5_EXECUTE') or die('Access Denied.'); ?>

<div class="form-group">
    <?php
    echo $form->label('spacerDesktop', t('Spacer desktop (px)'));
    echo $form->number('spacerDesktop', $spacerDesktop ? $spacerDesktop : 0, ['min' => 0, 'max' => 100]);
    ?>
</div>

<div class="form-group">
    <?php
    echo $form->label('spacerMobile', t('Spacer mobile (px)'));
    echo $form->number('spacerMobile', $spacerMobile ? $spacerMobile : 0, ['min' => 0, 'max' => 100]);
    ?>
</div>
