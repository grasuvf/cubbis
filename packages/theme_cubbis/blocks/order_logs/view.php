<?php defined('C5_EXECUTE') or die("Access Denied.");
date_default_timezone_set('Europe/Bucharest');

$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$dh = $app->make('helper/date');
$c = Page::getCurrentPage();

if ($c->isEditMode()) {
  ?>
  <div class="ccm-edit-mode-disabled-item" style="width: 100%; height: 100px">
    <div style="padding: 30px 0px 0px 0px"><?= t('Order Logs disabled in edit mode.') ?></div>
  </div>
  <?php
} else { ?>
  <div class="container">
    <div class="order-logs-block order-list-container"
         data-action-open="<?= $this->action('order_list_open') ?>">
      <h2><?= t('Comenzile mele'); ?></h2>
      <?php if (count($orderLists) > 0) {
        foreach($orderLists as $key => $list) {
          $orderDate = $dh->date('d.m.Y H:i:s', $list['orderDate']);
          $orderPriceTotal = number_format((float)$list['orderPriceTotal'], 2, '.', '');
        ?>
        <div class="order-list" data-list-id="<?= $list['orderID'] ?>">
          <div class="order-list-heading clearfix">
            <div class="order-list-info">
              <h3><?= t('Comanda') ?> #<?= count($orderLists) - $key ?></h3>
              <span><?= t('Data') ?>: <?= $orderDate ?></span><span><?= t('Total') ?>: <?= $orderPriceTotal ?> lei</span>
            </div>
            <div class="order-list-actions">
              <button type="button" class="btn-style-icon toggle order-open"></button>
              <button type="button" class="btn-style-icon download order-download"></button>
            </div>
          </div>
          <div class="order-list-products"></div>
        </div>
        <?php 
        }
      } else { ?>
        <?php echo $textEmptyOrders; ?>
        <?php 
      } ?>

      <form id="formOrderDownload" method="POST" action="<?= $this->action('order_list_download') ?>">
        <input type="hidden" name="p_order_id" value="" />
      </form>
    </div>
  </div>
  <?php
} ?>
