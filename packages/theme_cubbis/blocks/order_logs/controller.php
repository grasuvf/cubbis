<?php
namespace Concrete\Package\ThemeCubbis\Block\OrderLogs;
use Concrete\Core\Block\BlockController;
use Concrete\Core\Database\Connection\Connection;
use Concrete\Core\Editor\LinkAbstractor;
use Concrete\Core\User\User;
use Page;

class Controller extends BlockController
{
	protected $btTable = 'btCubbisOrderLogs';
	protected $btInterfaceWidth = 500;
	protected $btInterfaceHeight = 550;
	protected $btWrapperClass = 'ccm-ui';
	protected $btCacheBlockRecord = false;
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btIgnorePageThemeGridFrameworkContainer = true;
	protected $btExportTables = ['btCubbisOrderLogs'];
	protected $btDefaultSet = 'blocksetcubbis';

	protected $userID = null;
	protected $appExternalFilePath = null;

	public function getBlockTypeName()
	{
		return t('Order Logs');
	}

	public function getBlockTypeDescription()
	{
		return t('Order Logs widget');
	}

	public function on_start()
	{
		$u = new User();
		$ui = $u->getUserInfoObject();
		$this->userID = $u->getUserID();
		$this->appExternalFilePath = $this->app->make('site')->getSite()->getAttribute('app_external_file_path');
	}

	public function registerViewAssets($outputContent = '')
	{
		$this->requireAsset('javascript', 'jquery');
		$this->requireAsset('javascript', 'lazy-sizes');
		$this->requireAsset('css', 'order-list');
		$this->requireAsset('toastr');

		$alertVars = "var errAlertOrder = '{$this->errorAlertOrder}';";
		$this->addFooterItem('<script type="text/javascript">'. $alertVars .'</script>');
	}

	public function add()
	{
		$this->edit();
	}

	public function edit()
	{
		$this->set('textEmptyOrders', LinkAbstractor::translateFrom($this->textEmptyOrders));
	}

	public function view()
	{
		$orderLists = $this->getOrderLists();
		$this->set('orderLists', $orderLists);
		$this->set('textEmptyOrders', LinkAbstractor::translateFrom($this->textEmptyOrders));
	}

	public function delete()
	{
		parent::delete();
	}

	public function save($args)
	{
		$args['textEmptyOrders'] = LinkAbstractor::translateTo($args['textEmptyOrders']);
		parent::save($args);
	}

	public function getOrderLists()
	{
		$db = $this->app->make('database')->connection();
		$query = $db->fetchAll('SELECT * FROM btCubbisOrderList WHERE uID = ? ORDER BY orderDate DESC', [$this->userID]);
		return $query;
	}

	public function setProductList($products=array(), $priceTotal=null) 
	{
		$html = '';
		$imgFilePath = $this->appExternalFilePath;
		if (count($products) > 0) {
			$html .= '<table>';
			$html .= 	'<thead>';
			$html .= 		'<tr>';
			$html .=			'<th class="title">'.t('Produs').'</th>';
			$html .=			'<th class="price">'.t('Pret unitar').'</th>';
			$html .=			'<th class="quantity">'.t('Cantitate').'</th>';
			$html .=			'<th class="subtotal">'.t('Subtotal').'</th>';
			$html .=		'</tr>';
			$html .=	'</thead>';
			$html .=	'<tbody>';
			foreach ($products as $product) {
				$price = number_format($product['price'],2,'.','');
				$subtotal = number_format(($product['quantity'] * $product['price']),2,'.','');
				$imgSrc = $imgFilePath.'/'.$product['filename'];
				
				$html .= 	'<tr>';
				$html .= 		'<td class="title">';
				if (!empty($product['filename'])) {
					$html .= 			'<img class="image lazyload" data-src="'.$imgSrc.'" />';
				} else {
					$html .= 			'<span class="image"></span>';
				}
				$html .= 			'<p>'.$product['name'].'<span>'.$product['provider'].'</span><span>'.$product['code'].'</span></p>';
				$html .= 		'</td>';
				$html .= 		'<td class="price">'.$price.' lei</td>';
				$html .= 		'<td class="quantity">'.$product['quantity'].'</td>';
				$html .= 		'<td class="subtotal">'.$subtotal.' lei</td>';
				$html .= 	'</tr>';
			}
			$html .= 		'<tr>';
			$html .= 			'<td colspan="2"></td>';
			$html .= 			'<td>'.t('Total').'</td>';
			$html .= 			'<td>'.number_format($priceTotal,2,'.','').' lei</td>';
			$html .= 		'</tr>';
			$html .= 	'</tbody>';
			$html .= '</table>';
		}
		return $html;
	}

	public function action_order_list_open()
	{
		$u = new User();
		if (!$u->isLoggedIn()) {
			header('HTTP/1.0 401 Unauthorized');
			echo 'not logged in';
			exit();
		}

		$orderID = $_POST['p_order_id'];
		$db = $this->app->make('database')->connection();
		$productList = $db->fetchAll('SELECT * FROM btCubbisOrderListEntries WHERE orderID = ?', [$orderID]);
		$orderPriceTotal = $db->fetchColumn('SELECT orderPriceTotal FROM btCubbisOrderList WHERE orderID = ?', [$orderID]);

		if (count($productList) > 0) {
			echo json_encode(array(
				'success' => true,
				'productList' => $this->setProductList($productList, $orderPriceTotal)
			));
		} else {
			header('HTTP/1.0 404 Not Found');
			echo 'list ID not found';
		}
		exit();
	}

	public function action_order_list_download()
	{
		$u = new User();
		if (!$u->isLoggedIn()) {
			header('HTTP/1.0 401 Unauthorized');
			echo 'not logged in';
			exit();
		}

		$orderID = $_POST['p_order_id'];
		$db = $this->app->make('database')->connection();
		$productList = $db->fetchAll('SELECT pID, code, name, provider, price, quantity FROM btCubbisOrderListEntries WHERE orderID = ?', [$orderID]);

		if (count($productList) > 0) {
			header('Pragma: public');
			header('Content-Type: text/csv');
			header('Cache-control: private');
			header('Content-Disposition: attachment; filename=comanda_cubbis_#'.$orderID.'.csv');

			echo "Produs ID, Cod, Nume, Furnizor, Pret unitar, Cantitate\n";

			$row = array();
			$fp = fopen('php://output', 'w');
			foreach($productList as $product) {
				$row = array($product['pID'],$product['code'],$product['name'],$product['provider'],$product['price'],$product['quantity']);
				fputcsv($fp, $row);
			}
			fclose($fp);
		} else {
			header('HTTP/1.0 404 Not Found');
			echo 'list ID not found';
		}
		exit();
	}
}
