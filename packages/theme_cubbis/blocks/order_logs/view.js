$(document).ready(function() {
  var orderList, orderID;
  // global js variables added from controller including PHP data
  var errorAlertOrder = errAlertOrder ? errAlertOrder : 'Ceva nu a mers bine. Mai incearca o data.';

  // toggle display or get order list
  $(document).on('click', '.order-list .order-open', function(e) {
    e.preventDefault();
    orderOpenAction(this);
  });

  // download order list as CSV
  $(document).on('click', '.order-list .order-download', function(e) {
    e.preventDefault();
    orderDownloadAction(this);
  });

  function orderOpenAction(_this) {
    orderList = $(_this).parents('.order-list');
    orderID = orderList.attr('data-list-id');
    if (orderList.hasClass('loaded')) {
      orderList.toggleClass('opened');
      orderList.find('.order-list-products').stop().slideToggle();
    } else {
      $.ajax({
        url: $('.order-logs-block').attr('data-action-open'),
        type: 'POST',
        dataType: 'json',
        data: $.param({'p_order_id': orderID}),
        beforeSend: function() {
          orderList.addClass('preloader spinner');
        },
        success: function(response) {
          if (response.success) {
            orderList.addClass('loaded opened');
            orderList.find('.order-list-products').html(response.productList).slideDown();
          } else {
            toastr.error(errorAlertOrder);
          }
        },
        error: function() {
          toastr.error(errorAlertOrder);
        },
        complete: function() {
          $('.order-logs-block .order-list').removeClass('preloader spinner');
        }
      });
    }
  }

  function orderDownloadAction(_this) {
    orderList = $(_this).parents('.order-list');
    orderID = orderList.attr('data-list-id');

    $("#formOrderDownload input").val(orderID);
    $("#formOrderDownload").trigger('submit'); 
  }
});
