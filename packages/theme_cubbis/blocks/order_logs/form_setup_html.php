<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<p style="color: #daa520">For a proper display, add this block directly to Main Content Area.</p>

<div class="form-group">
	<?php
		echo $form->label('errorAlertOrder', t('Error Alert on Open Order') . ' <small>(short text)</small>');
		echo $form->textarea(
			'errorAlertOrder', 
			$errorAlertOrder ? h($errorAlertOrder) : t('Ceva nu a mers bine. Incarca din nou pagina si mai incearca o data.'), 
			['rows' => 2]
		);
	?>
</div>
<div class="form-group rich-text">
	<?php
		$editor = Core::make('editor');
		echo $form->label('textEmptyOrders', t('Empty Order List Text'));
		echo $editor->outputBlockEditModeEditor('textEmptyOrders', $textEmptyOrders);
	?>
</div>

<style>
	.ccm-ui textarea {
		resize: none;
	}
	.ccm-ui .rich-text .cke_contents {
		height: 200px!important
	}
</style>