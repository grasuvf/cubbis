<?php defined('C5_EXECUTE') or die("Access Denied.");

$getBlockString = Core::make('helper/validation/identifier')->getString(18);
$tabsBlock = [
	['emailsLinks-' . $getBlockString, t('Emails and Links'), true],
	['notifications-' . $getBlockString, t('Notifications')],
];
$email_send_to_msg = "Send TO/FROM emails should not be Yahoo Mail addresses because Yahoo policy is to reject emails that aren't sent through Yahoo servers!";
$email_send_from_msg = "Send FROM email should be a 'noreply' email for the reason explained above!";
$link_order_send_msg = "Button seen by users who are not logged in";
$link_order_save_msg = "Button seen by by logged in users";
?>

<p style="color: #daa520">For a proper display, add this block directly to Main Content Area.</p>

<?php echo Core::make('helper/concrete/ui')->tabs($tabsBlock); ?>

<div id="ccm-tab-content-emailsLinks-<?= $getBlockString; ?>" class="ccm-tab-content">
	<div class="form-group">
		<?php
		echo $form->label(
			'orderEmailTo', 
			t('Order Send Email To'). ' ' .'<i class="fa fa-exclamation-circle red launch-tooltip" title="'.$email_send_to_msg.'"></i>'
		);
		echo $form->text('orderEmailTo', $orderEmailTo ? $orderEmailTo : 'comenzi@cubbis.ro');
		?>
	</div>
	<div class="form-group">
		<?php
		echo $form->label(
			'orderEmailFrom', 
			t('Order Send Email From'). ' ' .'<i class="fa fa-exclamation-circle red launch-tooltip" title="'.$email_send_from_msg.'"></i>'
		);
		echo $form->text('orderEmailFrom', $orderEmailFrom ? $orderEmailFrom : 'noreply@cubbis.ro');
		?>
	</div>
	<div class="form-group">
		<?php
		echo $form->label('orderBackLinkTitle', t('Continue Shopping Link Title'));
		echo $form->text('orderBackLinkTitle', $orderBackLinkTitle ? $orderBackLinkTitle : t('Continua cumparaturile'));
		?>
	</div>
	<div class="form-group">
		<?php
		echo $form->label(
			'orderSendLinkTitle', 
			t('Order Send Link Title'). ' ' .'<i class="fa fa-question-circle launch-tooltip" title="'.$link_order_send_msg.'"></i>'
		);
		echo $form->text('orderSendLinkTitle', $orderSendLinkTitle ? $orderSendLinkTitle : t('Trimite comanda'));
		?>
	</div>
	<div class="form-group">
		<?php
		echo $form->label(
			'orderSaveLinkTitle', 
			t('Order Send Link Title'). ' ' .'<i class="fa fa-question-circle launch-tooltip" title="'.$link_order_save_msg.'"></i>'
		);
		echo $form->text('orderSaveLinkTitle', $orderSaveLinkTitle ? $orderSaveLinkTitle : t('Salveaza si trimite comanda'));
		?>
	</div>
</div>

<div id="ccm-tab-content-notifications-<?= $getBlockString; ?>" class="ccm-tab-content">
	<div class="form-group">
		<?php
			echo $form->label('errorAlertCartUpdate', t('Error Alert on Cart Update') . ' <small>(short text)</small>');
			echo $form->textarea(
				'errorAlertCartUpdate',
				$errorAlertCartUpdate ? h($errorAlertCartUpdate) : t('Ceva nu a mers bine. Incarca din nou pagina si mai incearca o data.'),
				['rows' => 2]
			);
		?>
	</div>
	<div class="form-group">
		<?php
			echo $form->label('errorAlertOrderSend', t('Error Alert on Order Send') . ' <small>(short text)</small>');
			echo $form->textarea(
				'errorAlertOrderSend',
				$errorAlertOrderSend ? h($errorAlertOrderSend) : t('Comanda nu a fost trimisa.'),
				['rows' => 2]
			);
		?>
	</div>
	<div class="form-group">
		<?php
			echo $form->label('errorTextOrderSend', t('Text page on Order Send Error'));
			echo $form->textarea(
				'errorTextOrderSend',
				$errorTextOrderSend ? h($errorTextOrderSend) : t('Te rugam sa ne trimiti lista pe adresa noastra de email comenzi@cubbis.ro (include nume, prenume, adresa ta de email si un numar de contact) si noi o pregatim pentru tine. Te asteptam in showroom pentru plata si ridicarea comenzii. Iti multumim pentru cumparaturi!'),
				['rows' => 4]
			);
		?>
	</div>
	<div class="form-group">
		<?php
			echo $form->label('successAlertOrderSend', t('Success Alert on Order Send') . ' <small>(short text)</small>');
			echo $form->textarea(
				'successAlertOrderSend',
				$successAlertOrderSend ? h($successAlertOrderSend) : t('Comanda a fost trimisa cu success.'),
				['rows' => 2]
			);
		?>
	</div>
	<div class="form-group">
		<?php
			echo $form->label('successTextOrderSend', t('Text page on Order Send Success'));
			echo $form->textarea(
				'successTextOrderSend',
				$successTextOrderSend ? h($successTextOrderSend) : t('Te asteptam in showroom pentru plata si ridicarea comenzii. Iti multumim pentru cumparaturi!'),
				['rows' => 4]
			);
		?>
	</div>
	<div class="form-group">
		<?php
			echo $form->label('successAlertOrderSave', t('Success Alert on Order Save') . ' <small>(short text)</small>');
			echo $form->textarea(
				'successAlertOrderSave',
				$successAlertOrderSave ? h($successAlertOrderSave) : t('Comanda a fost salvata cu success.'),
				['rows' => 2]
			);
		?>
	</div>
	<div class="form-group rich-text">
		<?php
			$editor = Core::make('editor');
			echo $form->label('textEmptyCart', t('Empty Order Cart Text'));
			echo $editor->outputBlockEditModeEditor('textEmptyCart', $textEmptyCart);
		?>
	</div>
</div>

<style>
	.ccm-ui .fa.red {
		color: #daa520;
	}
	.ccm-ui label.launch-tooltip {
		border-bottom: none;
	}
	.ccm-ui .tooltip-inner {
		background-color: #2076af;
	}
	.ccm-ui .tooltip.top .tooltip-arrow {
		border-top-color: #2076af;
	}
	.ccm-ui textarea.form-control {
		resize: none;
		padding: 2px 8px;
	}
	.ccm-ui .rich-text .cke_contents {
		height: 200px!important
	}
</style>
