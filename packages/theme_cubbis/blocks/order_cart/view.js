$(document).ready(function() {
  var orderList, inputTimeout = null;
  // global js variables added from controller including PHP data
  var errorAlertCartUpdate = errAlertCartUpdate ? errAlertCartUpdate : 'Ceva nu a mers bine. Mai incearca o data.';
  var errorAlertOrderSend = errAlertOrderSend ? errAlertOrderSend : 'Comanda nu a fost trimisa.';
  var errorTextOrderSend = errTextCartUpdate ? errTextCartUpdate : 'Te rugam sa ne trimiti comanda pe adresa noastra de email.';
  var successAlertOrderSend = scsAlertOrderSend ? scsAlertOrderSend : 'Comanda a fost trimisa cu success.';
  var successTextOrderSend = scsTextOrderSend ? scsTextOrderSend : 'Iti multumim pentru cumparaturi!';
  var successAlertOrderSave = scsAlertOrderSave ? scsAlertOrderSave : 'Comanda a fost salvata cu success.';

  // update order cart list item
  $(document).on('keydown paste drop focusout input', '.order-list .quantity input', function(e) {
    if (e.type == 'keydown') {
      var code = e.which || e.keyCode;
      if (([39,37,46,8].indexOf(code) != -1) || (([65,86,67].indexOf(code) != -1) && (e.ctrlKey === true || e.metaKey === true))) {
        return;
      }
      if ((e.shiftKey || (code < 48 || code > 57)) && (code < 96 || code > 105)) {
        e.preventDefault();
      }
    }
    if (e.type == 'paste') {
      var paste = (event.clipboardData || window.clipboardData).getData('text');
      if (/^[\d]+$/.test(paste) == false) {
        e.preventDefault();
      }
    }
    if (e.type == 'drop') {
      var drop = (event.dataTransfer || window.dataTransfer).getData('text');
      if (/^[\d]+$/.test(drop) == false) {
        e.preventDefault();
      }
    }
    if ((this.value != '' && e.type != 'focusout') || (this.value == '' && e.type == 'focusout')) {
      var _this = this;
      var newValue = this.value;
      var oldValue = $(this).attr('value');
      clearTimeout(inputTimeout);  
      inputTimeout = setTimeout(function() {
        if (newValue != oldValue) {
          var data = $.param({ 
            'p_id': $(_this).attr('data-id'),
            'p_quantity': newValue,
          });
          cartUpdateAction(data);
        }
      }, 600);
    }
  });

  // delete order cart list item
  $(document).on('click', '.order-list td.delete button', function(e) {
    e.preventDefault();
    var data = $.param({ 
      'p_id': $(this).parents('tr').find('input').attr('data-id'),
      'p_quantity': 0,
    });
    cartUpdateAction(data);
  });

  // delete order cart list
  $(document).on('click', '.order-list th.delete button', function(e) {
    e.preventDefault();
    var data = $.param({ 'p_cart_delete': true });
    cartUpdateAction(data);
  });

  // send order cart list
  $(document).on('click', '.litebox .order-cart-form .order-send', function(e) {
    e.preventDefault();

    var formValid = true;
    var nameElem = $('.validate-name input');
    var emailElem = $('.validate-email input');
    var phoneElem = $('.validate-phone input');
    
    var emailRegex = /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    var phoneRegex = /^[+]?[\s./0-9]*[(]?[0-9]{1,4}[)]?[-\s./0-9]*$/;

    nameElem.val($.trim(nameElem.val()));
    emailElem.val($.trim(emailElem.val()));
    phoneElem.val($.trim(phoneElem.val()));

    $('.order-cart-form .input-group').removeClass('has-error');

    if (nameElem.val().length < 3) {
      formValid = false;
      $('.validate-name').addClass('has-error');
    }
    if (emailElem.val().length < 5 || !emailRegex.test(emailElem.val())) {
      formValid = false;
      $('.validate-email').addClass('has-error');
    }
    if (phoneElem.val().length < 4 || !phoneRegex.test(phoneElem.val())) {
      formValid = false;
      $('.validate-phone').addClass('has-error');
    }

    if (formValid) {
      $('.litebox-close').trigger('click');
      orderSendAction();
    }
  });

  function cartUpdateAction(data) {
    $.ajax({
      url: $('.order-cart-block').attr('data-action-update'),
      type: 'POST',
      data: data,
      dataType: 'json',
      beforeSend: function() {
        $('.order-cart-block').addClass('preloader spinner');
      },
      success: function(response) {
        if (response.success) {
          if (response.sessionProducts.length > 0) {
            $('.header-cart .total').text(response.cartTotal > 0 ? response.cartTotal : '');
            $('.order-cart-block .order-list').html(response.sessionProducts);
          } else {
            $('.header-cart .total').text('');
            $('.order-cart-block .order-list').remove();
            $('.order-cart-block .order-list-actions').remove();
            $('.order-cart-block').append('<p>Cosul tau de cumparaturi este gol.</p>');
          }
        } else {
          toastr.error(errorAlertCartUpdate);
        }
      },
      error: function() {
        toastr.error(errorAlertCartUpdate);
      },
      complete: function() {
        $('.order-cart-block').removeClass('preloader spinner');
      }
    });
  }

  function orderSendAction() {
    orderList = $('.order-cart-block .order-list');
    $.ajax({
      url: $('#formOrderSend').attr('action'),
      type: 'POST',
      data: $('#formOrderSend').serialize(),
      dataType: 'json',
      beforeSend: function() {
        orderList.addClass('preloader spinner');
      },
      success: function(response) {
        if (response.success) {
          if (response.orderSaved == true) {
            toastr.success(successAlertOrderSave);
          }
          if (response.orderSent == true) {
            toastr.success(successAlertOrderSend);
            $('.order-cart-block').html('<p class="send-text">'+ successTextOrderSend +'</p>');
          } else {
            orderSendFailAction();
          }
        } else {
          orderSendFailAction();
        }
      },
      error: function() {
        orderSendFailAction();
      },
      complete: function() {
        $('.header-cart .total').text('');
        orderList.removeClass('preloader spinner');
      }
    });
  }

  function orderSendFailAction() {
    toastr.warning(errorAlertOrderSend);
    $('.order-cart-block').html('<p class="send-text">'+ errorTextOrderSend +'</p>');
    // $('#orderCartDownload').trigger('click'); doesn't work
    document.getElementById('orderCartDownload').click();
  }
});
