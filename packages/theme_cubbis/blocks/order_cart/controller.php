<?php
namespace Concrete\Package\ThemeCubbis\Block\OrderCart;
use Concrete\Core\Block\BlockController;
use Concrete\Core\Database\Connection\Connection;
use Concrete\Core\Editor\LinkAbstractor;
use Concrete\Core\User\User;
use Page;
use Core;

class Controller extends BlockController
{
	protected $btTable = 'btCubbisOrderCart';
	protected $btInterfaceWidth = 550;
	protected $btInterfaceHeight = 600;
	protected $btWrapperClass = 'ccm-ui';
	protected $btCacheBlockRecord = false;
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btIgnorePageThemeGridFrameworkContainer = true;
	protected $btExportTables = ['btCubbisOrderCart', 'btCubbisOrderList', 'btCubbisOrderListEntries'];
	protected $btDefaultSet = 'blocksetcubbis';

	protected $userID = null;
	protected $userName = null;
	protected $userEmail = null;
	protected $userPhone = null;
	protected $userLoggedIn = false;
	protected $isPartnerType = false;
	protected $partnerPrice = 'price';
	protected $appExternalFilePath = null;

	public function getBlockTypeName()
	{
		return t('Order Cart');
	}

	public function getBlockTypeDescription()
	{
		return t('Order Cart widget');
	}

	/*
	different product prices are assigned to different user types, which are of 6 types: 'price', 'price1', 'price2'.. 'price5' 
	5 levels of partner users can see from 'price1' to 'price5', the rest will see standard price which is 'price'
	if for a product, a partner has its particular price equal with 0, then get the standard price (which will never be equal with 0)
	*/
	public function on_start()
	{
		$u = new User();
		$ui = $u->getUserInfoObject();
		$this->userID = $u->getUserID();
		$this->userLoggedIn = $u->isLoggedIn() && is_object($ui);
		$this->userEmail = $this->userLoggedIn ? $ui->getUserEmail() : null;
		$this->userPhone = $this->userLoggedIn ? $ui->getAttribute('user_phone') : null;
		$this->userName = $this->userLoggedIn ? ($ui->getAttribute('user_first_name') . ' ' . $ui->getAttribute('user_last_name')) : null;
		$this->isPartnerType = $this->userLoggedIn && $ui->getAttribute('user_type') == 'partner';

		if ($this->isPartnerType && in_array($ui->getAttribute('user_price'), array('price1','price2','price3','price4','price5'))) {
			$this->partnerPrice = $ui->getAttribute('user_price');
		}

		$site = $this->app->make('site')->getSite();
		$this->appExternalFilePath = $site->getAttribute('app_external_file_path');

		date_default_timezone_set('Europe/Bucharest');
	}

	public function registerViewAssets($outputContent = '')
	{
		$this->requireAsset('javascript', 'jquery');
		$this->requireAsset('javascript', 'lazy-sizes');
		$this->requireAsset('css', 'order-list');
		$this->requireAsset('litebox');
		$this->requireAsset('toastr');

		$alertVars = "var errAlertCartUpdate = '{$this->errorAlertCartUpdate}';";
		$alertVars .= "var errAlertOrderSend = '{$this->errorAlertOrderSend}';";
		$alertVars .= "var errTextCartUpdate = '{$this->errorTextOrderSend}';";
		$alertVars .= "var scsAlertOrderSend = '{$this->successAlertOrderSend}';";
		$alertVars .= "var scsTextOrderSend = '{$this->successTextOrderSend}';";
		$alertVars .= "var scsAlertOrderSave = '{$this->successAlertOrderSave}';";
		$this->addFooterItem('<script type="text/javascript">'. $alertVars .'</script>');
	}

	public function add()
	{
		$this->requireAsset('core/file-manager');
		$this->requireAsset('core/sitemap');
	}

	public function edit()
	{
		$this->requireAsset('core/file-manager');
		$this->requireAsset('core/sitemap');
		$this->set('textEmptyCart', LinkAbstractor::translateFrom($this->textEmptyCart));
	}

	public function view()
	{
		$sessionCart = $this->getSessionCart();
		$this->set('sessionProducts', $this->setProductList($sessionCart['productList'], $sessionCart['priceTotal']));

		$this->set('userName', $this->userName);
		$this->set('userPhone', $this->userPhone);
		$this->set('userEmail', $this->userEmail);
		$this->set('textEmptyCart', LinkAbstractor::translateFrom($this->textEmptyCart));
	}

	public function delete()
	{
		parent::delete();
	}

	public function save($args)
	{
		$this->set('textEmptyCart', LinkAbstractor::translateFrom($this->textEmptyCart));
		parent::save($args);
	}

	public function getSessionCart()
	{
		$session = $this->app->make('session');
		$cartList = is_array($session->get('cartList')) ? $session->get('cartList') : array();
		$cartItemsIDs = array_keys($cartList);
		$productList = array();
		$priceTotal = 0;

		if (count($cartItemsIDs) > 0) {
			$db = $this->app->make('database')->connection('cubbis_module');
			$qb = $db->createQueryBuilder();

			// SELECT products fields
			$query = $qb->select('p.id,p.code,p.name,p.filename')->from('nomen_products', 'p');
			$query->addSelect('ifnull(np.name,"") as provider');
			// ugly but optimizes the query to get only the price assigned to this user
			if ($this->isPartnerType) {
				$query->addSelect(array('IF(p.'.$this->partnerPrice.'>0, p.'.$this->partnerPrice.', p.price) as uprice'));
			} else {
				$query->addSelect('p.price as uprice');
			}
			$query->join('p', 'nomen_providers', 'np', 'p.nomen_provider_id = np.id');
			$query->andWhere('p.id IN (:idList)')->setParameter(':idList', $cartItemsIDs, Connection::PARAM_STR_ARRAY); 
			$productList = $query->execute()->fetchAll();

			// UPDATE quantity for each product
			foreach ($productList as $key => $product) {
				$productQuantity = (int)$cartList[$product['id']];
				if ($productQuantity > 0) {
					$productList[$key]['quantity'] = $productQuantity;
					$priceTotal += $productQuantity * $product['uprice'];
				} else {
					unset($productList[$key]);
				}
			}
		}
		
		return array(
			'productList' => $productList,
			'priceTotal' => $priceTotal
		);
	}

	public function setProductList($products=array(), $priceTotal=null) 
	{
		$html = '';
		$imgFilePath = $this->appExternalFilePath;
		if (count($products) > 0) {
			$html .= '<table>';
			$html .= 	'<thead>';
			$html .= 		'<tr>';
			$html .=			'<th class="title">'.t('Produs').'</th>';
			$html .=			'<th class="price">'.t('Pret unitar').'</th>';
			$html .=			'<th class="quantity">'.t('Cantitate').'</th>';
			$html .=			'<th class="subtotal">'.t('Subtotal').'</th>';
			$html .=			'<th class="delete"><button class="btn-style-icon trash" type="button"></button></th>';
			$html .=		'</tr>';
			$html .=	'</thead>';
			$html .=	'<tbody>';
			foreach ($products as $product) {
				$price = number_format($product['uprice'],2,'.','');
				$subtotal = number_format(($product['quantity'] * $product['uprice']),2,'.','');
				$imgSrc = $imgFilePath.'/'.$product['filename'];

				$html .= 	'<tr>';
				$html .= 		'<td class="title">';
				if (!empty($product['filename'])) {
					$html .= 			'<img class="image lazyload" data-src="'.$imgSrc.'" />';
				} else {
					$html .= 			'<span class="image"></span>';
				}
				$html .= 			'<p>'.$product['name'].'<span>'.$product['provider'].'</span><span>'.$product['code'].'</span></p>';
				$html .= 		'</td>';
				$html .= 		'<td class="price">'.$price.' lei</td>';
				$html .= 		'<td class="quantity">';
				$html .= 			'<input class="input-style" type="number" value="'.$product['quantity'].'" data-id="'.$product['id'].'"/>';
				$html .= 		'</td>';
				$html .= 		'<td class="subtotal">'.$subtotal.' lei</td>';
				$html .= 		'<td class="delete"><button class="btn-style-icon trash" type="button"></button></td>';
				$html .= 	'</tr>';
			}
			$html .= 		'<tr>';
			$html .= 			'<td colspan="2"></td>';
			$html .= 			'<td>'.t('Total').'</td>';
			$html .= 			'<td>'.number_format($priceTotal,2,'.','').' lei</td>';
			$html .= 			'<td></td>';
			$html .= 		'</tr>';
			$html .= 	'</tbody>';
			$html .= '</table>';
		}
		return $html;
	}

	public function action_session_cart_update() 
	{
		$session = $this->app->make('session');
		$cartList = is_array($session->get('cartList')) ? $session->get('cartList') : array();

		if ($_POST['p_cart_delete'] == true) {
			$session->remove('cartList');
			echo json_encode(array(
				'success' => true,
				'sessionProducts' => array(),
				'cartTotal' => 0
			));
		} else {
			if (!empty($_POST['p_id'])) {
				if ($_POST['p_quantity'] > 0) {
					$cartList[$_POST['p_id']] = (int)$_POST['p_quantity'];
				} else {
					unset($cartList[$_POST['p_id']]);
				}
				$session->set('cartList', $cartList);

				$sessionCart = $this->getSessionCart();
				$sessionProducts = $this->setProductList($sessionCart['productList'], $sessionCart['priceTotal']);
				
				if (count($sessionCart['productList']) == 0) {
					$session->remove('cartList');
				}
				echo json_encode(array(
					'success' => true,
					'sessionProducts' => $sessionProducts,
					'cartTotal' => count($sessionCart['productList'])
				));
			} else {
				header('HTTP/1.0 404 Not Found');
				echo 'product ID not found';
			}
		}
		exit();
	}

	public function action_session_cart_send() 
	{
		$sessionCart = $this->getSessionCart();
		if (count($sessionCart['productList']) > 0) {
			$dh = $this->app->make('helper/date');
			$dateNow = $dh->toDB();

			// SAVE ORDER LIST
			if ($this->userLoggedIn) {
				$db = $this->app->make('database')->connection();

				$orderListLastId = $db->fetchColumn('SELECT MAX(orderID) FROM btCubbisOrderList');
				$orderListLastId = !empty($orderListLastId) ? ($orderListLastId + 1) : 1;

				$db->executeQuery('DELETE FROM btCubbisOrderListEntries WHERE orderID = ?', [$orderListLastId]);
				foreach ($sessionCart['productList'] as $prod) {
					$db->executeQuery('INSERT INTO btCubbisOrderListEntries (orderID, pID, code, name, provider, filename, price, quantity) values(?,?,?,?,?,?,?,?)',
						[$orderListLastId, $prod['id'], $prod['code'], $prod['name'], $prod['provider'], $prod['filename'], $prod['uprice'], $prod['quantity']]
					);
				}

				$db->executeQuery('INSERT INTO btCubbisOrderList (uID, orderPriceTotal, orderDate, orderID) values(?,?,?,?)', 
					[$this->userID, $sessionCart['priceTotal'], $dateNow, $orderListLastId]
				);
			}

			// EMAIL ORDER LIST
			$attachment = "Produs ID, Cod, Nume, Furnizor, Pret unitar, Cantitate\n";
			foreach ($sessionCart['productList'] as $prod) {
				$attachment .= '"'.$prod['id'].'","'.$prod['code'].'","'.$prod['name'].'","'.$prod['provider'].'","'.$prod['uprice'].'","'.$prod['quantity'].'"';
				$attachment .= "\n";
			}

			$mailService = Core::make('mail');
			$mailService->setTesting(true); 
			$mailService->addParameter('userName', $_POST['u_name']);
			$mailService->addParameter('userEmail', $_POST['u_email']);
			$mailService->addParameter('userPhone', $_POST['u_phone']);
			$mailService->addParameter('userExtraInfo', $_POST['u_extra']);
			$mailService->addParameter('orderDate', $dh->date('d.m.Y H:i:s', $dateNow));
			$mailService->addParameter('orderPrice', $sessionCart['priceTotal'].' lei');
			$mailService->addParameter('orderList', $sessionCart['productList']);
			$mailService->load('order_list', 'theme_cubbis');
			$mailService->addRawAttachment($attachment, 'cubbis_comanda.csv');
			$mailService->from($this->orderEmailFrom, $_POST['u_name']);
			$mailService->to($this->orderEmailTo);
			$mailService->sendMail();

			$mailServiceFail = $mailService->getTesting();

			echo json_encode(array(
				'success' => true,
				'orderSaved' => $this->userLoggedIn,
				'orderSent' => !$mailServiceFail,
			));
		} else {
			header('HTTP/1.0 404 Not Found');
			echo 'list is empty';
		}

		$session = $this->app->make('session');
		$session->remove('cartList');
		exit();
	}

	public function action_session_cart_download()
	{
		$sessionCart = $this->getSessionCart();
		if (count($sessionCart['productList']) > 0) {
			header('Pragma: public');
			header('Content-Type: text/csv');
			header('Cache-control: private');
			header('Content-Disposition: attachment; filename=comanda_cubbis.csv');

			echo "Produs ID, Cod, Nume, Furnizor, Pret unitar, Cantitate\n";

			$row = array();
			$fp = fopen('php://output', 'w');
			foreach($sessionCart['productList'] as $product) {
				$row = array($product['id'],$product['code'],$product['name'],$product['provider'],$product['uprice'],$product['quantity']);
				fputcsv($fp, $row);
			}
			fclose($fp);
		} else {
			header('HTTP/1.0 404 Not Found');
			echo 'list ID not found';
		}
		$session = $this->app->make('session');
		$session->remove('cartList');
		exit();
	}
}
