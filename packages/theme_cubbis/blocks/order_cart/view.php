<?php defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();
$u = new User();

$site = Site::getSite();
$productsPage = $site->getAttribute('product_list_page');
$productsPage = !empty($productsPage) ? $productsPage : (BASE_URL . '/produse');

if ($c->isEditMode()) {
  ?>
  <div class="ccm-edit-mode-disabled-item" style="width: 100%; height: 100px">
    <div style="padding: 30px 0px 0px 0px"><?= t('Order Cart disabled in edit mode.') ?></div>
  </div>
  <?php
} else { ?>
  <div class="container">
    <div class="order-cart-block order-list-container" data-action-update="<?= $this->action('session_cart_update') ?>">
      <h2><?= t('Cosul meu'); ?></h2>
      <?php if (!empty($sessionProducts)) { ?>
        <div class="order-list">
          <?php echo $sessionProducts; ?>
        </div>
        <div class="order-list-actions clearfix">
          <a href="<?= $productsPage ?>" class="btn-style small back-to-shopping">
            <i class="fa fa-shopping-cart"></i><?= $orderBackLinkTitle ?>
          </a>
          <a href="#orderCartAnchor" class="btn-style small order-form" type="button" data-litebox="inline">
            <?= $u->isLoggedIn() ? $orderSaveLinkTitle : $orderSendLinkTitle ?><i class="fa fa-envelope-o"></i>
          </a>
        </div>
      <?php } else { ?>
        <?php echo $textEmptyCart; ?>
      <?php } ?>
    </div>

    <?php if (!empty($sessionProducts)) { ?>
      <div class="order-cart-form litebox-hide" id="orderCartAnchor">
        <h4><?= t('Formular de comanda') ?></h4>
        <form id="formOrderSend" action="<?= $this->action('session_cart_send')?>">
          <div class="input-group validate-name">
            <label for="userName"><?= t('Nume si prenume') ?> <span>*</span></label>
            <input id="userName" type="text" class="input-style" 
                   name="u_name" placeholder="Nume si prenume" 
                   value="<?= !empty($userName) ? $userName : '' ?>" />
          </div>
          <div class="input-group validate-email">
            <label for="userEmail"><?= t('Email') ?> <span>*</span></label>
            <input id="userEmail" type="text" class="input-style" 
                   name="u_email" placeholder="Email"
                   value="<?= !empty($userEmail) ? $userEmail : '' ?>" />
          </div>
          <div class="input-group validate-phone">
            <label for="userPhone"><?= t('Telefon') ?> <span>*</span></label>
            <input id="userPhone" type="text" class="input-style" 
                   name="u_phone" placeholder="Telefon"
                   value="<?= !empty($userPhone) ? $userPhone : '' ?>" />
          </div>
          <div class="input-group">
            <label for="userExtra"><?= t('Informatii aditionale') ?></label>
            <textarea id="userExtra" class="textarea-style" name="u_extra" placeholder="Adauga informatii aditionale"></textarea>
          </div>
          <div class="text-right">
            <button type="button" class="btn-style order-send"><?= t('Trimite') ?></button>
          </div>
        </form>
      </div>
      <a id="orderCartDownload" href="<?= $this->action('session_cart_download') ?>" download></a>
    <?php } ?>
  </div>
  <?php
} ?>
