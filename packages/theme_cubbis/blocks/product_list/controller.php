<?php
namespace Concrete\Package\ThemeCubbis\Block\ProductList;
use Concrete\Core\Block\BlockController;
use Concrete\Core\Support\Facade\Application;
use Concrete\Core\Database\Connection\Connection;
use Concrete\Core\User\User;
use Core;
use Page;
use File;

class Controller extends BlockController
{
	protected $btTable = 'btCubbisProductList';
	protected $btInterfaceWidth = 500;
	protected $btInterfaceHeight = 650;
	protected $btWrapperClass = 'ccm-ui';
	protected $btCacheBlockRecord = false;
	protected $btCacheBlockOutput = false;
	protected $btCacheBlockOutputOnPost = false;
	protected $btIgnorePageThemeGridFrameworkContainer = true;
	protected $btExportTables = ['btCubbisProductList',	'btCubbisProductStyleColors', 'btCubbisProductStyleProviders'];
	protected $btDefaultSet = 'blocksetcubbis';

	protected $isPartnerType = false;
	protected $partnerPrice = 'price';
	protected $appExternalFilePath = null;
	protected $categoriesOrderArray = array();
	protected $categoriesIgnoredArray = array();
	
	public function getBlockTypeName()
	{
		return t('Product List');
	}

	public function getBlockTypeDescription()
	{
		return t('Product List widget with filters, ordering and display options');
	}

	/*
	different product prices are assigned to different user types, which are of 6 types: 'price', 'price1', 'price2'.. 'price5' 
	5 levels of partner users can see from 'price1' to 'price5', the rest will see standard price which is 'price'
	if for a product, a partner has its particular price equal with 0, then get the standard price (which will never be equal with 0)
	*/
	public function on_start()
	{
		$u = new User();
		$ui = $u->getUserInfoObject();
		$this->isPartnerType = $u->isLoggedIn() && is_object($ui) && $ui->getAttribute('user_type') == 'partner';
		$this->appExternalFilePath = $this->app->make('site')->getSite()->getAttribute('app_external_file_path');
		$this->categoriesOrderArray = explode(',', $this->filterCategoriesOrder);
		$this->categoriesIgnoredArray = explode(',', $this->filterCategoriesIgnored);

		if ($this->isPartnerType && in_array($ui->getAttribute('user_price'), array('price1','price2','price3','price4','price5'))) {
			$this->partnerPrice = $ui->getAttribute('user_price');
		}
	}

	public function registerViewAssets($outputContent = '')
	{
		$this->requireAsset('javascript', 'jquery');
		$this->requireAsset('javascript', 'lazy-sizes');
		$this->requireAsset('range-slider');
		$this->requireAsset('litebox');
		$this->requireAsset('toastr');

		$alertVars = "var errAlertFilter = '{$this->errorAlertFilter}';";
		// remove Cart functionality
		// $alertVars .= "var errAlertCartAdd = '{$this->errorAlertCart}';";
		// $alertVars .= "var scsAlertCartAdd = '{$this->successAlertCart}';";
		$this->addFooterItem('<script type="text/javascript">'. $alertVars .'</script>');

		$filterStyleColors = $this->getFilterStyleColors();
		$filterStyleProviders = $this->getFilterStyleProviders();
		if (!empty($filterStyleColors)) {
			$colorVars = "";
			foreach ($filterStyleColors as $styleColor) {
				$titleCssClass = preg_replace('/\W+/', '', strtolower($styleColor['title']));
				$colorVars .= ".product-filters .color.color_{$titleCssClass} {background: {$styleColor['code']}}";
				$colorVars .= ".product-filters .color.color_{$titleCssClass}:before {color: {$styleColor['code']};filter: invert(100%)}";
			}
			$this->addHeaderItem('<style>'. $colorVars .'</style>');
		}
		if (!empty($filterStyleProviders)) {
			$providerVars = "";
			foreach ($filterStyleProviders as $styleProvider) {
				$f = File::getByID($styleProvider['fID']);
				if (is_object($f) && $f->getFileID()) {
					$imgWidth = $f->getAttribute('width');
					$imgHeight = $f->getAttribute('height');
					$cropWidth = 120;
					$cropHeight = ($cropWidth * $imgHeight) / $imgWidth;

					$img = $this->app->make('helper/image');
					$thumb = $img->getThumbnail($f, $cropWidth, $cropHeight, true);
					$titleCssClass = preg_replace('/\W+/', '', strtolower($styleProvider['title']));
					$providerVars .= ".product-filters .provider.provider_{$titleCssClass} {background-image: url({$thumb->src});}";
				}
			}
			$this->addHeaderItem('<style>'. $providerVars .'</style>');
		}
	}

	public function add()
	{
		$this->requireAsset('core/file-manager');
		$this->requireAsset('core/sitemap');
	}

	public function edit()
	{
		$this->requireAsset('core/file-manager');
		$this->requireAsset('core/sitemap');

		$this->set('filterStyleColors', $this->getFilterStyleColors());
		$this->set('filterStyleProviders', $this->getFilterStyleProviders());
	}

	public function view()
	{
		$response = $this->getProductViewData();

		if (!$response['productListEmpty']) {
			$this->set('productListEmpty', $response['productListEmpty']);
			$this->set('productList', $response['productList']);
			$this->set('productListPagination', $response['productListPagination']);

			$this->set('filterCategories', $response['filterCategories']);
			$this->set('filterColors', $response['filterColors']);
			$this->set('filterProviders', $response['filterProviders']);

			$priceLimits = $this->getPriceLimitsMinMax();
			$this->set('priceLimitMin', $priceLimits['min']);
			$this->set('priceLimitMax', $priceLimits['max']);
		} else {
			$this->set('productListEmpty', $response['productListEmpty']);
			$this->set('productList', $response['productList']);
		}
	}

	public function delete()
	{
		$db = $this->app->make('database')->connection();
		$db->executeQuery('DELETE FROM btCubbisProductList');
		$db->executeQuery('DELETE FROM btCubbisProductStyleColors');
		$db->executeQuery('DELETE FROM btCubbisProductStyleProviders');
		parent::delete();
	}

	public function duplicate($newBID)
	{
		parent::duplicate($newBID);
		$db = $this->app->make('database')->connection();
		$queryColor = $db->executeQuery('SELECT * FROM btCubbisProductStyleColors WHERE bID = ?', [$this->bID]);
		foreach ($queryColor as $row) {
			$db->executeQuery('INSERT INTO btCubbisProductStyleColors (bID, title, code, sortOrder) values(?,?,?,?)',
				[$newBID,	$row['title'], $row['code'], $row['sortOrder']]
			);
		}
		$queryProvider = $db->executeQuery('SELECT * FROM btCubbisProductStyleProviders WHERE bID = ?', [$this->bID]);
		foreach ($queryProvider as $row) {
			$db->executeQuery('INSERT INTO btCubbisProductStyleProviders (bID, fID, title, sortOrder) values(?,?,?,?)',
				[$newBID,	$row['fID'], $row['title'],	$row['sortOrder']]
			);
		}
	}

	public function save($args)
	{
		$db = $this->app->make('database')->connection();
		$db->executeQuery('DELETE FROM btCubbisProductStyleColors WHERE bID = ?', [$this->bID]);
		$db->executeQuery('DELETE FROM btCubbisProductStyleProviders WHERE bID = ?', [$this->bID]);

		parent::save($args);

		$countColors = isset($args['colorSortOrder']) ? count($args['colorSortOrder']) : 0;
		$countProviders = isset($args['providerSortOrder']) ? count($args['providerSortOrder']) : 0;
		$i = 0; $j = 0;

		while ($i < $countColors) {
			$db->executeQuery('INSERT INTO btCubbisProductStyleColors (bID, title, code, sortOrder) values(?,?,?,?)',
				[$this->bID, $args['colorTitle'][$i], $args['colorCode'][$i], $args['colorSortOrder'][$i]]
			);
			++$i;
		}

		while ($j < $countProviders) {
			$db->executeQuery('INSERT INTO btCubbisProductStyleProviders (bID, fID, title, sortOrder) values(?,?,?,?)',
				[$this->bID, (int) $args['fID'][$j], $args['providerTitle'][$j], $args['providerSortOrder'][$j]]
			);
			++$j;
		}
	}

	public function validate($args)
	{
		$error = Core::make('helper/validation/error');
		$fcList = explode(',', $args['filterCategoriesOrder']);
		if (count($fcList) == 0 || in_array("", $fcList)) {
			$error->add(t('Add all product categories in the desired order divided by commas, like so: category1,category2,category3'));
		}
		return $error;
	}

	public function getPriceLimitsMinMax()
	{
		$db = $this->app->make('database')->connection('cubbis_module');
		$query = $db->fetchAll('SELECT MIN(price) as min, MAX(price) as max FROM nomen_products');
		
		return array(
			'min' => $query[0]['min'] >= 0 ? floor($query[0]['min']) : null,
			'max' => $query[0]['max'] >= 0 ? ceil($query[0]['max']) : null
		);
	}

	public function getProductViewData() 
	{
		$totalProducts = $this->getTotalProducts();
		
		if ($totalProducts > 0) {
			$perPage = !empty($_POST['p_per_page']) ? $_POST['p_per_page'] : 30;
			$page = !empty($_POST['p_page']) ? $_POST['p_page'] : 1;
			$pages = ceil($totalProducts / $perPage);
			if ($page > $pages) {
				$page = $_POST['p_page'] = 1;
			}

			$productsData = $this->getCurrentPageProducts();
			$productListView = $this->setProductList($productsData);
			$productListPaginationView = $this->setProductListPagination($page, $pages);

			$filterCategories = $this->getFiltersByType('category');
			$filterColors = $this->getFiltersByType('color');
			$filterProviders = $this->getFiltersByType('provider');

			$filterCategoriesView = $this->setFilterByType($filterCategories, 'p_category', $this->filterCategoryLabel);
			$filterColorsView = $this->setFilterByType($filterColors, 'p_color', $this->filterColorLabel);
			$filterProvidersView = $this->setFilterByType($filterProviders, 'p_provider', $this->filterProviderLabel);
			
			return array(
				'productListEmpty' => false,
				'productList' => $productListView,
				'productListPagination' => $productListPaginationView,
				'filterCategories' => $filterCategoriesView,
				'filterColors' => $filterColorsView,
				'filterProviders' => $filterProvidersView
			);
		} else {
			$productListView = $this->setProductListEmpty();

			return array(
				'productListEmpty' => true,
				'productList' => $productListView
			);
		}
	}

	public function getTotalProducts()
	{
		$db = $this->app->make('database')->connection('cubbis_module');
		$qb = $db->createQueryBuilder();

		// SELECT products fields
		$queryProducts = $qb->select('COUNT(p.id) as totalProducts')->from('nomen_products', 'p');
		$queryProducts->addSelect('IFNULL(np.name,"") as provider');
		$queryProducts->addSelect('IFNULL(nc.name,"") as category');

		// ugly but optimizes the query to get only the price assigned to this user
		if ($this->isPartnerType) {
			$queryProducts->addSelect(array('IF(p.'.$this->partnerPrice.'>0, p.'.$this->partnerPrice.', p.price) as uprice'));
			if (!empty($_POST['p_price_min']) && !empty($_POST['p_price_max'])) {
				$queryProducts->andWhere(
					$queryProducts->expr()->orX(
						$queryProducts->expr()->andX(
							'p.'.$this->partnerPrice.' > 0',
							'p.'.$this->partnerPrice.' >= :price_min',
							'p.'.$this->partnerPrice.' <= :price_max'
						),
						$queryProducts->expr()->andX('p.price >= :price_min', 'p.price <= :price_max')
					)
				);
				$queryProducts->setParameter(':price_min', $_POST['p_price_min']);
				$queryProducts->setParameter(':price_max', $_POST['p_price_max']);
			}
		} else {
			$queryProducts->addSelect('p.price as uprice');
			if (!empty($_POST['p_price_min']) && !empty($_POST['p_price_max'])) {
				$queryProducts->andWhere('p.price >= :price_min')->setParameter(':price_min', $_POST['p_price_min']);
				$queryProducts->andWhere('p.price <= :price_max')->setParameter(':price_max', $_POST['p_price_max']);
			}
		}

		// JOINS
		$queryProducts->join('p', 'nomen_providers', 'np', 'p.nomen_provider_id = np.id');
		$queryProducts->join('p', 'nomen_product_categories', 'nc', 'p.nomen_product_categories_id = nc.id');

		// APPLY filters
		if (!empty($_POST['p_search'])) {
			$queryProducts->andWhere(
				$queryProducts->expr()->orX(
					$queryProducts->expr()->like('p.name', ':search'),
					$queryProducts->expr()->like('p.code', ':search'),
					$queryProducts->expr()->like('nc.name', ':search'),
					$queryProducts->expr()->like('np.name', ':search'),
					$queryProducts->expr()->like('p.description', ':search'))
			)->setParameter(':search', '%'.$_POST['p_search'].'%');
		}
		if (!empty($_POST['p_color'])) {
			$queryProducts->andWhere('p.color IN (:colorList)')
										->setParameter(':colorList', $_POST['p_color'], Connection::PARAM_STR_ARRAY); 
		}
		$pProvider = !empty($_POST['p_provider']) ? $_POST['p_provider'] : $_GET['p_provider'];
		if (!empty($pProvider)) {
			$queryProducts->andWhere('np.name IN (:providerList)')
										->setParameter(':providerList', $pProvider, Connection::PARAM_STR_ARRAY); 
		}
		$pCategory = !empty($_POST['p_category']) ? $_POST['p_category'] : $_GET['p_category'];
		if (!empty($pCategory)) {
			$queryProducts->andWhere('nc.name IN (:categoryList)')
										->setParameter(':categoryList', $pCategory, Connection::PARAM_STR_ARRAY); 
		}
		// ignore products with specified categories
		$queryProducts->andWhere('nc.name NOT IN (:categoryIgnoredList)')
									->setParameter(':categoryIgnoredList', $this->categoriesIgnoredArray, Connection::PARAM_STR_ARRAY);

		$response = $queryProducts->execute()->fetchColumn();
		return $response;
	}

	public function getCurrentPageProducts()
	{
		$page = !empty($_POST['p_page']) ? $_POST['p_page'] : 1;
		$perPage = !empty($_POST['p_per_page']) ? $_POST['p_per_page'] : 30;

		$db = $this->app->make('database')->connection('cubbis_module');
		$qb = $db->createQueryBuilder();

		// SELECT products fields
		$queryProducts = $qb->select('p.id,p.name,p.code,p.color,p.filename,p.description')->from('nomen_products', 'p');
		$queryProducts->addSelect('IFNULL(np.name,"") as provider');
		$queryProducts->addSelect('IFNULL(nc.name,"") as category');

		// ugly but optimizes the query to get only the price assigned to this user
		if ($this->isPartnerType) {
			$queryProducts->addSelect(array('IF(p.'.$this->partnerPrice.'>0, p.'.$this->partnerPrice.', p.price) as uprice'));
			if (!empty($_POST['p_price_min']) && !empty($_POST['p_price_max'])) {
				$queryProducts->andWhere(
					$queryProducts->expr()->orX(
						$queryProducts->expr()->andX(
							'p.'.$this->partnerPrice.' > 0',
							'p.'.$this->partnerPrice.' >= :price_min',
							'p.'.$this->partnerPrice.' <= :price_max'
						),
						$queryProducts->expr()->andX('p.price >= :price_min', 'p.price <= :price_max')
					)
				);
				$queryProducts->setParameter(':price_min', $_POST['p_price_min']);
				$queryProducts->setParameter(':price_max', $_POST['p_price_max']);
			}
		} else {
			$queryProducts->addSelect('p.price as uprice');
			if (!empty($_POST['p_price_min']) && !empty($_POST['p_price_max'])) {
				$queryProducts->andWhere('p.price >= :price_min')->setParameter(':price_min', $_POST['p_price_min']);
				$queryProducts->andWhere('p.price <= :price_max')->setParameter(':price_max', $_POST['p_price_max']);
			}
		}

		// JOINS
		$queryProducts->join('p', 'nomen_providers', 'np', 'p.nomen_provider_id = np.id');
		$queryProducts->join('p', 'nomen_product_categories', 'nc', 'p.nomen_product_categories_id = nc.id');

		// APPLY filters
		if (!empty($_POST['p_search'])) {
			$queryProducts->andWhere(
				$queryProducts->expr()->orX(
					$queryProducts->expr()->like('p.name', ':search'),
					$queryProducts->expr()->like('p.code', ':search'),
					$queryProducts->expr()->like('nc.name', ':search'),
					$queryProducts->expr()->like('np.name', ':search'),
					$queryProducts->expr()->like('p.description', ':search'))
			)->setParameter(':search', '%'.$_POST['p_search'].'%');
		}
		if (!empty($_POST['p_color'])) {
			$queryProducts->andWhere('p.color IN (:colorList)')
										->setParameter(':colorList', $_POST['p_color'], Connection::PARAM_STR_ARRAY); 
		}
		$pProvider = !empty($_POST['p_provider']) ? $_POST['p_provider'] : $_GET['p_provider'];
		if (!empty($pProvider)) {
			$queryProducts->andWhere('np.name IN (:providerList)')
										->setParameter(':providerList', $pProvider, Connection::PARAM_STR_ARRAY); 
		}
		$pCategory = !empty($_POST['p_category']) ? $_POST['p_category'] : $_GET['p_category'];
		if (!empty($pCategory)) {
			$queryProducts->andWhere('nc.name IN (:categoryList)')
										->setParameter(':categoryList', $pCategory, Connection::PARAM_STR_ARRAY); 
		}
		// ignore products with specified categories
		$queryProducts->andWhere('nc.name NOT IN (:categoryIgnoredList)')
									->setParameter(':categoryIgnoredList', $this->categoriesIgnoredArray, Connection::PARAM_STR_ARRAY);

		// ORDER by the price assigned to this user or by specific filter categories order (default)
		if (!empty($_POST['p_price_sort'])) {
			$queryProducts->orderBy('uprice', $_POST['p_price_sort']);
		} else {
			$queryProducts->add('orderBy', 'FIELD(nc.name,:categories)')
										->setParameter(':categories', $this->categoriesOrderArray, Connection::PARAM_STR_ARRAY);
			// display product with images first
			$queryProducts->addOrderBy('p.filename', 'DESC'); 
		}

		// SET LIMIT for pagination (get products only for current page)
		$queryProducts->setFirstResult(($page - 1) * $perPage);
		$queryProducts->setMaxResults($perPage);

		$response = $queryProducts->execute()->fetchAll();
		return $response;
	}

	public function getFiltersByType($filter) 
	{
		$db = $this->app->make('database')->connection('cubbis_module');
		$qb = $db->createQueryBuilder();

		// SELECT group of unique filter values and count products for each one
		if ($filter == 'category') {
			$queryProducts = $qb->select('IFNULL(nc.name,"") as title, COUNT(nc.name) AS count')->from('nomen_products', 'p');
			// ugly but filter categories needs to be displayed in specific order
			$queryProducts->add('orderBy', 'FIELD(nc.name,:categories)')
										->setParameter(':categories', $this->categoriesOrderArray, Connection::PARAM_STR_ARRAY);
			$queryProducts->groupBy('nc.name');
		}
		if ($filter == 'provider') {
			$queryProducts = $qb->select('IFNULL(np.name,"") as title, COUNT(np.name) AS count')->from('nomen_products', 'p');
			$queryProducts->groupBy('np.name');
		}
		if ($filter == 'color') {
			$queryProducts = $qb->select("p.color AS title, COUNT(p.color) AS count")->from('nomen_products', 'p');
			$queryProducts->groupBy('p.color');
			// don't display filter color for products with empty color value
			$queryProducts->andWhere("p.color <> ''");
		}

		// ugly but optimizes the query to get only the price assigned to this user
		if ($this->isPartnerType) {
			$queryProducts->addSelect(array('IF(p.'.$this->partnerPrice.'>0, p.'.$this->partnerPrice.', p.price) as uprice'));
			if (!empty($_POST['p_price_min']) && !empty($_POST['p_price_max'])) {
				$queryProducts->andWhere(
					$queryProducts->expr()->orX(
						$queryProducts->expr()->andX(
							'p.'.$this->partnerPrice.' > 0',
							'p.'.$this->partnerPrice.' >= :price_min',
							'p.'.$this->partnerPrice.' <= :price_max'
						),
						$queryProducts->expr()->andX('p.price >= :price_min', 'p.price <= :price_max')
					)
				);
				$queryProducts->setParameter(':price_min', $_POST['p_price_min']);
				$queryProducts->setParameter(':price_max', $_POST['p_price_max']);
			}
		} else {
			$queryProducts->addSelect('p.price as uprice');
			if (!empty($_POST['p_price_min']) && !empty($_POST['p_price_max'])) {
				$queryProducts->andWhere('p.price >= :price_min')->setParameter(':price_min', $_POST['p_price_min']);
				$queryProducts->andWhere('p.price <= :price_max')->setParameter(':price_max', $_POST['p_price_max']);
			}
		}

		// JOINS
		$queryProducts->join('p', 'nomen_providers', 'np', 'p.nomen_provider_id = np.id');
		$queryProducts->join('p', 'nomen_product_categories', 'nc', 'p.nomen_product_categories_id = nc.id');

		// APPLY filters
		if (!empty($_POST['p_search'])) {
			$queryProducts->andWhere(
				$queryProducts->expr()->orX(
					$queryProducts->expr()->like('p.name', ':search'),
					$queryProducts->expr()->like('p.code', ':search'),
					$queryProducts->expr()->like('nc.name', ':search'),
					$queryProducts->expr()->like('np.name', ':search'),
					$queryProducts->expr()->like('p.description', ':search'))
			)->setParameter(':search', '%'.$_POST['p_search'].'%');
		}
		// for current applied filter, count products should not be recalculated, it remains the same
		if (!empty($_POST['p_color']) && $filter != 'color') {
			$queryProducts->andWhere('p.color IN (:colorList)')
										->setParameter(':colorList', $_POST['p_color'], Connection::PARAM_STR_ARRAY); 
		}
		$pProvider = !empty($_POST['p_provider']) ? $_POST['p_provider'] : $_GET['p_provider'];
		if (!empty($pProvider) && $filter != 'provider') {
			$queryProducts->andWhere('np.name IN (:providerList)')
										->setParameter(':providerList', $pProvider, Connection::PARAM_STR_ARRAY); 
		}
		$pCategory = !empty($_POST['p_category']) ? $_POST['p_category'] : $_GET['p_category'];
		if (!empty($pCategory) && $filter != 'category') {
			$queryProducts->andWhere('nc.name IN (:categoryList)')
										->setParameter(':categoryList', $pCategory, Connection::PARAM_STR_ARRAY); 
		}
		// ignore products with specified categories
		$queryProducts->andWhere('nc.name NOT IN (:categoryIgnoredList)')
									->setParameter(':categoryIgnoredList', $this->categoriesIgnoredArray, Connection::PARAM_STR_ARRAY);

		$response = $queryProducts->execute()->fetchAll();
		return $response;
	}

	public function getFilterStyleColors()
	{
		$db = $this->app->make('database')->connection();
		$filterColors = $db->fetchAll('SELECT * FROM btCubbisProductStyleColors WHERE bID = ? ORDER BY sortOrder', [$this->bID]);
		return $filterColors;
	}

	public function getFilterStyleProviders()
	{
		$db = $this->app->make('database')->connection();
		$filterProviders = $db->fetchAll('SELECT * FROM btCubbisProductStyleProviders WHERE bID = ? ORDER BY sortOrder', [$this->bID]);
		return $filterProviders;
	}

	public function setFilterByType($filterList=array(), $filterName=null, $filterLabel=null) 
	{
		$html = '';
		if (!empty($filterList)) {
			$html = '<h5>'.$filterLabel.'</h5>';
			$html .= '<ul class="list-scroll">';
			foreach ($filterList as $k => $item) {
				$checked = '';
				$id = $filterName.'_'.$k;
				$title = $item['title'];
				$count = $item['count'];

				switch ($filterName) {
					case 'p_category':
						$filterType = !empty($_POST['p_category']) ? $_POST['p_category'] : $_GET['p_category'];
						$filterStyle = null;
						break;
					case 'p_provider':
						$filterType = !empty($_POST['p_provider']) ? $_POST['p_provider'] : $_GET['p_provider'];
						$filterStyle = 'provider provider_' . preg_replace('/\W+/', '', strtolower($title));
						break;
					case 'p_color':
						$filterType = $_POST[$filterName];
						$filterStyle = 'color color_' . preg_replace('/\W+/', '', strtolower($title));
						break;
					default:
						$filterType = $_POST[$filterName];
						$filterStyle = null;
				}

				$queryFilters = !empty($filterType) ? array_map('strtolower', (array)$filterType) : array();
				if (in_array(strtolower($title), $queryFilters)) { // ... || $title == $_POST['p_search'] //
					$checked = 'checked';
				}
				$html .= '<li>';
				$html .= 	'<input class="filter-change" type="checkbox" id="'.$id.'" name="'.$filterName.'[]" value="'.$title.'" '.$checked.' />';
				$html .= 	'<label for="'.$id.'">';
				if ($filterStyle) {
					$html .= 	'<span class="'.$filterStyle.'"></span>';
				}
				$html .= 		'<span class="title">'.$title.'</span>';
				$html .= 		'<span class="count">'.$count.'</span>';
				$html .= 	'</label>';
				$html .= '</li>';
			}
			$html .= '</ul>';
		}
		return $html;
	}

	public function setProductList($products=array()) 
	{
		$html = '';
		$imgPlaceholderPath = '/packages/theme_cubbis/themes/cubbis/images/placeholder.jpg';
		$imgFilePath = $this->appExternalFilePath;
		foreach($products as $product) {
			$imgSrc = $imgFilePath.'/'.$product['filename'];
			$html .= '<div class="product-item">';
			$html .= 	'<div class="product-item-inner">';
			if (!empty($product['filename'])) {
				$html .= 	'<div class="product-image image-style-zoom">';
				$html .= 		'<div class="img-zoom">';
				$html .= 			'<img class="lazyload" src="'.$imgPlaceholderPath.'" data-src="'.$imgSrc.'" data-litebox="image" />';
				$html .= 		'</div>';
				$html .= 	'</div>';
			} else {
				$html .= 	'<div class="product-image">';
				$html .= 		'<img src="'.$imgPlaceholderPath.'" />';
				$html .= 	'</div>';
			}
			$html .= 		'<div class="product-details">';
			$html .= 			'<h3 class="name">'.$product['name'].'</h3>';
			$html .= 			'<span class="type">'.$product['code'].'</span>';
			$html .= 			'<span class="type">'.$product['category'].'</span>';
			$html .= 			'<span class="type">'.$product['provider'].'</span>';
			if (!empty($product['description'])) {
				$html .= 		'<p class="description">'.$product['description'].'</p>';
			}
			$html .= 			'<span class="price">'.number_format($product['uprice'],2,'.','').' lei</span>';
			// remove Cart functionality
			// $html .= 			'<div class="add-to-cart">';
			// $html .= 				'<div class="quantity">';
			// $html .= 					'<span class="btn-qty down"></span>';
			// $html .= 					'<input type="number" value="1" data-id="'.$product['id'].'" />';
			// $html .= 					'<span class="btn-qty up"></span>';
			// $html .= 				'</div>';
			// $html .= 				'<button type="button" class="btn-style">'.$this->cartLinkTitle.'</button>';
			// $html .= 			'</div>';
			$html .= 		'</div>';
			$html .= 	'</div>';
			$html .= '</div>';
		}
		return $html;
	}

	public function setProductListEmpty() 
	{
		$c = Page::getCurrentPage();
		$html = '<div class="container">';
		$html .= 	'<div class="product-list-empty">';
		$html .= 		'<p>'.t('Nu s-au gasit produse care sa corespunda selectiei dvs.').'</p>';
		$html .= 		'<a href="'.$c->getCollectionLink().'" class="btn-style small">';
		$html .= 			t('Reseteaza filtrele').'<i class="fa fa-refresh"></i>';
		$html .= 		'</a>';
		$html .= 	'</div>';
		$html .= '</div>';
		return $html;
	}

	public function setProductListPagination($page=1, $pages=1) 
	{
		$html = '';
		if ($pages > 1) {
      $value = ($page > 1) ? ($page - 1) : 1;
      $disabled = $page == 1 ? 'disabled' : '';
      $html = '<ul>';
			$html .= '<li>';
			$html .= 	'<input id="p_page_first" class="filter-change" type="radio" name="p_page" value="1" '.$disabled.'/>';
			$html .= 	'<label for="p_page_first"><i class="fa fa-angle-double-left"></i></label>';
			$html .= '</li>';
			$html .= '<li>';
			$html .= 	'<input id="p_page_prev" class="filter-change" type="radio" name="p_page" value="'.$value.'" '.$disabled.'/>';
			$html .= 	'<label for="p_page_prev"><i class="fa fa-angle-left"></i></label>';
			$html .= '</li>';
			if (($page - 2) > 0) {
        $checked = $page == 1 ? 'checked' : '';
				$html .= ($page == 1) ? '<li class="active">' : '<li>';
				$html .= 	'<input id="p_page_1" class="filter-change" type="radio" name="p_page" value="1" '.$checked.'/>';
				$html .= 	'<label for="p_page_1">1</label>';
				$html .= '</li>';
			}
			if (($page - 2) > 1) {
        $html .= '<li><span class="dots">...</span></li>';
			}
			for ($i = ($page - 1); $i <= ($page + 1); $i++)	{
				if ($i < 1) { continue; }
				if ($i > $pages) { break; }
        $checked = $page == $i ? 'checked' : '';
				$html .= ($page == $i) ? '<li class="active">' : '<li>';
				$html .= 	'<input id="p_page_'.$i.'" class="filter-change" type="radio" name="p_page" value="'.$i.'" '.$checked.'/>';
				$html .= 	'<label for="p_page_'.$i.'">'.$i.'</label>';
				$html .= '</li>';
			}
			if (($pages - ($page + 1)) > 1) {
				$html .= '<li><span class="dots">...</span></li>';
			}
			if (($pages - ($page + 1)) > 0) {
        $checked = $page == $pages ? 'checked' : '';
				$html .= ($page == $pages) ? '<li class="active">' : '<li>';
				$html .= 	'<input id="p_page_'.$pages.'" class="filter-change" type="radio" name="p_page" value="'.$pages.'" '.$checked.'/>';
				$html .= 	'<label for="p_page_'.$pages.'">'.$pages.'</label>';
				$html .= '</li>';
			}
			$value = $page + 1;
			$disabled = $page == $pages ? 'disabled' : '';
			$html .= '<li>';
			$html .= 	'<input id="p_page_next" class="filter-change" type="radio" name="p_page" value="'.$value.'" '.$disabled.'/>';
			$html .= 	'<label for="p_page_next"><i class="fa fa-angle-right"></i></label>';
			$html .= '</li>';
			$html .= '<li>';
			$html .= 	'<input id="p_page_last" class="filter-change" type="radio" name="p_page" value="'.$pages.'" '.$disabled.'/>';
			$html .= 	'<label for="p_page_last"><i class="fa fa-angle-double-right"></i></label>';
			$html .= '</li>';
			$html .= '</ul>';
		}
		return $html;
	}

	public function action_filter() 
	{
		$response = $this->getProductViewData();
		echo json_encode($response);
		exit();
	}

	public function action_search() 
	{
		$response = $this->getProductViewData();
		echo json_encode($response);
		exit();
	}

	// remove Cart functionality
	// public function action_shopping() 
	// {
	// 	$session = $this->app->make('session');
	// 	$cartList = is_array($session->get('cartList')) ? $session->get('cartList') : array();

	// 	if (!empty($_POST['p_id'])) {
	// 		if (!empty($cartList[$_POST['p_id']])) {
	// 			$cartList[$_POST['p_id']] += (int)$_POST['p_quantity'];
	// 		} else {
	// 			$cartList[$_POST['p_id']] = (int)$_POST['p_quantity'];
	// 		}
	// 		$session->set('cartList', $cartList);

	// 		echo json_encode(array(
	// 			'success' => true,
	// 			'cartTotal' => count($cartList)
	// 		));
	// 	} else {
	// 		header('HTTP/1.0 404 Not Found');
	// 		echo 'product ID not found';
	// 	}
	// 	exit();
	// }
}
