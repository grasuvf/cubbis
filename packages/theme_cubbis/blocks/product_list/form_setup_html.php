<?php defined('C5_EXECUTE') or die("Access Denied.");

$fp = FilePermissions::getGlobal();
$tp = new TaskPermission();

$getBlockString = Core::make('helper/validation/identifier')->getString(18);
$getFilterString = Core::make('helper/validation/identifier')->getString(18);
$tabsBlock = [
	['filters-' . $getBlockString, t('Filters Style'), true],
	['controls-' . $getBlockString, t('View Controls')],
	['notifications-' . $getBlockString, t('Notifications')],
];
$tabsFilter = [
	['categories-' . $getFilterString, t('Categories'), true],
	['colors-' . $getFilterString, t('Colors')],
	['providers-' . $getFilterString, t('Providers')],
];
$categories_order_msg = "Add all product categories in the desired order divided by commas, no spaces between!";
?>

<script>
	$(document).ready(function() {
		var ccmReceivingEntry = '';
		var colorEntriesContainer = $('.ccm-color-entries-<?= $bID; ?>');
		var providerEntriesContainer = $('.ccm-provider-entries-<?= $bID; ?>');
		var _templateColor = _.template($('#colorTemplate-<?= $bID; ?>').html());
		var _templateProvider = _.template($('#providerTemplate-<?= $bID; ?>').html());

		var attachFileManagerLaunch = function($obj) {
			$obj.click(function() {
				var oldLauncher = $(this);
				ConcreteFileManager.launchDialog(function(data) {
					ConcreteFileManager.getFileDetails(data.fID, function(r) {
						jQuery.fn.dialog.hideLoader();
						var file = r.files[0];
						oldLauncher.html(file.resultsThumbnailImg);
						oldLauncher.next('.image-fID').val(file.fID);
					});
				});
			});
		};

		var doSortCount = function(element) {
			element.find('.ccm-item-entry').each(function(index) {
				$(this).find('.ccm-item-entry-sort').val(index);
			});
		};

		var attachDelete = function($obj) {
			$obj.click(function() {
				var deleteIt = confirm('<?= t('Are you sure?'); ?>');
				if (deleteIt === true) {
					$(this).closest('.ccm-item-entry').remove();
					doSortCount($(this).closest('.ccm-item-entries'));
				}
			});
		};

		<?php
		if (!empty($filterStyleColors)) {
			foreach ($filterStyleColors as $row) {
				?>
				colorEntriesContainer.append(_templateColor({
					title: '<?= addslashes(h($row['title'])); ?>',
					code: '<?= addslashes(h($row['code'])); ?>',
					sort_order: '<?= $row['sortOrder']; ?>'
				}));
				<?php
			}
		} 
		if (!empty($filterStyleProviders)) {
			foreach ($filterStyleProviders as $row) {
				?>
				providerEntriesContainer.append(_templateProvider({
					fID: '<?= $row['fID']; ?>',
					<?php if (File::getByID($row['fID'])) { ?>
					image_url: '<?= File::getByID($row['fID'])->getThumbnailURL('file_manager_listing'); ?>',
					<?php } else { ?>
					image_url: '',
					<?php } ?>
					title: '<?= addslashes(h($row['title'])); ?>',
					sort_order: '<?= $row['sortOrder']; ?>'
				}));
				<?php
			}
		} 
		?>

		doSortCount(colorEntriesContainer);
		doSortCount(providerEntriesContainer);

		var newItem;
		$('.ccm-add-color-entry-<?= $bID; ?>').click(function() {
			colorEntriesContainer.append(_templateColor({
				title: '',
				code: '',
				sort_order: ''
			}));
			var newItem = $('.ccm-color-entry-<?= $bID; ?>').last();
			attachDelete(newItem.find('.ccm-delete-color-entry-<?= $bID; ?>'));
			doSortCount(colorEntriesContainer);
		});
		$('.ccm-add-provider-entry-<?= $bID; ?>').click(function() {
			providerEntriesContainer.append(_templateProvider({
				fID: '',
				image_url: '',
				title: '',
				sort_order: ''
			}));
			var newItem = $('.ccm-provider-entry-<?= $bID; ?>').last();
			attachDelete(newItem.find('.ccm-delete-provider-entry-<?= $bID; ?>'));
			attachFileManagerLaunch(newItem.find('.ccm-pick-slide-image'));
			doSortCount(providerEntriesContainer);
		});

		attachDelete($('.ccm-delete-provider-entry-<?= $bID; ?>'));
		attachFileManagerLaunch($('.ccm-pick-slide-image-<?= $bID; ?>'));
	});
</script>

<style>
	.ccm-product-filters .form-group {
		margin: 0px!important;
		padding: 0px!important;
		margin-right: 0px!important;
		border-bottom: none!important;
	}
	.ccm-product-filters .form-group_half {
		float: left;
		width: 50%;
		padding-right: 10px;
	}
	.ccm-product-filters .form-group_inline {
		float: left;
		padding-right: 10px;
	}
	.ccm-product-filters .form-group_padd_right {
		padding-right: 10px;
	}
	.ccm-product-filters .form-group_inline.inline_image {
		width: 70px;
	}
	.ccm-product-filters .form-group_inline.inline_input {
		width: 80%;
		width: calc(100% - 70px);
	}
	.ccm-product-filters input[type="text"] {
		display: block;
		width: 100%;
		height: 33px;
	}
	.ccm-product-filters .btn-success {
		margin-bottom: 10px;
		padding: 5px 20px;
	}
	.ccm-item-entries {
		padding-bottom: 0px;
		position: relative;
	}
	.ccm-pick-slide-image {
		padding: 5px;
		cursor: pointer;
		background: #dedede;
		border: 1px solid #cdcdcd;
		text-align: center;
		vertical-align: middle;
		width: 60px;
		height: 60px;
		display: table-cell;
	}
	.ccm-pick-slide-image img {
		max-width: 100%;
	}
	.ccm-item-entry {
		position: relative;
		padding: 10px 90px 10px 10px;
		margin-bottom: 10px;
		background-color: #f5f5f5;
		border: 1px solid #e3e3e3;
		border-radius: 4px;
		-moz-box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
		-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
		box-shadow: inset 0 1px 1px rgba(0,0,0,0.05);
	}
	.btn.btn-danger {
		position: absolute;
		bottom: 10px;
		right: 10px;
		padding: 5px 12px;
	}
	.ccm-ui .form-group {
		margin-bottom: 20px;
	}
	div.ccm-ui .ccm-tab-content {
		padding-top: 10px;
	}
	div.ccm-ui ul.nav-tabs {
		padding: 0;
	}
	div.ccm-ui ul.nav-tabs>li {
		width: 33.33%;
		text-align: center;
	}
	.ccm-ui label.launch-tooltip {
		border-bottom: none;
	}
	.ccm-ui .tooltip-inner {
		background-color: #2076af;
	}
	.ccm-ui .tooltip.top .tooltip-arrow {
		border-top-color: #2076af;
	}
	.ccm-ui textarea {
		resize: none;
	}
</style>

<p style="color: #daa520">For a proper display, add this block directly to Main Content Area.</p>

<?php echo Core::make('helper/concrete/ui')->tabs($tabsBlock); ?>

<div id="ccm-tab-content-filters-<?= $getBlockString; ?>" class="ccm-tab-content ccm-product-filters">

	<?php echo Core::make('helper/concrete/ui')->tabs($tabsFilter); ?>

	<div id="ccm-tab-content-categories-<?= $getFilterString; ?>" class="ccm-tab-content">
		<div class="form-group">
			<?php
			echo $form->label('filterCategoryLabel', t('Category Label'));
			echo $form->text('filterCategoryLabel', $filterCategoryLabel ? $filterCategoryLabel : t('Categorie'));
			?>
		</div>
		<br/>
		<div class="form-group">
			<?php
			echo $form->label(
				'filterCategoriesOrder', 
				t('Category List Order'). ' '.'<i class="fa fa-question-circle launch-tooltip" title="'.$categories_order_msg.'"></i>'
			);
			echo $form->textarea('filterCategoriesOrder', $filterCategoriesOrder, ['rows' => 2]);
			?>
		</div>
		<br/>
		<div class="form-group">
			<?php
			echo $form->label(
				'filterCategoriesIgnored', 
				t('Category List Ignored'). ' '.'<i class="fa fa-question-circle launch-tooltip" title="'.$categories_order_msg.'"></i>'
			);
			echo $form->textarea('filterCategoriesIgnored', $filterCategoriesIgnored, ['rows' => 2]);
			?>
		</div>
	</div>

	<div id="ccm-tab-content-colors-<?= $getFilterString; ?>" class="ccm-tab-content">
		<div class="form-group">
			<?php
			echo $form->label('filterColorLabel', t('Color Label'));
			echo $form->text('filterColorLabel', $filterColorLabel ? $filterColorLabel : t('Culoare'));
			?>
		</div>
		<br/>
		<div class="ccm-item-entries ccm-color-entries-<?= $bID; ?>"></div>
		<div>
			<button type="button" class="btn btn-success ccm-add-color-entry ccm-add-color-entry-<?= $bID; ?>"><?= t('Add'); ?></button>
		</div>
	</div>

	<div id="ccm-tab-content-providers-<?= $getFilterString; ?>" class="ccm-tab-content">
		<div class="form-group">
			<?php
			echo $form->label('filterProviderLabel', t('Provider Label'));
			echo $form->text('filterProviderLabel', $filterProviderLabel ? $filterProviderLabel : t('Provider'));
			?>
		</div>
		<br/>
		<div class="ccm-item-entries ccm-provider-entries-<?= $bID; ?>"></div>
		<div>
			<button type="button" class="btn btn-success ccm-add-provider-entry ccm-add-provider-entry-<?= $bID; ?>"><?= t('Add'); ?></button>
		</div>
	</div>
</div>

<div id="ccm-tab-content-controls-<?= $getBlockString; ?>" class="ccm-tab-content">
	<div class="form-group">
		<?php
		echo $form->label('itemsPerPageLabel', t('Items Per Page Label'));
		echo $form->text('itemsPerPageLabel', $itemsPerPageLabel ? $itemsPerPageLabel : t('Produse pe pagina'));
		?>
	</div>
	<div class="form-group">
		<?php
		echo $form->label('sortDefaultLabel', t('Sorting Default Label'));
		echo $form->text('sortDefaultLabel', $sortDefaultLabel ? $sortDefaultLabel : t('Ordoneaza dupa'));
		?>
	</div>
	<div class="form-group">
		<?php
		echo $form->label('filterPriceLabel', t('Price Label'));
		echo $form->text('filterPriceLabel', $filterPriceLabel ? $filterPriceLabel : t('Pret'));
		?>
	</div>
	<div class="form-group">
		<?php
		echo $form->label('sortAscPriceLabel', t('Sorting by Price Ascending Label'));
		echo $form->text('sortAscPriceLabel', $sortAscPriceLabel ? $sortAscPriceLabel : t('Pret crescator'));
		?>
	</div>
	<div class="form-group">
		<?php
		echo $form->label('sortDescPriceLabel', t('Sorting by Price Descending Label'));
		echo $form->text('sortDescPriceLabel', $sortDescPriceLabel ? $sortDescPriceLabel : t('Pret descrescator'));
		?>
	</div>
	<!-- remove Cart functionality -->
	<!-- <div class="form-group">
		<?php
		// echo $form->label('cartLinkTitle', t('Shopping List Link Title'));
		// echo $form->text('cartLinkTitle', $cartLinkTitle ? $cartLinkTitle : t('Adauga in lista'));
		?>
	</div> -->
</div>

<div id="ccm-tab-content-notifications-<?= $getBlockString; ?>" class="ccm-tab-content">
	<div class="form-group">
		<?php
			echo $form->label('errorAlertFilter', t('Error Alert on Filter Products') . ' <small>(short text)</small>');
			echo $form->textarea(
				'errorAlertFilter', 
				$errorAlertFilter ? h($errorAlertFilter) : t('Ceva nu a mers bine. Incarca din nou pagina si mai incearca o data.'), 
				['rows' => 2]
			);
		?>
	</div>
	<!-- remove Cart functionality -->
	<!-- <div class="form-group">
		<?php
			// echo $form->label('errorAlertCart', t('Error Alert on Add to Cart') . ' <small>(short text)</small>');
			// echo $form->textarea(
			// 	'errorAlertCart', 
			// 	$errorAlertCart ? h($errorAlertCart) : t('Produsul nu a fost adaugat. Mai incearca o data.'), 
			// 	['rows' => 2]
			// );
		?>
	</div>
	<div class="form-group">
		<?php
			// echo $form->label('successAlertCart', t('Success Alert on Add to Cart') . ' <small>(short text)</small>');
			// echo $form->textarea(
			// 	'successAlertCart', 
			// 	$successAlertCart ? h($successAlertCart) : t('Produsul a fost adaugat cu succes.'), 
			// 	['rows' => 2]
			// );
		?>
	</div> -->
</div>

<script type="text/template" id="colorTemplate-<?= $bID; ?>">
	<div class="ccm-item-entry ccm-color-entry ccm-color-entry-<?= $bID; ?>">
		<div class="form-group clearfix">
			<div class="form-group_half">
				<label class="control-label"><?= t('Title'); ?></label>
				<input class="form-control ccm-input-text" type="text" name="<?= $view->field('colorTitle'); ?>[]" value="<%=title%>" />
			</div>
			<div class="form-group_half">
				<label class="control-label"><?= t('Code'); ?></label>
				<input class="form-control ccm-input-text" type="text" name="<?= $view->field('colorCode'); ?>[]" value="<%=code%>" />
			</div>
		</div>
		<button type="button" class="btn btn-sm btn-danger ccm-delete-color-entry-<?= $bID; ?>"><?= t('Remove'); ?></button>
		<input class="ccm-item-entry-sort" type="hidden" name="<?= $view->field('colorSortOrder'); ?>[]" value="<%=sort_order%>"/>
	</div>
</script>

<script type="text/template" id="providerTemplate-<?= $bID; ?>">
	<div class="ccm-item-entry ccm-provider-entry ccm-provider-entry-<?= $bID; ?>">
		<div class="form-group clearfix">
			<div class="form-group_inline inline_image">
				<div class="ccm-pick-slide-image ccm-pick-slide-image-<?= $bID; ?>">
					<% if (image_url.length > 0) { %> <img src="<%= image_url %>" /> <% } else { %> <i class="fa fa-picture-o"></i> <% } %>
				</div>
				<input type="hidden" name="<?= $view->field('fID'); ?>[]" class="image-fID" value="<%=fID%>" />
			</div>
			<div class="form-group_inline inline_input">
				<label class="control-label"><?= t('Title'); ?></label>
				<input class="form-control ccm-input-text" type="text" name="<?= $view->field('providerTitle'); ?>[]" value="<%=title%>" />
			</div>
		</div>
		<button type="button" class="btn btn-sm btn-danger ccm-delete-provider-entry-<?= $bID; ?>"><?= t('Remove'); ?></button>
		<input class="ccm-item-entry-sort" type="hidden" name="<?= $view->field('providerSortOrder'); ?>[]" value="<%=sort_order%>"/>
	</div>
</script>
