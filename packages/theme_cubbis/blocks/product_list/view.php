<?php defined('C5_EXECUTE') or die("Access Denied.");
$c = Page::getCurrentPage();

if ($c->isEditMode()) {
  ?>
  <div class="ccm-edit-mode-disabled-item" style="width: 100%; height: 100px">
    <div style="padding: 30px 0px 0px 0px"><?= t('Product List disabled in edit mode.') ?></div>
  </div>
  <?php
} else {
  ?>
  <div class="product-list-block">
    <?php if (!$productListEmpty) { ?>
    <form id="formFilterProducts" action="<?= $this->action('filter')?>" novalidate="true">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-3">
            <div class="product-filters mobile-sidebar mobile-sidebar_filters">
              <?php if(isset($priceLimitMin) && isset($priceLimitMax)) { ?>
              <div class="product-filters_price">
                <h5><?= $filterPriceLabel ? $filterPriceLabel : t('Pret'); ?></h5>
                <input type="text" id="priceSlider" style="display: none"
                        data-price-min="<?= $priceLimitMin ?>" 
                        data-price-max="<?= $priceLimitMax ?>" 
                />
                <div class="product-filter-range clearfix">
                  <input type="number" class="input-style price-min" name="p_price_min" value="" />
                  <input type="number" class="input-style price-max" name="p_price_max" value="" />
                  <button type="button" class="price-change btn-style"><i class="fa fa-chevron-right"></i></button>
                </div>
                <span class="divider"></span>
              </div>
              <?php } ?>
              <div class="product-filters_category"><?= $filterCategories ?></div>
              <div class="product-filters_color"><?= $filterColors ?></div>
              <div class="product-filters_provider"><?= $filterProviders ?></div>
            </div>
          </div>
          <div class="col-sm-12 col-md-9">
            <div class="product-panel clearfix">
              <div class="pull-left hidden-md hidden-lg">
                <a class="btn-sidebar-open btn-sidebar-toggle" href="javascript:void(0)" data-sidebar="open-sidebar_filters">
                  <span></span><span><?= t('Filtre') ?></span>
                </a>
              </div>
              <div class="product-panel-per_page hidden-xs hidden-sm">
                <span><?= $itemsPerPageLabel ? $itemsPerPageLabel : t('Produse pe pagina:') ?></span>
                <ul>
                  <?php foreach(array(30,60,90) as $key => $pp) { ?>
                  <li>
                    <input id="p_per_page_<?= $key ?>" class="filter-change" type="radio" style="display: none" 
                      name="p_per_page" value="<?= $pp ?>" 
                      <?php if($key == 0) { echo 'checked'; } ?>/>
                    <label for="p_per_page_<?= $key ?>"><?= $pp ?></label>
                  </li>
                  <?php } ?>
                </ul>
              </div>
              <div class="product-panel-view clearfix hidden-xs hidden-sm">
              <?php foreach(array(1,2,3) as $pw) { ?>
                <a href="javascript:void(0)" 
                    class="per-row-<?= $pw ?> <?= $pw == 3 ? 'active' : '' ?>" 
                    data-per-row="<?= $pw ?>"></a>
              <?php } ?>
              </div>
              <div class="product-panel-sort">
                <select name="p_price_sort" class="filter-change">
                  <option value="" selected="selected"><?= $sortDefaultLabel ? $sortDefaultLabel : t('Ordoneaza dupa') ?></option>
                  <option value="asc"><?= $sortAscPriceLabel ? $sortAscPriceLabel : t('Pret crescator') ?></option>
                  <option value="desc"><?= $sortDescPriceLabel ? $sortDescPriceLabel : t('Pret descrescator') ?></option>
                </select>
              </div>
            </div>
            <div class="product-list per-row-3" data-action="<?php //remove Cart functionality //echo $this->action('shopping') ?>"><?= $productList ?></div>
            <div class="product-list per-row-3"><?= $productList ?></div>
            <div class="product-pagination"><?= $productListPagination ?></div>
          </div>
        </div>
      </div>
    </form>
    <?php } else {
      echo $productList;
    } ?>
  </div>
  <?php
} ?>
