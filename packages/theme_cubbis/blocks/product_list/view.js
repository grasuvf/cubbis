$(document).ready(function() {
  window.history.pushState({}, document.title, document.location.pathname);

  var productElm, inputElem, inputVal;
  // global js variables added from controller including PHP data
  var errorAlertFilter = errAlertFilter ? errAlertFilter : 'Ceva nu a mers bine. Mai incearca o data.';
  // remove Cart functionality
  // var errorAlertCart = errAlertCartAdd ? errAlertCartAdd : 'Produsul nu a fost adaugat. Mai incearca o data.';
  // var successAlertCart = scsAlertCartAdd ? scsAlertCartAdd : 'Produsul a fost adaugat cu succes.';

  // PRODUCT VIEW CONTROL
  if ($('.product-filters_category input:checked').length) {
    var lastCheckedCategory = $('.product-filters_category input:checked:last').parent().position().top;
    $('.product-filters_category .list-scroll').scrollTop(lastCheckedCategory);
  }
  if ($('.product-filters_provider input:checked').length) {
    var lastCheckedProvider = $('.product-filters_provider input:checked:last').parent().position().top;
    $('.product-filters_provider .list-scroll').scrollTop(lastCheckedProvider);
  }

  $(document).on('click', '.product-panel-view > a', function(e) {
    e.preventDefault();
    
    var perRow = parseInt($(this).attr('data-per-row')) || 3;
    $('.product-list').removeClass (function (index, css) {
      return (css.match(/(^|\s)per-row-\S+/g) || []).join(' ');
    }).addClass('per-row-'+perRow);

    $('.product-panel-view > a').removeClass('active');
    $(this).addClass('active');
    return false;
  });

  // PRODUCT FILTERS
  var minPriceLimit = parseInt($('#priceSlider').attr('data-price-min'));
  var maxPriceLimit = parseInt($('#priceSlider').attr('data-price-max'));
  var minPriceInput = $('.product-filter-range').find('.price-min');
  var maxPriceInput = $('.product-filter-range').find('.price-max');
  var minMax;

  var priceSlider = new rSlider({
    target: '#priceSlider',
    values: {min: minPriceLimit, max: maxPriceLimit},
    step: 1,
    range: true,
    tooltip: true,
    labels: true,
    onChange: function (values) {
      minPriceInput.val(values.split(',')[0]);
      maxPriceInput.val(values.split(',')[1]);
    }
  });
  
  $(document).on('change', '.product-filter-range input', function(e) {
    minMax = parseInt($(this).val());
    if ($(this).hasClass('price-min')) {
      if (minMax >= minPriceLimit && minMax < parseInt(maxPriceInput.val())) {
        priceSlider.setValues(minMax, null);
      }
    }
    if ($(this).hasClass('price-max')) {
      if (minMax > parseInt(minPriceInput.val()) && minMax <= maxPriceLimit) {
        priceSlider.setValues(null, minMax);
      }
    }
  }).on('keyup', function (e) {
    if (e.keyCode === 13) {
      e.preventDefault();
      $(this).trigger('blur');
      $(this).trigger('focusout');
      ajaxFilterProducts();
      return false;
    }
  });

  $(document).on('click', '.price-change', function(e) {
    e.preventDefault();
    ajaxFilterProducts();
    return false;
  });

  $(document).on('change', '.filter-change', function(e) {
    e.preventDefault();
    // reset pagination when sorting
    if ($(this).attr('name') == 'p_price_sort') {
      $('.product-pagination input').val('');
    }
    ajaxFilterProducts($(this).attr('name') == 'p_page');
    return false;
  });

  // remove Cart functionality
  // PRODUCT ADD TO CART
  // $(document).on('click', '.add-to-cart button', function(e) {
  //   e.preventDefault();
  //   productElm = $(this).parents('.product-item');
  //   inputElem = productElm.find('input');

  //   if ((inputElem.val() > 0)) {
  //     $.ajax({
  //       url: $('.product-list').attr('data-action'),
  //       type: 'POST',
  //       data: $.param({
  //         'p_id': inputElem.attr('data-id'),
  //         'p_quantity': inputElem.val(),
  //       }),
  //       dataType: 'json',
  //       beforeSend: function() {
  //         productElm.find('.add-to-cart button').addClass('focused');
  //         productElm.find('.product-item-inner').addClass('preloader spinner');
  //       },
  //       success: function(response) {
  //         if (response.success) {
  //           $('.header-cart .total').text(response.cartTotal > 0 ? response.cartTotal : '');
  //           $('.header-cart .total').css('opacity', 1).animate(
  //             {opacity: 1},
  //             {
  //               duration: 300,
  //               step: function() { $(this).css('transform','scale(1.3,1.3)'); },
  //               complete: function() { $(this).css('transform','scale(1,1)'); }
  //             }
  //           );
  //           toastr.success(successAlertCart);
  //         } else {
  //           toastr.error(errorAlertCart);
  //         }
  //       },
  //       error: function(err) {
  //         toastr.error(errorAlertCart);
  //       },
  //       complete: function() {
  //         $('.add-to-cart button').removeClass('focused');
  //         $('.product-item-inner').removeClass('preloader spinner');
  //       }
  //     });
  //   } else {
  //     toastr.error(errorAlertCart);
  //   }
  //   inputElem.val(1);
  //   return false;
  // });

  // $(document).on('mousedown', '.add-to-cart .btn-qty', function(e) {
  //   e.preventDefault();
  //   addRemoveQuantity(this);
  //   return false;
  // });

  // $(document).on('keydown paste focusin focusout', '.add-to-cart input', function(e) {
  //   if (e.type == 'keydown') {
  //     var code = !e.charCode ? e.which : e.charCode;
  //     if (!(code === 39 || code === 37 || code === 46 || code === 8 || code >= 96 && code <= 105 || code >= 48 && code <= 57)) {
  //       e.preventDefault();
  //     }
  //   }
  //   if (e.type == 'paste') {
  //     var paste = (event.clipboardData || window.clipboardData).getData('text');
  //     if (paste <= 0) {
  //       e.preventDefault();
  //     }
  //   }
  //   if (e.type == 'focusin') {
  //     $(this).parents('.add-to-cart').find('button').addClass('focused');
  //   }
  //   if (e.type == 'focusout') {
  //     $(this).parents('.add-to-cart').find('button').removeClass('focused');
  //   }
  // });

  // METHODS
  function ajaxFilterProducts(scrollTop) {
    if (!(parseInt(minPriceInput.val()) < parseInt(maxPriceInput.val()))) {
      minPriceInput.val(minPriceLimit);
      maxPriceInput.val(maxPriceLimit);
      priceSlider.setValues(minPriceLimit, maxPriceLimit);
    }
    
    $.ajax({
      url: $('#formFilterProducts').attr('action'),
      type: 'POST',
      data: $('#formFilterProducts, .header-autonav .search-form')
        .serializeArray()
        .filter(function (i) {
          return i.value;
        }),
      dataType: 'json',
      beforeSend: function() {
        if (!!scrollTop) { 
          $(window).scrollTop(0); 
        }
        $('.product-filters').addClass('preloader');
        $('.product-list').addClass('preloader spinner');
      },
      success: function(response) {
        if (!response.productListEmpty) {
          $('.product-filters_category').html(response.filterCategories);
          $('.product-filters_color').html(response.filterColors);
          $('.product-filters_provider').html(response.filterProviders);
          $('.product-list').html(response.productList);
          $('.product-pagination').html(response.productListPagination);
        } else {
          $('.product-list-block').html(response.productList);
        }
      },
      error: function() {
        toastr.error(errorAlertFilter);
      },
      complete: function() {
        $('.product-filters').removeClass('preloader');
        $('.product-list').removeClass('preloader spinner');
      }
    });
  }

  // remove Cart functionality
  // function addRemoveQuantity(_this) {
  //   inputElem = $(_this).parent().find('input');
  //   inputVal = parseInt(inputElem.val());
  //   if ($(_this).hasClass('up')) inputElem.val(++inputVal);
  //   if ($(_this).hasClass('down') && inputVal > 1) inputElem.val(--inputVal);
  // }
});