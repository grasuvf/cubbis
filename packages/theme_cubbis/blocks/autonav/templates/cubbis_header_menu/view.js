$(document).ready(function() {
  var headerSubmenu = $('.nav-list .carousel-container');

  $(document).on('keyup', '.search-form input', function(e) {
    if (e.keyCode === 13) {
      e.preventDefault();
      $(this).trigger('blur');
      $(this).trigger('focusout');
      return false;
    }
  });
  
  // create or destroy header submenu carousel
  setSubmenuCarousel();

  $(window).on("resize", function() {
    if (992 < window.innerWidth) {
      setSubmenuCarousel();
    } else {
      if (headerSubmenu.hasClass('slick-initialized')) {
        headerSubmenu.slick('unslick');
      }
    }
  });

  function setSubmenuCarousel() {
    if (!headerSubmenu.hasClass('slick-initialized')) {
      headerSubmenu.slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 5,
        swipeToSlide: true
      });
    }
  }
});
