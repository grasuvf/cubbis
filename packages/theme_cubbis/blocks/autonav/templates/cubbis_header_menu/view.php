<?php defined('C5_EXECUTE') or die("Access Denied.");
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$navItems = $controller->getNavItems();
$c = Page::getCurrentPage();
$site = Site::getSite();

$productsPageAttr = $site->getAttribute('product_list_page');
$productsPageObj = $productsPageAttr ? Page::getByPath($productsPageAttr) : null;
$productsPageID = $productsPageObj ? $productsPageObj->getCollectionID() : null;

if (count($navItems) > 0) {
	echo '<div class="nav-list">'; //opens the top-level menu

	foreach ($navItems as $ni) {
		if ($ni->level == 1) {
			$navPageClass = $ni->hasSubmenu ? 'nav-page has-submenu' : 'nav-page';
			echo '<div class="'. $navPageClass .'">'; //open 1st level page
			echo '<a href="'. $ni->url .'" class="'. ($ni->inPath ? 'active' : '') .'">'. h($ni->name) .'</a>';

			if ($ni->hasSubmenu) {
				$navDropdownClass = $productsPageID == $ni->cID ? 'nav-dropdown category-dropdown' : 'nav-dropdown';
				echo '<span class="nav-dropdown-arrow"></span>';
				echo '<div class="'. $navDropdownClass .'">'; //opens a dropdown sub-menu
				echo '<div class="carousel-slick">';
				echo '<div class="carousel-container">';
			}
		}

		if ($ni->level == 2) {
			$thumbnail = $ni->cObj->getAttribute('thumbnail');
			if ($thumbnail) {
				$img = $app->make('helper/image');
				$thumb = $img->getThumbnail($thumbnail, 250, 250, true);
				$tag = new \HtmlObject\Image();
				$tag->src($thumb->src);
				$tag->alt($ni->name ? $ni->name : '');

				echo '<div class="carousel-item image-style-zoom">';
				echo 	  '<div class="img-zoom">'. $tag .'</div>';
				echo 		'<a class="nav-item-link" href="'. $ni->url .'"></a>';
				echo 		'<span class="nav-item-title btn-style">'. h($ni->name) .'</span>';
				echo '</div>';
			} else {
				echo '<div class="carousel-item">';
				echo 		'<a class="nav-item-link" href="'. $ni->url .'"></a>';
				echo 		'<span class="nav-item-title btn-style">'. h($ni->name) .'</span>';
				echo '</div>';
			}

			if ($ni->isLast) {
				echo '</div></div></div></div>'; //closes dropdown sub-menu elements and 1st level page parent
			}
		}

		if ($ni->level == 1 && !$ni->hasSubmenu) {
			echo '</div>'; //closes 1st level page without dropdown sub-menu
		}
	}

	echo '</div>'; //closes the top-level menu
} elseif (is_object($c) && $c->isEditMode()) {
	?>
	<div class="ccm-edit-mode-disabled-item"><?= t('Empty Auto-Nav Block.') ?></div>
<?php
}
