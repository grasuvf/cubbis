<?php defined('C5_EXECUTE') or die("Access Denied.");
$app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
$c = Page::getCurrentPage();

if (count($rows) > 0) {
	?>
	<div class="block-teaser-banner clearfix">
		<div class="teaser-banner-left <?= $zoomEffect == 1 ? 'image-style-zoom' : '' ?>">
			<?php
			$fL = File::getByID($rows[0]['fID']);
			if (is_object($fL) && $fL->getFileID()) {
				$imgL = $app->make('helper/image');
				$thumbL = $imgL->getThumbnail($fL, 800, 600, true);
				$imgTagL = new \HtmlObject\Image();
				$imgTagL->src($thumbL->src);
				$imgTagL->class('img-responsive');
				$imgTagL->alt('img');
				?>
				<div class="img-zoom">
					<?php echo $imgTagL; ?>

					<?php if ($rows[0]['linkURL']) { ?>
						<a href="<?= $rows[0]['linkURL']; ?>"></a>
					<?php } ?>
				</div>

				<?php if ($rows[0]['title']) { ?>
					<span><?= $rows[0]['title']; ?></span>
				<?php } ?>

				<?php
			} ?>
		</div>
		<div class="teaser-banner-right">
			<div class="teaser-banner-right-inner">
				<?php if ($heading) { ?>
					<h2><?= $heading ?></h2>
				<?php } ?>

				<?php if ($paragraph) { ?>
					<p><?= $paragraph ?></p>
				<?php } ?>

				<div class="teaser-banner-images col-<?= count($rows) - 1 ?> clearfix">
					<?php
					$countR = count($rows) - 1;
					$col = $countR == 4 ? 2 : $countR;
					foreach ($rows as $key => $row) {
						if ($key == 0) { continue; }
						if ($key >= 5) { return; }

						$fR = File::getByID($row['fID']);
						if (is_object($fR) && $fR->getFileID()) {
							$cropWidth = $countR == 4 ? 300 : (600 / $col);
							$cropHeight = 200;

							$imgR = $app->make('helper/image');
							$thumbR = $imgR->getThumbnail($fR, $cropWidth, $cropHeight, true);

							$imgTagR = new \HtmlObject\Image();
							$imgTagR->src($thumbR->src);
							$imgTagR->class('img-responsive');
							$imgTagR->alt('img');
							?>
							<div class="teaser-banner-image <?= $zoomEffect == 1 ? 'image-style-zoom' : '' ?>">
								<div class="img-zoom <?= $row['title'] ? 'text-zoom' : '' ?>">
									<?php echo $imgTagR; ?>

									<?php if ($row['linkURL']) { ?>
										<a href="<?= $row['linkURL']; ?>"></a>
									<?php  } ?>

									<?php if ($row['title']) { ?>
										<span><?= $row['title']; ?></span>
									<?php } ?>
								</div>
							</div>
						<?php
						}
					} ?>
				</div>
			</div>
		</div>
	</div>
	<?php
} elseif ($c->isEditMode()) {
	?>
	<div class="ccm-edit-mode-disabled-item"><?php echo t('Empty Teaser Banner Block'); ?></div>
	<?php
}
