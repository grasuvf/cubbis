<?php
namespace Concrete\Package\ThemeCubbis\Block\TeaserBanner;
use Concrete\Core\Block\BlockController;
use Core;
use Page;

class Controller extends BlockController
{
    protected $btTable = 'btCubbisTeaserBanner';
    protected $btInterfaceWidth = 500;
    protected $btInterfaceHeight = 600;
    protected $btWrapperClass = 'ccm-ui';
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btExportTables = ['btCubbisTeaserBanner', 'btCubbisTeaserBannerEntries'];
    protected $btDefaultSet = 'blocksetcubbis';

    public function getBlockTypeName()
    {
        return t('Teaser Banner');
    }

    public function getBlockTypeDescription()
    {
        return t('Teaser Banner widget with title, text and images with links');
    }

    public function add()
    {
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
    }

    public function edit()
    {
        $this->requireAsset('core/file-manager');
        $this->requireAsset('core/sitemap');
        $db = $this->app->make('database')->connection();
        $query = $db->fetchAll('SELECT * FROM btCubbisTeaserBannerEntries WHERE bID = ? ORDER BY sortOrder', [$this->bID]);
        $this->set('rows', $query);
    }

    public function view()
    {
        $db = $this->app->make('database')->connection();
        $query = $db->fetchAll('SELECT * FROM btCubbisTeaserBannerEntries WHERE bID = ? ORDER BY sortOrder', [$this->bID]);

        $rows = [];
        foreach ($query as $row) {
            if (!$row['linkURL'] && $row['internalLinkCID']) {
                $c = Page::getByID($row['internalLinkCID'], 'ACTIVE');
                $row['linkURL'] = $c->getCollectionLink();
                $row['linkPage'] = $c;
            }
            $rows[] = $row;
        }

        $this->set('rows', $rows);
    }

    public function duplicate($newBID)
    {
        parent::duplicate($newBID);
        $db = $this->app->make('database')->connection();
        $v = [$this->bID];
        $q = 'SELECT * FROM btCubbisTeaserBannerEntries WHERE bID = ?';
        $r = $db->executeQuery($q, $v);
        foreach ($r as $row) {
            $db->executeQuery('INSERT INTO btCubbisTeaserBannerEntries (bID, fID, linkURL, title, sortOrder, internalLinkCID) values(?,?,?,?,?,?)',
                [
                    $newBID,
                    $row['fID'],
                    $row['linkURL'],
                    $row['title'],
                    $row['sortOrder'],
                    $row['internalLinkCID'],
                ]
            );
        }
    }

    public function delete()
    {
        $db = $this->app->make('database')->connection();
        $db->executeQuery('DELETE FROM btCubbisTeaserBannerEntries WHERE bID = ?', [$this->bID]);
        parent::delete();
    }

    public function save($args)
    {
        $args += [
            'zoomEffect' => 0,
        ];
        $args['zoomEffect'] = isset($args['zoomEffect']) ? 1 : 0;

        $db = $this->app->make('database')->connection();
        $db->executeQuery('DELETE FROM btCubbisTeaserBannerEntries WHERE bID = ?', [$this->bID]);
        parent::save($args);

        $count = isset($args['sortOrder']) ? count($args['sortOrder']) : 0;
        $i = 0;
        while ($i < $count) {
            $linkURL = $args['linkURL'][$i];
            $internalLinkCID = $args['internalLinkCID'][$i];
            switch ((int) $args['linkType'][$i]) {
                case 1:
                    $linkURL = '';
                    break;
                case 2:
                    $internalLinkCID = 0;
                    break;
                default:
                    $linkURL = '';
                    $internalLinkCID = 0;
                    break;
            }

            $db->executeQuery('INSERT INTO btCubbisTeaserBannerEntries (bID, fID, title, sortOrder, linkURL, internalLinkCID) values(?,?,?,?,?,?)',
                [
                    $this->bID,
                    (int) $args['fID'][$i],
                    $args['title'][$i],
                    $args['sortOrder'][$i],
                    $linkURL,
                    $internalLinkCID,
                ]
            );
            ++$i;
        }
    }

    public function validate($args)
    {
        $error = Core::make('helper/validation/error');

        $i = 0;
        $count = isset($args['sortOrder']) ? count($args['sortOrder']) : 0;
        $poz = ["1st", "2nd", "3rd", "4th", "5th"];
        while ($i < $count) {
            if (!$args['fID'][$i]) {
                $error->add(t('Please select the ' . $poz[$i] . ' image.'));
            }
            ++$i;
        }

        return $error;
    }
}
