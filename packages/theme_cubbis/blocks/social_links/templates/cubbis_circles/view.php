<?php defined('C5_EXECUTE') or die("Access Denied."); ?>

<div id="social-links-<?= $bID; ?>" class="social-links">
	<ul class="list-inline">
		<?php foreach ($links as $link) {
			$service = $link->getServiceObject();
			if ($service) {
				?>
				<li class="icon_<?= strtolower($service->getDisplayName()); ?>">
					<a target="_blank" rel="noopener noreferrer" href="<?= h($link->getURL()); ?>"
						 aria-label="<?= $service->getDisplayName(); ?>"><?= $service->getServiceIconHTML(); ?></a>
				</li>
			<?php
			}
		} ?>
	</ul>
</div>
