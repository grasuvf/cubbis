<?php
namespace Concrete\Package\ThemeCubbis\Theme\Cubbis;
use Concrete\Core\Page\Theme\Theme;
use Page;

defined('C5_EXECUTE') or die('Access Denied.');

class PageTheme extends Theme
{
	protected $pThemeGridFrameworkHandle = 'bootstrap3';

	public function registerAssets()
	{
		$this->requireAsset('javascript', 'jquery');
		$this->requireAsset('css', 'font-awesome');
		$this->providesAsset('css', 'bootstrap/*');
		$this->providesAsset('javascript', 'bootstrap/*');

		$c = Page::getCurrentPage();
		if ($c->isEditMode()) {
			$this->requireAsset('core/colorpicker');
		}
	}

	public function getThemeName()
	{
		return t('Cubbis');
	}

	public function getThemeDescription()
	{
		return t('Cubbis Theme for Concrete5');
	}

	public function getThemeEditorClasses()
	{
		return array(
			array('title' => t('Button style'), 'element' => array('a'), 'attributes' => array('class' => 'btn-style')),
			array('title' => t('Button style small'), 'element' => array('a'), 'attributes' => array('class' => 'btn-style small')),
			array('title' => t('Table centered'), 'element' => array('table'), 'attributes' => array('class' => 'table-centered')),
		);
	}
}
