<?php defined('C5_EXECUTE') or die("Access Denied.");
	$site = Site::getSite();
  $productsPage = $site->getAttribute('product_list_page');
  $productsPage = !empty($productsPage) ? $productsPage : (BASE_URL . '/produse');
  $searchPlaceholder = t('Cauta produse');
?>

<form class="search-form" method="POST" action="<?= View::url($productsPage) ?>">
  <input type="text" name="p_search" class="input-style" value="<?= $_POST['p_search'] ?>" placeholder="<?= $searchPlaceholder ?>" />
  <button type="submit" value="search" class="btn-style"><i class="fa fa-search"></i></button>
</form>
