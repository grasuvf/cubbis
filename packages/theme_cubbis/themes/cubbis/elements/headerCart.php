<?php defined('C5_EXECUTE') or die("Access Denied.");
  $app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();
	$c = Page::getCurrentPage();
	$u = new \Concrete\Core\User\User;
  
	if ($c->isEditMode() || $u->getUserID() == -1) {
    ?>
      <div class="header-cart">
        <a href="#" class="icon-color"><i class="fa fa-shopping-cart"></i><span><?= t('Cosul meu') ?></span></a>
      </div>
    <?php
	} else {
    $site = Site::getSite();
    $orderCartPage = $site->getAttribute('order_cart_page');
    $orderCartPage = !empty($orderCartPage) ? $orderCartPage : (BASE_URL . '/cosul-meu');

    $session = $app->make('session');
    $cartList = $session->get('cartList');
    $cartTotal = !empty($cartList) ? count($cartList) : '';
    ?>
    <div class="header-cart">
      <a href="<?= $orderCartPage ?>" class="icon-color">
        <i class="fa fa-shopping-cart"><sup class="total"><?= $cartTotal ?></sup></i>
        <span><?= t('Cosul meu') ?></span>
      </a>
    </div>
    <?php
  }
?>
