<?php defined('C5_EXECUTE') or die("Access Denied.");
  $site = Site::getSite();
  $logo = $site->getAttribute('header_logo');
  $app = \Concrete\Core\Support\Facade\Application::getFacadeApplication();

  if (is_object($logo) && $logo->getFileID()) {
    $imgw = $logo->getAttribute('width');
    $imgh = $logo->getAttribute('height');
    $cropw = 300;
    $croph = (300 * $imgh) / $imgw;
    $img = $app->make('helper/image');
    $thumb = $img->getThumbnail($logo, $cropw, (int)$croph, true);
    $logoURL = $thumb->src;
    echo '<a href="/"><img src="' . $logoURL . '" alt="logo" /></a>';
  }
?>
