<?php defined('C5_EXECUTE') or die("Access Denied.");
	$c = Page::getCurrentPage();
	$p = new Permissions($c);
?>

<!DOCTYPE html>
<html lang="<?= Localization::activeLanguage() ?>">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <?php Loader::element('header_required', array('pageTitle' => $pageTitle)); ?>
  <link href="<?= $view->getThemePath() ?>/css/addons/bootstrap.min.css" rel="stylesheet" type="text/css">
  <link href="<?= $view->getThemePath() ?>/css/addons/slick.min.css" rel="stylesheet" type="text/css">
  <?php echo $html->css($view->getStylesheet('main.less')); ?>
</head>
<body class="<?= $p->canViewToolbar() ? 'admin-mode' : '' ?>">
<div class="page-container <?= $c->getPageWrapperClass() ?>">
  <header id="header" class="header">
    <div class="hidden-xs hidden-sm header-inner">
      <div class="container">
        <div class="row">
          <div class="col-md-5 header-info">
            <?php $this->inc('elements/headerInfo.php'); ?>
          </div>
          <div class="col-md-2 header-logo">
            <?php $this->inc('elements/headerLogo.php'); ?>
          </div>
          <div class="col-md-3 header-search">
            <?php $this->inc('elements/headerSearch.php'); ?>
          </div>
          <div class="col-md-2 header-user clearfix">
            <?php // remove Cart functionality //$this->inc('elements/headerCart.php'); ?>
            <?php $this->inc('elements/headerAccount.php'); ?>
          </div>
        </div>
      </div>
    </div>
    <div class="header-autonav">
      <div class="mobile-sidebar-overlay">
        <a class="btn-sidebar-close btn-sidebar-toggle" href="javascript:void(0)"></a>
      </div>
      <a class="btn-sidebar-open btn-sidebar-toggle" href="javascript:void(0)" data-sidebar="open-sidebar_header">
        <span></span><span><?= t('Meniu') ?></span>
      </a>
      <div class="header-logo_mobile">
        <?php $this->inc('elements/headerLogo.php'); ?>
      </div>
      <div class="header-user">
        <?php // remove Cart functionality //$this->inc('elements/headerCart.php'); ?>
      </div>
      <div class="container">
        <div class="mobile-sidebar mobile-sidebar_header">
          <div class="header-logo">
            <?php $this->inc('elements/headerLogo.php'); ?>
          </div>
          <div class="header-search">
            <?php $this->inc('elements/headerSearch.php'); ?>
          </div>
          <div class="header-nav">
            <?php
              $a = new GlobalArea('Header autonav');
              $a->display();
            ?>
          </div>
          <div class="header-info">
            <?php $this->inc('elements/headerInfo.php'); ?>
          </div>
          <div class="header-user">
            <?php $this->inc('elements/headerAccount.php'); ?>
          </div>
        </div>
      </div>
    </div>
  </header>
  <main id="main" class="main">
