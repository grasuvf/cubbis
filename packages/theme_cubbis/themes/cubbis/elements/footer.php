<?php defined('C5_EXECUTE') or die("Access Denied.");
	$c = Page::getCurrentPage();
	$p = new Permissions($c);
?>
	</main>
	<footer id="footer" class="footer">
		<div class="container">
			<div class="row">
				<div class="hidden-xs hidden-sm hidden-md col-lg-1"></div>
				<div class="col-md-2 col-md-push-5 col-lg-2 col-lg-push-4 logo">
					<?php
					$a = new GlobalArea('Footer Logo');
					$a->display();
					?>
				</div>
				<div class="col-md-5 col-md-pull-2 col-lg-4 col-lg-pull-2">
					<?php
					$a = new GlobalArea('Footer 1');
					$a->display();
					?>
				</div>
				<div class="col-md-5 col-lg-4">
					<?php
					$a = new GlobalArea('Footer 2');
					$a->display();
					?>
				</div>
				<div class="hidden-xs hidden-sm hidden-md col-lg-1"></div>
			</div>
			<div class="row">
				<div class="col-xs-12">
					<?php
					$a = new GlobalArea('Footer Bottom');
					$a->display();
					?>
				</div>
			</div>
		</div>
	</footer>
	<a href="javascript:void(0)" class="scroll-to-top">
		<i class="fa fa-chevron-up" aria-hidden="true"></i>
	</a>
</div>

<?php Loader::element('footer_required'); ?>

<script>
	if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
		var msViewportStyle = document.createElement('style')
		msViewportStyle.appendChild(
			document.createTextNode(
				'@-ms-viewport{width:auto!important}'
			)
		)
		document.querySelector('head').appendChild(msViewportStyle)
	}
</script>
<script src="<?= $view->getThemePath() ?>/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= $view->getThemePath() ?>/js/slick.min.js" type="text/javascript"></script>
<script src="<?= $view->getThemePath() ?>/js/main_redirect-ext.js" type="text/javascript"></script>

<?php if (!$p->canViewToolbar()) { ?>
	<script src="<?= $view->getThemePath() ?>/js/main.js"></script>
<?php } ?>
</body>
</html>
