<?php defined('C5_EXECUTE') or die("Access Denied.");
	$c = Page::getCurrentPage();
	$u = new \Concrete\Core\User\User;
  $ui = $u->getUserInfoObject();

	if ($c->isEditMode() || $u->getUserID() == -1) {
		?>
		<div class="header-account">
			<a href="#" class="icon-color"><i class="fa fa-user-circle-o"></i><span><?= t('Contul meu') ?></span></a>
		</div>
		<?php 
	} else {
		if ($u->isLoggedIn() && is_object($ui)) {
			$userName = $ui->getAttribute('user_first_name') ? $ui->getAttribute('user_first_name') : $ui->getUserName();
			$userEmail = $ui->getUserEmail();
			$userToken = $ui->getAttribute('user_token');
			$userTokenID = $ui->getAttribute('user_token_id');

			$site = Site::getSite();
			// remove Cart functionality
			// $orderListPage = $site->getAttribute('order_list_page');
			// $orderListPage = !empty($orderListPage) ? $orderListPage : (BASE_URL . '/comenzile-mele');
			$appExternalPage = $site->getAttribute('app_external_page');
			$appExternalPage = !empty($appExternalPage) ? $appExternalPage : 'https://app.cubbis.ro/app/login';
			$appExternalLink = str_replace('/app/login', '', $appExternalPage);
			?>
			<div class="header-account">
				<a href="javascript:void(0)" class="icon-color"><i class="fa fa-user-circle-o"></i><span><?= t('Contul meu') ?></span></a>
				<ul>
					<li class="greeting"><strong><?= t('Salut') ?>, <?= $userName ?></strong></li>
					<li class="divider"></li>
					<!-- <li><a href="<?php //echo $orderListPage ?>"><?php //echo t('Comenzile mele') ?></a></li> -->
					<?php if ($userTokenID && $userToken) { ?>
						<li><a class="app-external" href="/"><?= t('Status comenzi') ?></a></li>
					<?php } else { ?>
						<li><a class="app-external-link" href="<?= $appExternalLink ?>"><?= t('Status comenzi') ?></a></li>
					<?php } ?>
					<li class="divider"></li>
					<li class="logout"><a href="<?= $this->url('/login', 'logout', Loader::helper('validation/token')->generate('logout')) ?>"><?= t('Delogare') ?></a></li>
				</ul>
				<form method="POST" action="<?= $appExternalPage ?>" target="_blank">
					<input type="hidden" name="id" value="<?= $userTokenID ?>" />
					<input type="hidden" name="email" value="<?= $userEmail ?>" />
					<input type="hidden" name="password" value="<?= $userToken ?>" />
				</form>
			</div>
			<?php 
		} else {
			?>
			<div class="header-login">
				<a href="<?= $this->url('/login'); ?>" class="icon-color">
					<i class="fa fa-user-circle-o"></i><span><?= t('Autentificare') ?></span>
				</a>
			</div>
			<?php 
		}
	}
?>
