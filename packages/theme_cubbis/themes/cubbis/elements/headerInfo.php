<?php defined('C5_EXECUTE') or die("Access Denied.");
	$site = Site::getSite();
	$headerPhone = $site->getAttribute('header_phone');
	$headerPhoneLabel = t('Poti sa ne suni la:');
	$headerEmail = $site->getAttribute('header_email');
	$headerEmailLabel = t('Poti sa ne scrii la:');
?>

<?php if ($headerPhone) { ?>
	<a href="tel:<?= $headerPhone ?>" class="icon-color">
		<i class="fa fa-phone"></i><label><?= $headerPhoneLabel ?><span><?= $headerPhone ?></span></label>
	</a>
<?php } ?>
<?php if ($headerEmail) { ?>
	<a href="mailto:<?= $headerEmail ?>" class="icon-color">
		<i class="fa fa-envelope"></i><label><?= $headerEmailLabel ?><span><?= $headerEmail ?></span></label>
	</a>
<?php } ?>
