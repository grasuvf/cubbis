<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
<?php $this->inc('elements/header.php'); ?>

<div class="container text-center">
	<h1><?= t('Pagina interzisa!') ?></h1>
	<p><?= t('Nu esti autorizat sa accesezi aceasta pagina.') ?></p>
	<a class="btn-style small text-uppercase" href="/">
		<i class="fa fa-chevron-circle-left"></i>&nbsp;&nbsp;<?= t('Inapoi') ?>
	</a>
</div>

<?php $this->inc('elements/footer.php'); ?>
