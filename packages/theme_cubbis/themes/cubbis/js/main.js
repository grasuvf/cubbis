$(document).ready(function() {
  // header nav sticky
  var breakpoint = 992;
  var isOnMobile = false;
  var isOnDesktop = false;
  var header = $('.header');
  var scrollToTop = $('a.scroll-to-top');

  stickyHeader();
  toggleSidebarOnMobile();

  // scroll to top
  scrollToTop.on('click', function(e) {
    e.preventDefault();
    $('html, body').animate({ scrollTop: 0 }, 500);
    return false;
  });

  var currentScrollPos, prevScrollPos = $(document).scrollTop();
  $(window).on('scroll', function() {
    stickyHeader();
    currentScrollPos = $(document).scrollTop();
    if(currentScrollPos > 100 && prevScrollPos > currentScrollPos) {
      scrollToTop.addClass('show');
    } else {
      scrollToTop.removeClass('show');
    }
    prevScrollPos = currentScrollPos;
  });

  $(window).on('resize', function() {
    if (breakpoint < window.innerWidth) {
      if (isOnDesktop == false) {
        isOnDesktop = true;
        isOnMobile = false;
        stickyHeader();
      }
    } else {
      if (isOnMobile == false) {
        isOnMobile = true;
        isOnDesktop = false;
      }
    }
  });

  function stickyHeader() {
    var scroll = $(document).scrollTop();
    var headerHeight = header.outerHeight();

    if (breakpoint < window.innerWidth) {
      if (scroll > (headerHeight + 30)) {
        header.addClass('header-sticky');
      } else {
        header.removeClass('header-sticky');
      }
    } else {
      header.removeClass('header-sticky');
    }
  }

  function toggleSidebarOnMobile() {
    $('.btn-sidebar-toggle').on('click', function(e) {
      e.preventDefault();
      if($('body').hasClass('open-sidebar_')) {
        $('body').removeClass(function (index, css) {
          return (css.match(/(^|\s)open-sidebar\S+/g) || []).join(' ');
        });
      } else {
        $('body').addClass('open-sidebar_ ' + $(this).data('sidebar'));
      }
    });
    $('.mobile-sidebar-overlay').on('click', function(e) {
      e.preventDefault();
      $('body').removeClass(function (index, css) {
        return (css.match(/(^|\s)open-sidebar\S+/g) || []).join(' ');
      });
    });
  }
});
