$(document).ready(function() {
  var linkToAppExternal = $('a.app-external');

  // redirect to app external
  linkToAppExternal.on('click', function(e) {
    e.preventDefault();
    $(this).parents('.header-account').find('form').trigger('submit');
  });
});
