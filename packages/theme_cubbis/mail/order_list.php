<?php
defined('C5_EXECUTE') or die("Access Denied.");

$subject = t('Comanda Web (%s)', $userName);

ob_start()
?>
<table align="center" width="100%" cellpadding="0" cellspacing="0" bgcolor="#E6E6E6" style="background:#E6E6E6;font-size:15px;">
  <tr><td height="6"></td></tr>
  <tr>
    <td align="center">
      <table width="700" cellpadding="0" cellspacing="0" bgcolor="#FFFFFF" style="background:#FFFFFF;border-radius:6px">
        <tr><td height="10"></td></tr>
        <tr>
          <td style="padding: 0 10px;">
            <table width="100%" cellpadding="0" cellspacing="0">
              <tr>
                <td align="center">
                  <a href="http://cubbis.ro/">
                    <img width="auto" height="50" src="http://cubbis.ro/application/files/5115/8636/5800/Logo_cubbis_vertical.png" alt="cubbis">
                  </a>
                </td>
              </tr>
              <tr><td height="10"></td></tr>
              <tr><td height="1" bgcolor="#E6E6E6" style="background:#E6E6E6"></td></tr>
              <tr><td height="10"></td></tr>
              <tr>
                <td>
                  <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                      <td width="50" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                          <tr>
                            <td colspan="2" style="font-size:16px;font-weight:bold;padding-bottom:10px;">Persoana contact:</td>
                          </tr>
                          <tr>
                            <td width="90">Nume:</td>
                            <td><?= $userName ?></td>
                          </tr>
                          <tr>
                            <td width="90">Email:</td>
                            <td><?= $userEmail ?></td>
                          </tr>
                          <tr>
                            <td width="90">Telefon:</td>
                            <td><?= $userPhone ?></td>
                          </tr>
                        </table>
                      </td>
                      <td width="50" valign="top">
                        <table width="100%" cellpadding="0" cellspacing="0">
                          <tr>
                            <td colspan="2" style="font-size:16px;font-weight:bold;padding-bottom:10px;">Detalii comanda:</td>
                          </tr>
                          <tr>
                            <td width="90">Pret total:</td>
                            <td><?= $orderPrice ?></td>
                          </tr>
                          <tr>
                            <td width="90">Data plasare:</td>
                            <td><?= $orderDate ?></td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

              <?php if (!empty($userExtraInfo)) { ?>
                <tr>
                  <td style="font-size:16px;font-weight:bold;padding:10px 0;">Informatii aditionale:</td>
                </tr>
                <tr>
                  <td><?= $userExtraInfo ?></td>
                </tr>
              <?php } ?>

              <tr><td height="10"></td></tr>
              <tr><td height="1" bgcolor="#E6E6E6" style="background:#E6E6E6"></td></tr>
              <tr><td height="10"></td></tr>
              <tr>
                <td>
                  <table width="100%" cellpadding="2" cellspacing="0" border="1" style="text-align:left;border-color:#E6E6E6;">
                    <tr>
                      <th>Nume</th>
                      <th>Cod</th>
                      <th>Furnizor</th>
                      <th>Pret unitar</th>
                      <th>Cantitate</th>
                    </tr>
                    <?php foreach ($orderList as $list) { ?>
                      <tr>
                        <td><?= $list['name'] ?></td>
                        <td><?= $list['code'] ?></td>
                        <td><?= $list['provider'] ?></td>
                        <td><?= $list['uprice'] ?></td>
                        <td><?= $list['quantity'] ?></td>
                      </tr>
                    <?php } ?>
                  </table>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr><td height="10"></td></tr>
      </table>
    </td>
  </tr>
  <tr><td height="6"></td></tr>
</table>
<?php
$bodyHTML = ob_get_clean();
?>
