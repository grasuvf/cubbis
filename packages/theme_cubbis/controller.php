<?php
namespace Concrete\Package\ThemeCubbis;
use Package;
use PageTheme;
use PageTemplate;
use BlockType;
use BlockTypeSet;
use Concrete\Core\Page\Page;
use Concrete\Core\Page\Single as SinglePage;
use Concrete\Core\Asset\AssetList;
use Concrete\Core\Job\Job as AbstractJob;
use Concrete\Core\Attribute\Type as AttributeType;
use Concrete\Core\Attribute\Key\UserKey as UserAttributeKey;
use Concrete\Core\Attribute\Key\CollectionKey as CollectionAttributeKey;

defined('C5_EXECUTE') or die(_("Access Denied."));

class Controller extends Package
{
	protected $pkgHandle = 'theme_cubbis';
	protected $appVersionRequired = '5.7.5';
	protected $pkgVersion = '1.5.7';
	protected $pkg;

	public function getPackageName()
	{
		return t("Cubbis Theme");
	}

	public function getPackageDescription()
	{
		return t("Cubbis Theme for Concrete5");
	}

	public function on_start()
	{
		$al = AssetList::getInstance();
		
		$al->register('css', 'toastr', 'themes/cubbis/css/addons/toastr.min.css', array('minify' => false, 'combine' => false), 'theme_cubbis');
		$al->register('javascript', 'toastr', 'themes/cubbis/js/toastr.min.js', array('minify' => false, 'combine' => false), 'theme_cubbis');
		$al->registerGroup('toastr', array(
			array('css', 'toastr'),
			array('javascript', 'toastr')
		));
		
		$al->register('css', 'litebox', 'themes/cubbis/css/addons/litebox.min.css', array('minify' => false, 'combine' => false), 'theme_cubbis');
		$al->register('javascript', 'litebox', 'themes/cubbis/js/litebox.min.js', array('minify' => false, 'combine' => false), 'theme_cubbis');
		$al->registerGroup('litebox', array(
			array('css', 'litebox'),
			array('javascript', 'litebox')
		));

		$al->register('css', 'range-slider', 'themes/cubbis/css/addons/rSlider.min.css', array('minify' => false, 'combine' => false), 'theme_cubbis');
		$al->register('javascript', 'range-slider', 'themes/cubbis/js/rSlider.min.js', array('minify' => false, 'combine' => false), 'theme_cubbis');
		$al->registerGroup('range-slider', array(
			array('css', 'range-slider'),
			array('javascript', 'range-slider')
		));

		$al->register('javascript', 'lazy-sizes', 'themes/cubbis/js/lazysizes.min.js', array('minify' => false, 'combine' => false), 'theme_cubbis');
		$al->register('css', 'order-list', 'themes/cubbis/css/common/orderList.css', array('minify' => true, 'combine' => true), 'theme_cubbis');
	}

	public function install()
	{
		$pkg = parent::install();
		$this->installOrUpgrade($pkg);
	}

	public function upgrade()
	{
		parent::upgrade();
		$pkg = Package::getByHandle('theme_cubbis');
		$this->installOrUpgrade($pkg);
	}

	public function uninstall()
	{
		parent::uninstall();
	}

	public function installOrUpgrade($pkg)
	{
		// theme
		if (!is_object(PageTheme::getByHandle('cubbis'))) {
			PageTheme::add('cubbis', $pkg);
		}

		// single pages
		// remove Cart functionality
		// $sp = Page::getByPath('/dashboard/user_orders');
		// if (!is_object($sp) || $sp->isError()) {
		// 	$spa = SinglePage::add('/dashboard/user_orders', $pkg);
		// 	$spa->update(array('cName' => 'Users Orders', 'cDescription' => 'User Order List'));
		// }

		// page templates
		if (!is_object(PageTemplate::getByHandle('full'))) {
			PageTemplate::add('full', t('Full'), 'full.png', $pkg);
		}

		// blocks
		if (!is_object(BlockTypeSet::getByHandle('blocksetcubbis'))) {
			BlockTypeSet::add('blocksetcubbis', 'Cubbis', $pkg);
		}
		if (!is_object(BlockType::getByHandle('spacer'))) {
			BlockType::installBlockType('spacer', $pkg);
		}
		if (!is_object(BlockType::getByHandle('carousel'))) {
			BlockType::installBlockType('carousel', $pkg);
		}
		if (!is_object(BlockType::getByHandle('image_gallery'))) {
			BlockType::installBlockType('image_gallery', $pkg);
		}
		if (!is_object(BlockType::getByHandle('link_list'))) {
			BlockType::installBlockType('link_list', $pkg);
		}
		if (!is_object(BlockType::getByHandle('map_locations'))) {
			BlockType::installBlockType('map_locations', $pkg);
		}
		if (!is_object(BlockType::getByHandle('product_list'))) {
			BlockType::installBlockType('product_list', $pkg);
		}
		// remove Cart functionality
		// if (!is_object(BlockType::getByHandle('order_cart'))) {
		// 	BlockType::installBlockType('order_cart', $pkg);
		// }
		// if (!is_object(BlockType::getByHandle('order_logs'))) {
		// 	BlockType::installBlockType('order_logs', $pkg);
		// }
		if (!is_object(BlockType::getByHandle('teaser_banner'))) {
			BlockType::installBlockType('teaser_banner', $pkg);
		}
		if (!is_object(BlockType::getByHandle('teaser_info'))) {
			BlockType::installBlockType('teaser_info', $pkg);
		}
		if (!is_object(BlockType::getByHandle('video_background'))) {
			BlockType::installBlockType('video_background', $pkg);
		}

		// jobs
		if (!is_object(AbstractJob::getByHandle('update_users'))) {
			AbstractJob::installByPackage('update_users', $pkg);
		}

		// site attributes
		$imgAttr = AttributeType::getByHandle('image_file');
		$boolAttr = AttributeType::getByHandle('boolean');
		$textAttr = AttributeType::getByHandle('text');
		if (!is_object(CollectionAttributeKey::getByHandle('thumbnail'))) {
			CollectionAttributeKey::add($imgAttr, array('akHandle' => 'thumbnail', 'akName' => t('Thumbnail')), $pkg);
		}
		if (!is_object(CollectionAttributeKey::getByHandle('exclude_subpages_from_nav'))) {
			CollectionAttributeKey::add($boolAttr, array('akHandle' => 'exclude_subpages_from_nav', 'akName' => t('Exclude Subpages from Nav')), $pkg);
		}

		// user attributes
		if (!is_object(UserAttributeKey::getByHandle('user_first_name'))) {
			UserAttributeKey::add($textAttr, array('akHandle' => 'user_first_name', 'akName' => t('First Name')), $pkg);
		}
		if (!is_object(UserAttributeKey::getByHandle('user_last_name'))) {
			UserAttributeKey::add($textAttr, array('akHandle' => 'user_last_name', 'akName' => t('Last Name')), $pkg);
		}
		if (!is_object(UserAttributeKey::getByHandle('user_token_id'))) {
			UserAttributeKey::add($textAttr, array('akHandle' => 'user_token_id', 'akName' => t('Token ID')), $pkg);
		}
		if (!is_object(UserAttributeKey::getByHandle('user_token'))) {
			UserAttributeKey::add($textAttr, array('akHandle' => 'user_token', 'akName' => t('Token')), $pkg);
		}
		if (!is_object(UserAttributeKey::getByHandle('user_price'))) {
			UserAttributeKey::add($textAttr, array('akHandle' => 'user_price', 'akName' => t('Price')), $pkg);
		}
		if (!is_object(UserAttributeKey::getByHandle('user_type'))) {
			UserAttributeKey::add($textAttr, array('akHandle' => 'user_type', 'akName' => t('Type')), $pkg);
		}
		if (!is_object(UserAttributeKey::getByHandle('user_phone'))) {
			UserAttributeKey::add($textAttr, array('akHandle' => 'user_phone', 'akName' => t('Phone')), $pkg);
		}

		// This is only possible in 8.3.2 or greater
		if (version_compare(APP_VERSION, '8.3.2', '>=')) {
			$this->installContentFile('content.xml');
		}
	}
}
